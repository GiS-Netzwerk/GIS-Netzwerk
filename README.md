[![pipeline status](http://test.gis-netzwerk.com/mad/GIS-Netzwerk/badges/master/pipeline.svg)](http://test.gis-netzwerk.com/mad/GIS-Netzwerk/-/commits/master)
[![coverage report](http://test.gis-netzwerk.com/mad/GIS-Netzwerk/badges/master/coverage.svg)](http://test.gis-netzwerk.com/mad/GIS-Netzwerk/-/commits/master)
# GIS-Netzwerk
Static generated website using [GatsbyJS](https://www.gatsbyjs.org/).

## Data Sources

Data for posts is pulled from Markdown-files located at "[content/](https://github.com/DaTurboD/GIS-Netzwerk/tree/master/content)".

## Build and Deploy

AWS CodePipeline, CodeBuild, S3, Cloudfront#   G I S - N e t z w e r k 
 
 