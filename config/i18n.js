// Only one item MUST have the "default: true" key

module.exports = {
  de: {
    default: true,
    path: `de`,
    locale: `de-GER`,
    dateFormat: `DD-MM-YYYY`,
    siteLanguage: `de`,
    ogLanguage: `de-GER`,
    defaultTitle: `Gatsby Starter with multi-language and CMS`,
    defaultDescription: `Gatsby example site using Markdown, i18n and CMS`,
  },
  en: {
    path: `en`,
    locale: `en-US`,
    dateFormat: `DD/MM/YYYY`,
    siteLanguage: `en`,
    ogLanguage: `en_US`,
    defaultTitle: `Gatsby Starter with multi-language and CMS`,
    defaultDescription: `Gatsby example site using Markdown, i18n and CMS`,
  },
}
