---
page: true
layout: "page"
title: "Kontakt"
date: "2019-09-30"
---

Du hast Fragen über den Internetauftritt, GIS oder Kooperationen?
[kontakt@gis-netzwerk.com](mailto:kontakt@gis-netzwerk.com)

