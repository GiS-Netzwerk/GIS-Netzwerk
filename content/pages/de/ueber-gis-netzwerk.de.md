---
page: true
layout: "page"
title: "Über GIS-Netzwerk"
date: "2020-02-16"
---

import Instagram from "../../../src/components/Instagram/Instagram.jsx"

GIS-Netzwerk ist eine Informationsplattform für [GIS](/gis/was-ist-gis "Was ist GIS") (Geoinformationssystem), [Geodaten](/gis/was-sind-geodaten "Was sind Geodaten?") und [Web-Development](/web-development "Web-Development").

