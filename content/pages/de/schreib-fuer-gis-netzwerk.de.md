---
page: true
layout: "page"
title: "Schreib für GIS-Netzwerk"
date: "2020-02-16"
---

> Auf GIS-Netzwerk werden immer motivierte und engagierte Autoren gesucht, die ihr Wissen mit der Welt teilen möchten.

+ Du bist Experte in einen dieser [Kategorien bzw. Themen](https://github.com/DaTurboD/GIS-Netzwerk/blob/master/content/categories.yaml "Kategorien/Themen")?.
+ Dein Artikel umfasst mindestens 600 Wörter?.
+ Dein Content wurde und wird auch nicht anderswo veröffentlicht?.

## Beitrag einreichen

So reichst du einen Beitrag ein.

Du schreibst an mailto:kontakt@gis-netzwerk.com eine E-Mail mit folgendem Inhalt: 

* Infos zu dir: Name, Links zu Social-Media Profilen etc. (siehe [Autoren](https://github.com/DaTurboD/GIS-Netzwerk/blob/master/content/authors.yaml "Autoren")) mit einer kurzen Begründung, warum auf auf GIS-Netzwerk einen Beitrag veröffentlichen möchtest.
* Deinen Beitrag als Markdown-Datei inkl. aller verwendeteten Bilder (am Besten in einer .zip-Datei)






