---
page: true
layout: "page"
title: "Privacy Policy"
date: "2019-09-30"
---

## Privacy Policy

Personal data (usually referred to just as "data" below) will only be processed by us to the extent necessary and for the purpose of providing a functional and user-friendly website, including its contents, and the services offered there.

Per Art. 4 No. 1 of Regulation (EU) 2016/679, i.e. the General Data Protection Regulation (hereinafter referred to as the "GDPR"), "processing" refers to any operation or set of operations such as collection, recording, organization, structuring, storage, adaptation, alteration, retrieval, consultation, use, disclosure by transmission, dissemination, or otherwise making available, alignment, or combination, restriction, erasure, or destruction performed on personal data, whether by automated means or not.

The following privacy policy is intended to inform you in particular about the type, scope, purpose, duration, and legal basis for the processing of such data either under our own control or in conjunction with others. We also inform you below about the third-party components we use to optimize our website and improve the user experience which may result in said third parties also processing data they collect and control.

Our privacy policy is structured as follows:

I. Information about us as controllers of your data  
II. The rights of users and data subjects  
III. Information about the data processing

### I. Information about us as controllers of your data

The party responsible for this website (the "controller") for purposes of data protection law is:

Max Dietrich
Am Weiderfeld 5
83026 Rosenheim
Deutschland
E-Mail: max.dietrich@gis-netzwerk.com

### II. The rights of users and data subjects

With regard to the data processing to be described in more detail below, users and data subjects have the right

*   to confirmation of whether data concerning them is being processed, information about the data being processed, further information about the nature of the data processing, and copies of the data (cf. also Art. 15 GDPR);
*   to correct or complete incorrect or incomplete data (cf. also Art. 16 GDPR);
*   to the immediate deletion of data concerning them (cf. also Art. 17 DSGVO), or, alternatively, if further processing is necessary as stipulated in Art. 17 Para. 3 GDPR, to restrict said processing per Art. 18 GDPR;
*   to receive copies of the data concerning them and/or provided by them and to have the same transmitted to other providers/controllers (cf. also Art. 20 GDPR);
*   to file complaints with the supervisory authority if they believe that data concerning them is being processed by the controller in breach of data protection provisions (see also Art. 77 GDPR).

In addition, the controller is obliged to inform all recipients to whom it discloses data of any such corrections, deletions, or restrictions placed on processing the same per Art. 16, 17 Para. 1, 18 GDPR. However, this obligation does not apply if such notification is impossible or involves a disproportionate effort. Nevertheless, users have a right to information about these recipients.

**Likewise, under Art. 21 GDPR, users and data subjects have the right to object to the controller's future processing of their data pursuant to Art. 6 Para. 1 lit. f) GDPR. In particular, an objection to data processing for the purpose of direct advertising is permissible.**

### III. Information about the data processing

Your data processed when using our website will be deleted or blocked as soon as the purpose for its storage ceases to apply, provided the deletion of the same is not in breach of any statutory storage obligations or unless otherwise stipulated below.

#### Server data

For technical reasons, the following data sent by your internet browser to us or to our server provider will be collected, especially to ensure a secure and stable website: These server log files record the type and version of your browser, operating system, the website from which you came (referrer URL), the webpages on our site visited, the date and time of your visit, as well as the IP address from which you visited our site.

The data thus collected will be temporarily stored, but not in association with any other of your data.

The basis for this storage is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in the improvement, stability, functionality, and security of our website.

The data will be deleted within no more than seven days, unless continued storage is required for evidentiary purposes. In which case, all or part of the data will be excluded from deletion until the investigation of the relevant incident is finally resolved.

#### Cookies

##### a) Session cookies

We use cookies on our website. Cookies are small text files or other storage technologies stored on your computer by your browser. These cookies process certain specific information about you, such as your browser, location data, or IP address.  

This processing makes our website more user-friendly, efficient, and secure, allowing us, for example, to display our website in different languages or to offer a shopping cart function.

The legal basis for such processing is Art. 6 Para. 1 lit. b) GDPR, insofar as these cookies are used to collect data to initiate or process contractual relationships.

If the processing does not serve to initiate or process a contract, our legitimate interest lies in improving the functionality of our website. The legal basis is then Art. 6 Para. 1 lit. f) GDPR.

When you close your browser, these session cookies are deleted.

##### b) Third-party cookies

If necessary, our website may also use cookies from companies with whom we cooperate for the purpose of advertising, analyzing, or improving the features of our website.

Please refer to the following information for details, in particular for the legal basis and purpose of such third-party collection and processing of data collected through cookies.

##### c) Disabling cookies

You can refuse the use of cookies by changing the settings on your browser. Likewise, you can use the browser to delete cookies that have already been stored. However, the steps and measures required vary, depending on the browser you use. If you have any questions, please use the help function or consult the documentation for your browser or contact its maker for support. Browser settings cannot prevent so-called flash cookies from being set. Instead, you will need to change the setting of your Flash player. The steps and measures required for this also depend on the Flash player you are using. If you have any questions, please use the help function or consult the documentation for your Flash player or contact its maker for support.

If you prevent or restrict the installation of cookies, not all of the functions on our site may be fully usable.

#### Contact

If you contact us via email or the contact form, the data you provide will be used for the purpose of processing your request. We must have this data in order to process and answer your inquiry; otherwise we will not be able to answer it in full or at all.

The legal basis for this data processing is Art. 6 Para. 1 lit. b) GDPR.

Your data will be deleted once we have fully answered your inquiry and there is no further legal obligation to store your data, such as if an order or contract resulted therefrom.

#### Twitter

We maintain an online presence on Twitter to present our company and our services and to communicate with customers/prospects. Twitter is a service provided by Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA.

We would like to point out that this might cause user data to be processed outside the European Union, particularly in the United States. This may increase risks for users that, for example, may make subsequent access to the user data more difficult. We also do not have access to this user data. Access is only available to Twitter. Twitter Inc. is certified under the Privacy Shield and committed to adhering to European privacy standards.

[https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active)

The privacy policy of Twitter can be found at

[https://twitter.com/privacy](https://twitter.com/de/privacy)

#### LinkedIn

We maintain an online presence on LinkedIn to present our company and our services and to communicate with customers/prospects. LinkedIn is a service of LinkedIn Ireland Unlimited Company, Wilton Plaza, Wilton Place, Dublin 2, Irland, a subsidiary of LinkedIn Corporation, 1000 W. Maude Avenue, Sunnyvale, CA 94085, USA.

We would like to point out that this might cause user data to be processed outside the European Union, particularly in the United States. This may increase risks for users that, for example, may make subsequent access to the user data more difficult. We also do not have access to this user data. Access is only available to LinkedIn. LinkedIn Corporation is certified under the Privacy Shield and committed to comply with European privacy standards.

[https://www.privacyshield.gov/participant?id=a2zt0000000L0UZAA0&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000L0UZAA0&status=Active)

The LinkedIn privacy policy can be found here:

[https://www.linkedin.com/legal/privacy-policy](https://www.linkedin.com/legal/privacy-policy)

#### Facebook

To advertise our products and services as well as to communicate with interested parties or customers, we have a presence on the Facebook platform.

On this social media platform, we are jointly responsible with Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbor, Dublin 2, Ireland.

The data protection officer of Facebook can be reached via this contact form:

[https://www.facebook.com/help/contact/540977946302970](https://www.facebook.com/help/contact/540977946302970)

We have defined the joint responsibility in an agreement regarding the respective obligations within the meaning of the GDPR. This agreement, which sets out the reciprocal obligations, is available at the following link:

[https://www.facebook.com/legal/terms/page_controller_addendum](https://www.facebook.com/legal/terms/page_controller_addendum)

The legal basis for the processing of the resulting and subsequently disclosed personal data is Art. 6 para. 1 lit. f GDPR. Our legitimate interest lies in the analysis, communication, sales, and promotion of our products and services.

The legal basis may also be your consent per Art. 6 para. 1 lit. a GDPR granted to the platform operator. Per Art. 7 para. 3 GDPR, you may revoke this consent with the platform operator at any time with future effect.

When accessing our online presence on the Facebook platform, Facebook Ireland Ltd. as the operator of the platform in the EU will process your data (e.g. personal information, IP address, etc.).

This data of the user is used for statistical information on the use of our company presence on Facebook. Facebook Ireland Ltd. uses this data for market research and advertising purposes as well as for the creation of user profiles. Based on these profiles, Facebook Ireland Ltd. can provide advertising both within and outside of Facebook based on your interests. If you are logged into Facebook at the time you access our site, Facebook Ireland Ltd. will also link this data to your user account.

If you contact us via Facebook, the personal data your provide at that time will be used to process the request. We will delete this data once we have completely responded to your query, unless there are legal obligations to retain the data, such as for subsequent fulfillment of contracts.

Facebook Ireland Ltd. might also set cookies when processing your data.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Cookies that have already been saved can be deleted at any time. The instructions to do this depend on the browser and system being used. For Flash cookies, the processing cannot be prevented by the settings in your browser, but instead by making the appropriate settings in your Flash player. If you prevent or restrict the installation of cookies, not all of the functions of Facebook may be fully usable.

Details on the processing activities, their suppression, and the deletion of the data processed by Facebook can be found in its privacy policy:

[https://www.facebook.com/privacy/explanation](https://www.facebook.com/privacy/explanation)

It cannot be excluded that the processing by Facebook Ireland Ltd. will also take place in the United States by Facebook Inc., 1601 Willow Road, Menlo Park, California 94025.

Facebook Inc. has submitted to the EU-US Privacy Shield, thereby complying with the data protection requirements of the EU when processing data in the USA.

[https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active)

#### Instagram

To advertise our products and services as well as to communicate with interested parties or customers, we have a presence on the Instagram platform.

On this social media platform, we are jointly responsible with Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbour, Dublin 2 Ireland.

The data protection officer of Instagram can be reached via this contact form:

[https://www.facebook.com/help/contact/540977946302970](https://www.facebook.com/help/contact/540977946302970)

We have defined the joint responsibility in an agreement regarding the respective obligations within the meaning of the GDPR. This agreement, which sets out the reciprocal obligations, is available at the following link:

[https://www.facebook.com/legal/terms/page_controller_addendum](https://www.facebook.com/legal/terms/page_controller_addendum)

The legal basis for the processing of the resulting and subsequently disclosed personal data is Art. 6 para. 1 lit. f GDPR. Our legitimate interest lies in the analysis, communication, sales, and promotion of our products and services.

The legal basis may also be your consent per Art. 6 para. 1 lit. a GDPR granted to the platform operator. Per Art. 7 para. 3 GDPR, you may revoke this consent with the platform operator at any time with future effect.

When accessing our online presence on the Instagram platform, Facebook Ireland Ltd. as the operator of the platform in the EU will process your data (e.g. personal information, IP address, etc.).

This data of the user is used for statistical information on the use of our company presence on Instagram. Facebook Ireland Ltd. uses this data for market research and advertising purposes as well as for the creation of user profiles. Based on these profiles, Facebook Ireland Ltd. can provide advertising both within and outside of Instagram based on your interests. If you are logged into Instagram at the time you access our site, Facebook Ireland Ltd. will also link this data to your user account.

If you contact us via Instagram, the personal data your provide at that time will be used to process the request. We will delete this data once we have completely responded to your query, unless there are legal obligations to retain the data, such as for subsequent fulfillment of contracts.

Facebook Ireland Ltd. might also set cookies when processing your data.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Cookies that have already been saved can be deleted at any time. The instructions to do this depend on the browser and system being used. For Flash cookies, the processing cannot be prevented by the settings in your browser, but instead by making the appropriate settings in your Flash player. If you prevent or restrict the installation of cookies, not all of the functions of Instagram may be fully usable.

Details on the processing activities, their suppression, and the deletion of the data processed by Instagram can be found in its privacy policy:

[https://help.instagram.com/519522125107875](https://help.instagram.com/519522125107875)

It cannot be excluded that the processing by Facebook Ireland Ltd. will also take place in the United States by Facebook Inc., 1601 Willow Road, Menlo Park, California 94025.

Facebook Inc. has submitted to the EU-US Privacy Shield, thereby complying with the data protection requirements of the EU when processing data in the USA.

[https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active)

#### Social media links via graphics

We also integrate the following social media sites into our website. The integration takes place via a linked graphic of the respective site. The use of these graphics stored on our own servers prevents the automatic connection to the servers of these networks for their display. Only by clicking on the corresponding graphic will you be forwarded to the service of the respective social network.

Once you click, that network may record information about you and your visit to our site. It cannot be ruled out that such data will be processed in the United States.

Initially, this data includes such things as your IP address, the date and time of your visit, and the page visited. If you are logged into your user account on that network, however, the network operator might assign the information collected about your visit to our site to your personal account. If you interact by clicking Like, Share, etc., this information can be stored your personal user account and possibly posted on the respective network. To prevent this, you need to log out of your social media account before clicking on the graphic. The various social media networks also offer settings that you can configure accordingly.

The following social networks are integrated into our site by linked graphics:

#### Facebook

Facebook Ireland Limited, 4 Grand Canal Square, Dublin 2, Ireland, a subsidiary of Facebook Inc., 1601 S. California Ave., Palo Alto, CA 94304, USA.

Privacy Policy: [https://www.facebook.com/policy.php](https://www.facebook.com/policy.php)

EU-US Privacy Shield [https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active)

#### twitter

Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA

Privacy Policy: [https://twitter.com/privacy](https://twitter.com/privacy)

EU-US Privacy Shield

[https://www.privacyshield.gov/…0000TORzAAO&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active)

#### LinkedIn

LinkedIn Ireland Unlimited Company, Wilton Plaza, Wilton Place, Dublin 2, Irland, a subsidiary of LinkedIn Corporation, 1000 W. Maude Avenue, Sunnyvale, CA 94085 USA.

Privacy Policy: [https://www.linkedin.com/legal/privacy-policy](https://www.linkedin.com/legal/privacy-policy)

EU-US Privacy Shield [https://www.privacyshield.gov/participant?id=a2zt0000000L0UZAA0&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000L0UZAA0&status=Active)

#### Matomo (formerly: PIWIK)

Our website uses Matomo (formerly: PIWIK). This is open-source software with which we can analyze the use of our site. Data such as your IP address, the pages you visit, the website from which you came (referrer URL), the duration of your visit, and the frequency of your visits is processed.

Matomo stores a cookie on your device via your browser in order to collect this data. This cookie is valid for one week.

The legal basis is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in the analysis and optimization of our website.

We use Matomo with the "Automatically Anonymize Visitor IPs" function. This anonymization function truncates your IP address by two bytes so that it is impossible to assign it to you or to the internet connection you are using.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Further details can be found in the section about cookies above.

In addition, you have the option of terminating the analysis of your usage behavior by opting out. By confirming the link

<span style="color: #ff0000;">
<iframe
        style="border: 0; height: 200px; width: 600px;"
        src="https://dev.gis-netzwerk.com/matomo/index.php?module=CoreAdminHome&action=optOut&language=de&backgroundColor=&fontColor=&fontSize=&fontFamily="
        ></iframe>
</span>

a cookie is stored on your device via your browser to prevent any further analysis. Please note, however, that you must click the above link again if you delete the cookies stored on your end device.

#### Google-Maps

Our website uses Google Maps to display our location and to provide directions. This is a service provided by Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland (hereinafter: Google).

Through certification according to the EU-US Privacy Shield

[https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&status=Active](https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&status=Active)

Google guarantees that it will follow the EU's data protection regulations when processing data in the United States.

To enable the display of certain fonts on our website, a connection to the Google server in the USA is established whenever our website is accessed.

If you access the Google Maps components integrated into our website, Google will store a cookie on your device via your browser. Your user settings and data are processed to display our location and create a route description. We cannot prevent Google from using servers in the USA.

The legal basis is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in optimizing the functionality of our website.

By connecting to Google in this way, Google can determine from which website your request has been sent and to which IP address the directions are transmitted.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Further details can be found in the section about cookies above.

In addition, the use of Google Maps and the information obtained via Google Maps is governed by the [Google Terms of Use](http://www.google.de/accounts/TOS) [https://policies.google.com/terms?gl=DE&hl=en](https://policies.google.com/terms?gl=DE&hl=de) and the [Terms and Conditions for Google Maps](http://www.google.com/intl/de_de/help/terms_maps.html) https://www.google.com/intl/de_de/help/terms_maps.html.

Google also offers further information at

[https://adssettings.google.com/authenticated](https://adssettings.google.com/authenticated)

[https://policies.google.com/privacy](https://policies.google.com/privacy)

#### OpenStreetMap

For directions on our site, we use OpenStreetMap, a service of the OpenStreetMap Foundation, St John's Innovation Centre, Cowley Road, Cambridge, CB4 0WS, United Kingdom, hereinafter referred to as "OpenStreetMap".

When you access one of our Internet pages that includes the OpenStreetMap service, OpenStreetMap stores a cookie on your terminal device via your browser. This processes your user settings and user data for the purpose of displaying the page or guaranteeing the functionality of the OpenStreetMap service. Through this processing, OpenStreetMap can recognize the website from which your request has been sent and to which IP address the directions should be transmitted. 

The legal basis for collecting and processing this information is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in the optimization and economic operation of our site.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Further details can be found in the section about cookies above.

OpenStreetMap offers further information about its data collection and processing as well your rights and your options for protecting your privacy at this link:

[https://wiki.osmfoundation.org/wiki/Privacy_Policy](https://wiki.osmfoundation.org/wiki/Privacy_Policy).

#### HERE Maps

On our website we use the HERE Maps service to depict a route description.  This is a service of HERE Global B.V., Kennedyplein 222-226, 5611 ZT Eindhoven, The Netherlands, hereinafter referred to as "HERE".

In order to be able to display locations, a connection to the HERE server is required in addition to the querying of your IP address by HERE.  HERE initially uses your IP address exclusively to provide your Internet browser or yourself with the relevant information.  Your IP address is therefore required for the provision of the service.  However, HERE can use this connection to recognize that the request comes from our website and to which IP address the representations are to be sent.

In addition, HERE will store a cookie on your device via your internet browser.  This cookie is used to display the page and the associated functions in which HERE is integrated, and to process user settings and data.

If you do not consent to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your internet browser.  You can find details on this under "Cookies".

The legal basis is Art.  6 para. 1 lit. f) GDPR. Our legitimate interest lies in the optimisation of the functionality of our website.

If you have given your consent, in particular by making the appropriate setting in your Internet browser, HERE can also process your approximate location using your IP address.  The legal basis is Art.  6 para. 1 lit. a) GDPR. Consent can be revoked at any time in the future by setting your internet browser accordingly.

At

[https://legal.here.com/de-de/privacy/policy](https://legal.here.com/de-de/privacy/policy) 

HERE offers further information on data processing.

#### Mapbox API

For directions. we use Mapbox API, a service of Mapbox Inc., 740 15th Street NW, 5th Floor, Washington, District of Columbia 20005, USA, hereinafter referred to as "Mapbox".

Through certification according to the EU-US Privacy Shield

([https://www.privacyshield.gov/participant?id=a2zt0000000CbWQAA0&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000CbWQAA0&status=Active))

CloudFlare guarantees that it will follow the EU's data protection regulations when processing data in the United States.

The legal basis for collecting and processing this information is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in optimizing the functionality of our website.

When you access one of our pages that includes the Mapbox service, Mapbox stores a cookie on your terminal device via your browser. The information generated by the cookie about your use of our app including your IP address is transmitted to a Mapbox server in the USA and stored there. This data is processed for the purpose of displaying the page or ensuring the functionality of the Mapbox service. Mapbox may share this information with third parties where required to do so by law or where the information is processed by third parties on behalf of Mapbox.

The "Terms of Service" provided by Mapbox at [https://www.mapbox.com/tos/#maps](https://www.mapbox.com/tos/#maps) contain further information about the use of Mapbox and the data obtained by Mapbox.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Further details can be found in the section about cookies above. However, it will then no longer be possible to use the Mapbox service via our website.

In addition, Mapbox offers further information about how it collects and uses your data, your rights, and how to protect your privacy at the following link:

[https://www.mapbox.com/privacy/](https://www.mapbox.com/privacy/).

#### Amazon Associates (PartnerNet)

Our website participates in the Amazon Associates program, known as PartnerNet in German. This is a service provided by Amazon Europe Core S.à r.l., 5 Rue Plaetis, 2338 Luxembourg, Luxembourg. Advertisements from Amazon.de are placed on our website via the Amazon Associates program. If you click on one of these advertisements, you will be redirected to the corresponding offer on the Amazon website. If you subsequently decide to purchase the advertised product there, we will receive a commission from Amazon.

Amazon uses cookies to allow this service to work. With the help of these cookies, Amazon can verify that you were forwarded from our website to its website.

Amazon offers further information about data protection at this link:

[https://www.amazon.de/gp/help/customer/display.html?nodeId=201909010.](https://www.amazon.de/gp/help/customer/display.html?nodeId=201909010.)

The legal basis for collecting and processing this information is Art. 6 Para. 1 lit. f) GDPR. Our legitimate interest lies in ensuring that our commissions are processed and paid by Amazon.

If you do not agree to this processing, you have the option of preventing the installation of cookies by making the appropriate settings in your browser. Further details can be found in the section about cookies above.

#### MailChimp - Newsletter

We offer you the opportunity to register for our free newsletter via our website.

We use MailChimp, a service of The Rocket Science Group, LLC, 512 Means Street, Suite 404, Atlanta, GA 30318, USA, hereinafter referred to as "The Rocket Science Group".

Through certification according to the EU-US Privacy Shield

[https://www.privacyshield.gov/participant?id=a2zt0000000TO6hAAG&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000TO6hAAG&status=Active)

the Rocket Science Group guarantees that it will follow the EU's data protection regulations when processing data in the United States. In addition, the Rocket Science Group offers further information about its data protection practices at

[http://mailchimp.com/legal/privacy/](http://mailchimp.com/legal/privacy/)

If you register for our free newsletter, the data requested from you for this purpose, i.e. your email address and, optionally, your name and address, will be processed by The Rocket Science Group. In addition, your IP address and the date and time of your registration will be saved. During the registration process, your consent to receive this newsletter will be obtained together with a concrete description of the type of content it will offer and reference made to this privacy policy.

The newsletter then sent out by The Rocket Science Group will also contain a tracking pixel called a web beacon. This pixel helps us evaluate whether and when you have read our newsletter and whether you have clicked any links contained therein. In addition to further technical data, such as data about your computer hardware and your IP address, the data processed will be stored so that we can optimize our newsletter and respond to the wishes of our readers. The data will therefore increase the quality and attractiveness of our newsletter.

The legal basis for sending the newsletter and the analysis is Art. 6 Para. 1 lit. a) GDPR.

You may revoke your prior consent to receive this newsletter under Art. 7 Para. 3 GDPR with future effect. All you have to do is inform us that you are revoking your consent or click on the unsubscribe link contained in each newsletter.

[Model Data Protection Statement](https://www.ratgeberrecht.eu/leistungen/muster-datenschutzerklaerung.html) for [Anwaltskanzlei Weiß & Partner](https://www.ratgeberrecht.eu/)