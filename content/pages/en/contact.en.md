---
page: true
layout: "page"
title: "Contact"
date: "2019-09-30"
---

Do you have any questions about the website, GIS or cooperations?
[kontakt@gis-netzwerk.com](mailto: kontakt@gis-netzwerk.com)

