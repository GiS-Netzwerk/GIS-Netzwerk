---
layout: "post"
title: "GIS Software Categories - What Types of GIS Software Are There?"
date: "2019-04-28"
description: "ArcGIS, Google Maps, MySQL, FME, AutoCAD Map3D and software libraries all fall into the GIS software category, even if different processes are handled with each software."
category: "GIS"
tags: []
image: "/assets/img/gis-software-kategorien.jpg"
caption: "by USGS on Unsplash"
author: "Max Dietrich"
---

The term GIS software can sometimes be very confusing.

ArcGIS, Google Maps, MySQL, FME, AutoCAD Map3D and software libraries all fall into the GIS software category, even if different processes are handled with each software.

In general, GIS software is software that stores, retrieves, manages, edits, analyzes, or displays spatial data.

This includes the actual desktop GIS such as [QGIS](https://qgis.org) or ArcGIS, databases for storage, other tools such as FME with which geodata can be edited in all formats, or web servers which can be used to access geodata on the Internet.

In principle, GIS software can be divided into the following subcategories:

![GIS-Software Categories](./GIS-Software.jpg "GIS-Software Categories")

## Desktop GIS

A desktop GIS is usually offered in three ways.

* GIS Viewer: With a GIS Viewer, geodata can be viewed.
* GIS Editor: The editor enables the editing or editing of geodata.
* GIS Analyst: With an analyst, the spatial data can be analyzed using various criteria.

An editor and analyst are often also offered bundled in one version

## Mobile GIS

The functions of a Mobile GIS can be compared to a desktop GIS.

The only difference is that a mobile GIS is optimized for mobile devices such as smartphones and tablets.

## Online / Web GIS

A web GIS provides geodata via a web service, i.e. via the internet. This often has only simplified functions and is primarily used to display GIS data for the general public.

## Web map server

A web map server is the basis for a web GIS. The web map server, for example, provides a simple web map service, which is basically a web-based map service.

There are different types of functions of web map servers:

* [Web Map Service] (https://de.wikipedia.org/wiki/Web_Map_Service)(WMS)
* [Web Map Tile Service] (https://de.wikipedia.org/wiki/Web_Map_Tile_Service "Web Map Tile Service")(WMTS),
* [Web Map Service Caching] (https://de.wikipedia.org/wiki/Web_Map_Service-Caching "Web Map Service Caching")(WMS-C)
* Tile Map Service (TMS)

## database management systems

With database management systems, data is stored and managed independently of the application. Geospatial information systems use non-spatial and spatial database management systems.

* spatial database management systems: used to store the geometries of the objects
* Non-spatial database management systems: Used to store property data of the objects

## Software libraries / frameworks

With software development frameworks or libraries, applications can be created without having to completely reinvent the wheel.

Geodata can be rendered, geocoded, or made available on websites in various ways, for example. The necessary code can be found in these frameworks.