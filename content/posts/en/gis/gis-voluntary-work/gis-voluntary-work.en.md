---
layout: "post"
title: "GIS volunteering - Make the world a better place with GIS"
date: "2019-04-05"
description: "Volunteering offers a good opportunity to develop personally and professionally. You can also get involved in a good cause. You can later pack the projects into a pretty portfolio and thus stand out from the competition with extra points when applying."
category: "GIS"
tags: ["Jobs"]
image: "/assets/img/gis-freiwilligenarbeit.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

[GIS](/en/gis/geographic-information-system-what-is-gis "What is GIS?") Volunteering offers a good opportunity to develop personally and professionally.

You can also get involved in a good cause. You can later pack the projects into a pretty portfolio and thus stand out from the competition with extra points when applying.

What's even better is that you can join these organizations with a PC at home and don't have to travel the world.

## OpenStreetMap

![OpenStreetMap](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Openstreetmap_logo.svg/256px-Openstreetmap_logo.svg.png "OpenStreetMap")

[https://www.openstreetmap.org/](https://www.openstreetmap.org/)

OpenStreetMap is an international project that was founded in 2004.

The aim of OSM is to create a free world map and make it available to everyone free of charge. The data is collected by volunteers (also known as ** mappers **). Data on roads, railways, rivers, forests, houses, etc. are collected.

### How to join

There are many different ways to contribute to OpenStreetMap, from reporting small errors on the map, completing existing data, drawing new buildings from aerial photographs and recording routes and points of interest with the GPS device. Our instructions will help you use the right programs and enter data. (OpenStreetMap).

[Join...](https://www.openstreetmap.org/user/new?cookie_test=true)

## Humanitarian OpenStreetMap Team

![Humanitarian OpenStreetMap Team](https://www.hotosm.org/images/hot-logo-icon-nav.svg "Humanitarian OpenStreetMap Team")

[https://www.hotosm.org/](https://www.hotosm.org/)

The Humanitarian OpenStreetMap Team (HOT) is an international team dedicated to the mapping and mapping of humanitarian actions and the development of communities. The data are used to reduce risks in the and to work on sustainable development.

### GIS Freiwilligenarbeit mit HOT

As a **mapping volunteer** you can collect data for maps as with OpenStreetMap. ** Humanitarian and GIS Professionals ** also help in additional areas, such as data processing, validation of maps or create completely new maps and visualizations.

[Join...](https://www.hotosm.org/volunteer#humanitarian-and-gis-professionals)

## Standby Task Force

![Standby Task Force](https://www.standbytaskforce.org/wp-content/uploads/2016/02/cropped-Logo_SBTF_RED-03-450x203.png "Standby Task Force")

[https://www.standbytaskforce.org/](https://www.standbytaskforce.org/)

Standby Task Force is a global network of trained and experienced volunteers who work together online.

The Standby Task Force is a non-profit organization founded in 2010.

The Standby Task Force has been involved in many natural disasters since then, and the volunteers have assisted many humanitarian organizations in election observation and other projects.

### Voluntary with the Standby Task Force

You should already have professional experience in the areas of GIS management, disaster management and other technical areas.

[Join...](https://www.standbytaskforce.org/help-us/volunteer-with-us/)

## GISCorps

![GISCorp](https://www.urisa.org/clientuploads/directory/graphics/gc_logo.jpg "GISCorp")

[https://www.giscorps.org/](https://www.giscorps.org/)
GISCorps coordinates short-term, voluntary GIS services for disadvantaged communities.

The projects vary according to the needs of the partner agency and can include all aspects of GIS, including analysis, cartography, app development, needs analysis, technical workshops etc.

The service areas include humanitarian aid, disaster protection, environmental protection, health and health personnel services, GIS training and crowdsourcing of experts. GISCorps is supported by individual donations, companies and other non-profit groups with similar goals.

### Engage

At GISCorps there are several ways to get involved. You can apply as a volunteer and actively support GIS projects. You can get a one-year ArcGIS license for free here, provided you are accepted.

You can of course also support the project with donations.

[Join...](https://www.giscorps.org/become-a-volunteer/)