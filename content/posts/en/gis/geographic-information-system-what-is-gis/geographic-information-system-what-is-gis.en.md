---
layout: "post"
title: "Geographic information system - What is GIS?"
date: "2020-01-15"
description: "A geographic information system or GIS is a system for displaying and processing geodata, i.e. data to which a spatial position has been assigned."
category: "GIS"
tags: ["Geodata"]
image: "/assets/img/was-ist-ein-geoinformationssystem.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

The abbreviation GIS stands for geographic information system.

A geographic information system or **GIS** is a system for displaying and processing geodata, i.e. data to which a spatial position has been assigned.

With a geographic information system, complex issues can be visualized, presented and analyzed in a structured manner.

A GIS market was established in the mid-1980s. Authorities and universities in particular first recognized the great benefits of using GIS. The rapid technical progress of computers enabled the first GIS jobs to be created.

Purely graphically, a GIS consists of one or more layers on which data or [geodata](/geodata-what-are-geodata "what is geodata") are stored. As a definition of GIS, one could also call (like Arni) Google Maps with streets, places etc., because one can analyze and visualize geodata with Google Maps.

![GIS definition](./gis-schwarzenegger-tweet.jpg "GIS definition")

Google Maps helps to find the fastest way between two points/locations taking into account different modes of transport and is basically an interactive map.

> Incidentally, a free alternative to Google Maps is [OpenStreetMap](https://routing.openstreetmap.de/?z=7&center=50.004209%2C11.997070&hl=de&alt=0&srv=1 "OpenStreetMap"). OpenStreetMap, also OSM, is a project that deals with creating a free world map for everyone. This is licensed under the license "[Open Database License (ODbL) 1.0](http://opendatacommons.org/licenses/odbl/" Open Database License (ODbL) 1.0 ")" and can therefore also be used commercially for free.

With a GIS, you have the option of throwing together a wide variety of geodata in order to be able to recognize new patterns from this data. For example, orthophotos, topographic maps, statistical data and even technical plans can be integrated.

These are displayed on different layers, which can be shown or hidden as required. A geographic information system with the corresponding data thus provides the answers to many questions.

*   _Where are the most potential buyers for a product located?_
*   _Where do supply and disposal lines run?_
*   _Where do school catchments run and how does this affect public transport?_

A geographic information system basically consists of:

*   _[Software](/en/gis/open-source-proprietary-software-options "GIS software") or [Applications](/en/gis/gis-applications "GIS applications")_
*   _Hardware_
*   [_Data_](/en/gis/geodata-what-are-geodata "What are geodata")
*   Management / organization

![GIS infographic](./GIS-graphic_2-e1556304265569.jpg "GIS infographic")

## What GIS software is there?

There is relatively a large selection of GIS software options. These are available as open-source projects, which are provided free of charge, and as paid versions, which can cost up to several thousand euros for a fully equipped version.

A well-known **Open Source GIS** is [QGIS](https://www.qgis.org/en/site/ "QGIS"), which offers an extensive number of plugins and a very large community, which pushes the project forwards. QGIS is especially recommended for beginners, as there is a lot of documentation and you can of course use QGIS anywhere for free.

Two of the most popular proprietary GIS software ptions are Esri ArcGIS (Pro) and AutoCAD Map3D from Autodesk.

Which software is the better one cannot be said in general. This depends on several factors, but mainly the purpose and the budget available.

## Hardware

First of all, of course, you need a powerful computer on which the software works smoothly. The requirements of the GIS programs are similar to those of PC games, which is why a middle class "Gaming PC" offers a good GIS workplace.

More and more users are now also using mobile devices, for example tablets. These can be connected to GNSS receivers and you are able to record, edit and transfer high-quality geodata directly into a geographic information system from outdoor surveying work.

This saves a lot of time and work, since objects are measured outside in the classic way and this data is then incorporated in the office, which would be omitted here.

## Data - Geodata

Geodata is information that have a spatial reference. Everything happens somewhere. If you capture the "what" and "where" you will have geodata.

There are various ways of storing geodata and integrating them into a geographic information system. The most common options for storing geodata are:

*   in [Databases](/en/gis/gis-and-geo-database-management-system-options "GIS-Databases") (eg. [PostgreSQL](/en/gis/postgre-sql-with-post-gis-install-and-set-up in qgis "PostgreSQL"), MariaDB, MySQL etc.)
*   as [shapefile](/en/gis/what-is-a-shapefile "What is a shapefile") (format for spatial data developed by ESRI)
*   as GeoJSON
*   as XML or KML
*   in [CAD](/en/gis/gis-vs-cad-the-difference-between-gis-and-cad "difference GIS-CAD") formats like DXF / DWG
*   and much more (see [GIS data format](https://en.m.wikipedia.org/wiki/GIS_file_formats "GIS data formats"))

This data can be integrated into the geographic information system locally or via the web.

## Areas of application of GIS

Einsatz finden Geoinformationssysteme vorallem in:

*   _Municipalities, cities, counties, regional offices (GIS municipal)_
*   _Surveying and land registry_
*   _Energy supply (water, sewage, electricity, gas, etc.)_
*   _Property Management_
*   _Engineering offices (urban and traffic planning)_


## Conclusion

A geographic information system has become indispensable in many private and public areas. Property registers, supply networks and much more can be integrated very easily in a GIS. This data can then be maintained in the geographic information system and serve as the basis for further analyzes. With the help of GIS, complex problems can be clearly visualized and decisions and solutions can be found quickly and effectively.