---
layout: "post"
title: "Open-source Web-GIS applications"
date: "2019-04-24"
description: ""
category: "GIS"
tags: ["Web-GIS"]
image: "/assets/img/open-source-web-gis-anwendungen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

You want to know which open-source web-GIS applications are used to share geospatial data over the Internet?

## Web-GIS applications

### 1. GeoServer

GeoServer is an open source server for sharing geospatial data.

[http://geoserver.org/](http://geoserver.org/)

### 2. degree

deegree is an open source software for geodata infrastructures and the geospatial web.

[https://www.deegree.org/](https://www.deegree.org/)

### 3. FeatureServer

FeatureServer is an implementation of a RESTful Geographic Feature Service.

[http://featureserver.org/](http://featureserver.org/)

### MapGuide Open Source

MapGuide Open Source is a web-based platform that enables users to develop and deploy web mapping applications and geospatial services.

[http://mapguide.osgeo.org/](http://mapguide.osgeo.org/)

### 5. MapServer

MapServer is an open-source platform for publishing geodata and interactive map applications on the web.

[https://www.mapserver.org/](https://www.mapserver.org/)

* * *

## Javascript libraries

### 6. OpenLayers

OpenLayers is a JavaScript library that enables geospatial data to be displayed in the web browser. OpenLayers is a programming interface that allows client-side development independent of the server.

[https://openlayers.org/](https://openlayers.org/)

### 7. Leaflet

Leaflet is a free JavaScript library that can be used to create Web-GIS applications. The library uses HTML5, CSS3 and therefore supports most browsers.

[https://leafletjs.com/](https://leafletjs.com/)