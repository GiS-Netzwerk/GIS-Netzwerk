---
layout: "post"
title: "Geo and GIS podcasts to stay up to date"
date: "2019-04-20"
description: "Podcasts are a great way to keep up to date with current developments. Best of all, you can listen to podcasts practically anywhere."
category: "GIS"
tags: ["Podcasts"]
image: "/assets/img/geo-und-gis-podcasts-um-auf-dem-aktuellen-stand-zu-bleiben.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

Podcasts are a great way to keep up to date with current developments. Best of all, you can listen to podcasts practically anywhere.

## VerySpatial

The podcast is hosted by Jesse Rouse, Sue Bergeron and Frank Lafone. An established podcast that discusses geography, geographic information technologies and the impact of [GIS](/en/gis/geographic-information-system-what-is-gis "What is GIS") on everyday digital life.

[http://veryspatial.com/](http://veryspatial.com/)

![VerySpatial Podcast](VSLogo_Small_400x400.jpg "VerySpatial")

## Mapscaping

New podcast. Mainly interviews with people from the GIS and geo industry.

[https://mapscaping.com/blogs/the-mapscaping-podcast](https://mapscaping.com/blogs/the-mapscaping-podcast)

![Mapscaping Podcast](/gedY7m5O_400x400.jpg "Mapscaping")

## Radio OSM

Reports and news about OpenStreetMap, the free wiki world map.

[https://podcast.openstreetmap.de/](https://podcast.openstreetmap.de/)

![Radio OSM](podcast-e1557674429764-150x150.png)

## Geodorable

A podcast that can contain everything and everyone about the [geodata](/geodata-what-is-geodata) world.

[https://geodorable.com/](https://geodorable.com/)

![Geodorable Podcast](index.jpg "Geodorable")

## The Mappyist Hour

Geographers and geo-types who talk about how incredible their job is after work.

[http://www.themappyisthour.com/](http://www.themappyisthour.com/)

![The Mappyist Hour Podcast](F_739vLb_400x400.jpg "The Mappyist Hour")

## Speaking of GIS

A podcast by Kurt Towler. The podcast includes interviews with other geospatialists and reviews of conferences.

[https://speakingofgis.com/](https://speakingofgis.com/)

![Speaking of GIS Podcast](ltcN4uOG_400x400.jpg "Speaking of GIS")

## Scene from Above

A podcast with a view of the world of modern [remote sensing] (/en/tags/remote-sensing) and earth observation. Driven by their passion for all grid and geodata, they strive for a mix of news, opinions, discussions and interviews.

[http://scenefromabove.org/](http://scenefromabove.org/)

![Scene from Above Podcast](XIxrl16S_400x400.jpg "Scene from Above")

## Directions Magazine

Every six weeks, new location-based podcasts are released by another geographic information branch.

[https://player.fm/series/directions-magazine-podcasts](https://player.fm/series/directions-magazine-podcasts)

![Directions Magazine Podcast](VrHWMDgk_400x400.jpg "Directions Magazine ")

## Cageyjames & Geobabbler

This monthly podcast by James Fee and Bill Dollins is about how you can use spatial technologies in your workflow.

[https://cng.fireside.fm/](https://cng.fireside.fm/)

![Cageyjames & Geobabbler Podcast](cagey.png "Cageyjames & Geobabbler")

## Geographical Imaginations

Geographical Imaginations Expedition & Institute is a growing public geography initiative for multimedia media that aims to bring together academic and everyday geographic or spatial thinking.

[https://podcasts.apple.com/us/podcast/geographical-imaginations/id1386704057?mt=2](https://podcasts.apple.com/us/podcast/geographical-imaginations/id1386704057?mt=2)

![Geographical Imaginations](170x170bb.jpg)

## Women and Drones

A podcast that serves to make women in the UAS industry better known around the world.

[http://womenanddrones.libsyn.com/](http://womenanddrones.libsyn.com/)

![Women and Drones Podcast](nCObdGxO_400x400.jpg "Women and Drones")