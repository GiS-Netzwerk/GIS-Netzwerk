---
layout: "post"
title: "Free FME-licence for private use"
date: "2019-04-22"
description: "FME (Feature Manipulation Engine) is a powerful and the most used spatial ETL tool for the migration and processing of spatial data and non-spatial data"
category: "GIS"
tags: ["FME", "Geodata"]
image: "/assets/img/fme-lizenz-kostenlos.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

## FME Home license

FME (Feature Manipulation Engine) is a powerful and the most used spatial ETL tool for the migration and processing of [spatial data](/en/gis/geodata-what-are-geodata "What is geodata?") and non-spatial data. The software is very flexible and powerful. It can also handle very large amounts of data without problems.

Feature Manipulation Engine supports over 300 different data sources such as [GIS](/en/gis/geographic-information-system-what-is-gis "What is GIS?") - databases] (PostGIS, MySQL, Oracle, of course also most non-spatial databases), CAD files ( DWG, DXF), raster data, web services, coordinate lists, XML, KML, GML, GeoJSON and much more.

The software is very easy to use and it comes with a nice graphical user interface, in which the source and target data model or format is specified. This also makes complex processing processes very clear.

In between the readers and writers, countless so-called [transformers](https://cdn.safe.com/resources/fme/FME-Transformer-Reference-Guide.pdf "FME Transformers") can be used, with which the data can be processed before being imported into a new data source. This workflow can also be supplemented with Python or SQL scripts.

Safe Software offers FME in three versions:

*   FME Desktop
*   FME Server
*   FME Cloud

That sounds great, but you still have no idea?

You can apply for a free (home) license for FME Desktop at [Free Licenses for Home Use](https://www.safe.com/free-fme-licenses/home-use/ "Free Licenses for Home Use"). If you are a student you can apply for a separate license [here](https://www.safe.com/free-fme-licenses/students/).

**This license is of course only for personal and not commercial projects.**

Submitting the application is very easy. All you have to do is enter your name, your email address, your company if applicable, and how you will use the license. Here it is enough to simply write that you want to get to know the program and of course want to learn it.

As soon as the application has been accepted, you will receive an email with the license key.

On the page [Downloads](https://www.safe.com/support/support-resources/fme-downloads/ "Downloads") you can then download the desktop version and enter the license key you received after the installation process. The license is valid for one year (four months for students) and can be expanded as required.

There is a [Knowledge Base](https://knowledge.safe.com/page/knowledge-base) where you can find thousands of tutorials.

For more complex problems, it is also advisable to take a look at [gis-stackexchange.com](https://knowledge.safe.com/page/knowledge-base).
