---
layout: "post"
title: "GIS- und Geo-database management system options"
date: "2019-04-14"
description: "With the ever increasing amount of data, the question arises at some point how it can be managed effectively. GIS databases are used here."
category: "GIS"
tags: ["SQL", "Database"]
image: "/assets/img/gis-und-geo-datenbanken.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

With the ever increasing amount of data, the question arises at some point how it can be managed effectively. GIS databases are used here.

Spatial databases that are part of a [geographic information system](//en/gis/geographic-information-system-what-is-gis "geographic information system") can be used to store, manage and query [geodata](/en/gis/geodata-what-are-geodata).

## Free and open-source

1. ArangoDB Community Edition

ArangoDB is a user-friendly, powerful open source NoSQL database with a unique combination of functions.

[https://www.arangodb.com](https://www.arangodb.com)

1. PostGIS / PostgreSQL

PostGIS provides spatial objects for the PostgreSQL database that enable the storage and retrieval of information about location and assignment.

[https://postgis.net](https://postgis.net) / [https://www.postgresql.org](https://www.postgresql.org)

1. MariaDB

One of the most popular database servers. Developed by the original developers of MySQL.

[https://mariadb.org/](https://mariadb.org/)

1. MySQL

MySQL is one of the most widely used relational database management systems in the world. It is available as open source software and as a commercial enterprise version for various operating systems.

[https://www.mysql.com/](https://www.mysql.com/)

1. OrientDB

OrientDB is an open source NoSQL database written in Java. OrientDB is a document-oriented database that also has properties of graph databases

[https://www.orientdb.com](https://www.orientdb.com)

1. SQLite / SpatialLite

SpatiaLite is an open source library that extends SQLite with full spatial SQL functions.

[https://www.sqlite.org](https://www.sqlite.org/index.html) / [https://www.gaia-gis.it/fossil/libspatialite](https://www.gaia-gis.it/fossil/libspatialite/index)

## Proprietary

1. Oracle Spatial

[https://www.oracle.com](https://www.oracle.com/technetwork/database/options/spatialandgraph/downloads/index.html)

Oracle Spatial and Graph is a separately licensed component of the Oracle database. The extension is used to store and manage geographic information.