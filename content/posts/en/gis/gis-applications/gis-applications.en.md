---
layout: "post"
title: "GIS applications - Which GIS applications are there?"
date: "2019-02-28"
description: "In order to be able to work with digital maps or information geodata, a geographic information system is used. With GIS, geodata can be recorded, edited, analyzed and displayed appropriately. ."
category: "GIS"
tags: []
image: "/assets/img/gis-software-optionen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

In order to be able to work with digital maps or information ([geodata](/en/gis/geodata-what-are-geodata "geodata")), a [geographic information system (GIS)](/en/gis/geographic-information-system-what-is-gis "What is GIS?") is used. With  GIS, geodata can be recorded, edited, analyzed and displayed appropriately. There are now many good providers of geographic information systems, the two best known of which are probably QGIS (Open Source) and ArcGIS from Esri.

Now you have decided on a GIS, but the question still arises which additional GIS applications (also called industry models) are required or which are available at all. I would like to go into this further in this article.

Basically there are GIS applications for the following industries:

*	Banks
*   Education
*	Infrastructure development
*	Disaster management
*   Agriculture
*	Logistics
*	Marketing
*   Medicine
*	Telecommunications
*	Tourism
*	Crime mapping
*   Traffic
*	Insurance
*	Economic development

For each of these industries, GIS service providers offer different GIS applications and also adapt them individually to the needs of the customers.

In the following, I would like to go into more detail about a few applications, especially for municipalities (GIS).

## Tree cadastre

A tree cadastre supports municipalities and tree care companies in the collection, control and management of tree stands. Trees can be divided into groups of trees and various material data or media can also be added to the trees:

* Number, type, height, crown diameter, degree of sealing, type of soil, damage, maintenance measures carried out or checks and pictures, to name just a few.

The trend here is towards mobile solutions. This means apps that allow you to enter data into the GIS directly on a tablet using controls or maintenance measures. This data is stored online and can then be corrected or revised later in the office.

Development plans / land use plans

Development and land use plans can be easily managed and evaluated in a GIS.

Depending on the legal validity, areas of validity can be displayed in different colors, changes can be linked to the main plan, and text files, such as additions to the articles of association, can be added.

In addition, analog development plans can be prepared (scanned, georeferenced) and displayed in the GIS. This can be done with PDF or CAD files, for example.

As a result, you get a data record for each development plan, to which all associated files and changes are linked, and can present them in an appealing or clear manner.

Real estate cadastre

For example, in the case of a construction project in a certain area, all affected citizens can be identified and written to very easily by automatically creating reports using templates for writing and selecting all citizens in this area (in this area in the GIS).

## Sealing cadastre (split sewage fees)

All sealed areas of a property are determined in a sealing register. This takes place via a previously carried out aerial survey, in which high-resolution images are taken, or via digitization of satellite images.

All parcels are combined into one plot and the sealed areas of these parcels are linked to the plot. This enables municipalities to determine the split rainwater fee.

## Supply networks

### Water

Water supply networks can be managed digitally in a GIS. Hydrant plans can be created automatically, which can be very helpful, for example, for the local fire brigade in an emergency.

Repairs carried out on pipes can be stored digitally, so that you always have an overview of which pipes have already been renovated and which should be renovated in the near future.

### Sewage

Many commissions are legally obliged to keep a channel register.

A channel register is created either from analog data such as plans, which are digitized, or from a measurement that has been carried out beforehand.

In a channel cadastre, data such as posture length, depth, cable diameter material, etc. can be saved, managed and analyzed.

Large corporations now also have wastewater registers for their company premises.

### Electricity / gas / broadband / street lighting

Enables the acquisition, management and analysis of all supply networks.

### Tree and green areas

Allows the construction of a tree and green area register for further planning and maintenance of the inventory.

## Conclusion

Especially in municipalities, the importance of a geographic information system is beyond question. A lot of time is saved due to less administrative work.

The various industry models can be easily combined with each other, making work processes much more efficient.

But also in the private sector, e.g. In the real estate market, in agriculture or in archeology, the advantages of a geographic information system (GIS) are increasingly recognized and used. Mobile apps that are combined with a GIS are particularly popular.

If you want to see more applications, check out GIS-Geography. There you will find [1000 GIS Applications & Uses - How GIS Is Changing the World](https://gisgeography.com/gis-applications-uses/ "1000 GIS Applications & Uses - How GIS Is Changing the World").