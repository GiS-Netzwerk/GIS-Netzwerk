---
layout: "post"
title: "The most famous geospatial and GIS influencers"
date: "2019-03-14"
description: "Here you will find a list of the most famous GIS influencers with links to their web profiles, so that you always stay up to date."
category: "GIS"
tags: []
image: "/assets/img/gis-influencer.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

## Geospatial and GIS Influencers

Also in the area [GIS](/en/gis/geographic-information-system-what-is-gis "What is GIS?") And [Geodata](/en/gis/geodata-what-are-geodata "What is geodata?") There are of course numerous GIS influencers who share their knowledge Spread via social media channels and their blogs or websites and inspire people.

Here you will find a list of the most famous GIS influencers with links to their web profiles, so that you always stay up to date.

But first a Twitter list from CARTO, where you can follow 50 well-known GIS and geography influencers with one click: [CARTO Twitter Guide 2018.](https://twitter.com/CARTO/lists/carto-twitter-guide-2018)

## Top GIS Influencers

1. Anita Graser

![GIS Influencers Anita Graser](https://underdark.files.wordpress.com/2007/03/img_20161121_003528.jpg?w=150&h=150)

_Picture source: [https://anitagraser.com/about/](https://anitagraser.com/about/); [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en_US)_

**Data Scientist und Open-Source-GIS Expertin**
Anita Graser works in Austria as an author, data scientist and open source GIS expert in the fields of algorithms, mobility research, open source GIS solutions, geodata analysis and geodata visualization, as well as spatial big data.

She is part of the QGIS team and has been a QGIS PSC member since 2013. She has a total of 13 years of GIS experience.

Anita Graser has published numerous very well-known QGIS tutorials - such as [QGIS Map Design *](https://amzn.to/2HwGN6A) or [Learning QGIS *](https://amzn.to/2F7J2KS) in the form of books. (You can find the complete list [here *](https://amzn.to/2O3KC4h).) In addition, she teaches distance learning courses at the University of Salzburg for part-time geoinformatics which are part of [UNIGIS](/unigis-weiterbildung-geoinformatik) Tobe offered. You can follow her here:

*   [Twitter](https://twitter.com/underdarkgis?lang=de)
*   [GitHub](https://github.com/anitagraser/)
*   [StackExchange](https://gis.stackexchange.com/users/187/underdark)
*   [Linkedin](https://www.linkedin.com/in/anita-graser-95102530/)
*   [Youtube](https://www.youtube.com/anitagraser/)
*   [Xing](https://www.xing.com/profile/Anita_Graser2/cv)
*   [anitagraser.com](https://anitagraser.com/)

2. Ted MacKinnon

![GIS Influencers Ted MacKinnon](https://gogeomatics.ca/wp-content/uploads/MacKinnon2.jpg)

_Picture source: [https://gogeomatics.ca/author/tmackinnon/](https://gogeomatics.ca/author/tmackinnon/)_

**Certified Geomatics Specialist**

Ted MacKinnon is from Canada and a professional with more than 20 years of experience in the field of [GIS] (/ gis-what-is-a-geographic information system), remote sensing, cartography, LIDAR and GPS surveying in the private sector, in the academic field, in non-profit organizations as well as in the public sector. It inspires the younger [geodata](/geodata-what-ageodata) community to learn more about Canadian geographic information.

*   [Twitter](https://twitter.com/tedmackinnon?lang=de)
*   [Instagram](https://www.instagram.com/t_mackinnon/)
*   [Facebook](https://www.facebook.com/ted.mackinnon)
*   [Linkedin](https://www.linkedin.com/in/tedmackinnon/?originalSubdomain=ca)
*   [Youtube](https://www.youtube.com/channel/UCldWLa9bKxS7KDZlWGImRrw)
*   [Xing](https://www.xing.com/profile/Ted_MacKinnon/cv)
*   [tmackinnon.com](https://tmackinnon.com/)

3. Gretchen Peterson

![GIS Influencers Gretchen Peterson](https://pbs.twimg.com/profile_images/933003884615802880/kqQ_3Su__400x400.jpg)

_Picture source: [https://twitter.com/petersongis?lang=de[(https://twitter.com/petersongis?lang=de)_

**Cartographer, Data Scientist and Product Developer**

Gretchen Peterson is a cartographer, data scientist, and product developer at [PetersonGIS](http://petersongis.com), and an author in Colorado. She is an expert in the inference and visualization of geodata. Gretchen Peterson also creates maps for Tegola. She has already published books such as [Cartographer's Toolkit: Colors, Typography, Patterns *](https://amzn.to/2TFht5u) and [City Maps: A coloring book for adults *](https://amzn.to/2Fc4Ztg) ,

*   [Twitter](https://twitter.com/petersongis?lang=de)
*   [Linkedin](https://www.linkedin.com/in/gretchenpeterson/?locale=de_DE)
*   [Youtube](https://www.youtube.com/channel/UC5JSHHSkGx7Et7RDRZzv0sQ)
*   [gretchenpeterson.com](https://www.gretchenpeterson.com/)

### Other well-known "GIS personalities"

24 other well-known GIS experts with links to their Twitter profiles:

*   [Amanda Taub](http://twitter.com/amandahstaub)
*   [Andy Gup](http://twitter.com/agup)
*   [Alex Chaucer](http://twitter.com/geoparadigm)
*   [Bill Dollins](http://twitter.com/billdollins)
*   [James Fee](http://twitter.com/cageyjames)
*   [Valerie Yakich](http://twitter.com/GeoEntelechy)
*   [Glenn Letham](http://twitter.com/gletham)
*   [GIS Geospatial News](http://twitter.com/gisuser)
*   [Geospatial News](http://twitter.com/geospatialnews)
*   [Kate Chapman](http://twitter.com/wonderchook)
*   [Jim Barry](http://twitter.com/JimBarry)
*   [OpenGeo](http://twitter.com/OpenGeo)
*   [Dave Smith](http://twitter.com/DruidSmith)
*   [Andrew Turner](http://twitter.com/ajturner)
*   [OpenStreetMap](http://twitter.com/openstreetmap)
*   [Dave Bouwman](http://twitter.com/dbouwman)
*   [Don Meltz](http://twitter.com/DonMeltz)
*   [Jesse Rouse](http://twitter.com/kindaspatial)
*   [LindaHecht](http://twitter.com/LindaHecht)
*   [Jason Birch](http://twitter.com/jasonbirch)
*   [Justin C. Houk](http://twitter.com/GEOpdx)
*   [Adam Estrada](http://twitter.com/GeoDAWG)
*   [Randal Hale](http://twitter.com/rjhale)
*   [Joe Francica](http://twitter.com/joefrancica)

Isn't that still enough? Here you can find even more GIS influencers (thanks to opengeo): [Top 100 Geospatial Influencer](https://docs.google.com/spreadsheet/ccc?key=0Ana1iJKeRrCwdHhzdEt1d0ZuQVB2Y3NGc3dWVG5yTFE&usp=sharing)