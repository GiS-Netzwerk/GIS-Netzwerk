---
layout: "post"
title: "Geographie and GIS blogs"
date: "2019-03-01"
description: ""
category: "GIS"
tags: []
image: "/assets/img/geographie-und-gis-blogs.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

## GIS and Geo Blogs

Of course, to find out new things, I take a look at one or the other website that deals with [GIS](/en/gis/geographic-information-system-what-is-gis "What is GIS?") Or geoinformatics in general. You can find them here:

1. GIS Lounge

Maps and GIS by Caitlin Dempsey Morais. She has been blogging about GIS for more than 20 years.

[https://www.gislounge.com/](https://www.gislounge.com/)

1. GIS Geography

Blog about GIS and Geographie.

[www.gisgeography.com](https://gisgeography.com/)

1. Free and Open Source GIS Ramblings

Anita Graser's blog about QGIS, open source, analysis and simulation.

[https://anitagraser.com/](https://anitagraser.com/)

1. Geospatial World

How does location localization affect us?

[https://www.geospatialworld.net](https://www.geospatialworld.net)

1. Geoawesomeness

Blog about GIS, geodata and everything that goes with it.

[www.geoawesomness.com](https://geoawesomeness.com/)

1. GISTimes

GIStimes is for everything that happens on the geodata market.

[http://www.gistimes.com](http://www.gistimes.com)

1. GIS Professional

GIS news and articles about GNSS, Big Data, Addressing, BIM, and Smart Cities.

[https://www.gis-professional.com/news](https://www.gis-professional.com/news)

1. Geospatial-solutions

[http://geospatial-solutions.com/](http://geospatial-solutions.com/)

1. Google Maps Blog

Google Maps blog.

[https://www.blog.google/products/maps/](https://www.blog.google/products/maps/)

1. Carto

SaaS provider CartoDB also runs a very interesting GIS blog.

[https://carto.com/](https://carto.com/)

1. Reddit r/gis GIS-Community

A Reddit community about geographic information systems.

[https://www.reddit.com/r/gis/](https://www.reddit.com/r/gis/)

1. Benjaminspaulding

Geodata, analysis, programming.

[https://www.benjaminspaulding.com/](https://www.benjaminspaulding.com/)

1. GISuser

GIS and technology news for mapping experts.

[http://gisuser.com/](http://gisuser.com/)

1. Esri Newsroom

Esri's blog.

[https://www.esri.com/about/newsroom/blog](https://www.esri.com/about/newsroom/blog)

1. ThinkGeoBlog

GIS themes for .NET developers.

[http://blog.thinkgeo.com/](http://blog.thinkgeo.com/)

There is also a much larger list of links to GIS Blogs on Wiki.GIS (http://wiki.gis.com/wiki/index.php/List_of_GIS-related_Blogs). By the way, Wiki.GIS is a very extensive GIS encyclopedia.