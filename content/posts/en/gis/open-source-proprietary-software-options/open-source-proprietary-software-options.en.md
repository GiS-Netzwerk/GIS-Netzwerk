---
layout: "post"
title: "GIS software options - free, open-source and proprietary"
date: "2018-04-23"
description: "In this post you will find a list of several free, open-source and proprietary GIS-software options."
category: "GIS"
tags: []
image: "/assets/img/gis-software-optionen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

In this post you will find a list of several free, open-source and proprietary GIS-software options.

## Free and open-source desktop-GIS software

**1. [QGIS](https://www.qgis.org "QGIS")**

QGIS is a free and open-source geographic information system.
With QGIS you can create, modify, visualize and analyze spatial data on Windows, Mac, Linux, BSD and Android.

**2. [GRASS GIS](https://grass.osgeo.org/ "GRASS GIS")**

GRASS GIS is a hybrid, modular geographic information system software with grid and vector-oriented functions.

**3. [SAGA GIS](http://www.saga-gis.org/ "SAGA GIS")**

Software for automated geoscientific analysis. The SAGA project is mainly developed at the Department of Geography at the University of Hamburg.

**4. [JUMP GIS/OpenJUMP](http://www.openjump.org/ "JUMP GIS/OpenJUMP")**

Java based open-source GIS.

**5. [GeoDa](https://geodacenter.github.io/ "GeoDa")**

GeoDa is a free and open-source GIS-software and serves as an introduction to geodata analysis.

**6. [gvSIG](http://www.gvsig.com/en "gvSIG")**

Open-Source Desktop, Online und Mobile GIS.

**7. [MapmakerPro](http://www.mapmaker.com/v4/Map%20maker%204.Pro.htm "MapmakerPro")**

MapMaker is aimed at specialists who need to create maps. For example foresters, archaeologists, emergency services, etc..

**8. [DIVA GIS](http://www.diva-gis.org/ "DIVA GIS")**

DIVA-GIS is a free GIS for mapping and analyzing geographic data.

**9. [TerraLib](http://www.dpi.inpe.br/terralib_previous/index.php "TerraLib")**

TerraLib is an open source GIS software library that supports the development of custom geographic applications.

**10. [Kalypso](https://sourceforge.net/projects/kalypso/ "Kalypso")**

Kalypso is an open source modeling program. The focus is on numerical simulations in water management and ecology.

**11. [OrbisGIS](http://orbisgis.org/ "OrbisGIS")**

OrbisGIS is a cross-platform open source GIS developed by and for research.

**12. [OzGIS](https://sourceforge.net/projects/ozgis/ "OzGIS")**

OzGIS is a GIS for analyzing and displaying spatial statistics.

**13. FalconView**

FalconView is a GIS developed by the Georgia Tech Research Institute.

**14. [ILWIS](https://www.itc.nl/ilwis/ "ILWIS")**

The Integrated Land and Water Information System (ILWIS) is a desktop-based GIS and remote sensing software that was developed by ITC up to Release 3.3 in 2005.

**15. [MapWindow GIS](https://www.mapwindow.org/ "MapWindow GIS")**

MapWindow GIS is an open source GIS desktop application that is used by a large number of users and organizations around the world.

**16. [Whitebox GAT](https://www.uoguelph.ca/~hydrogeo/Whitebox/ "Whitebox GAT")**

Whitebox Geospatial Analysis Tools is an open source, cross-platform geospatial information system and remote sensing software package.

**17. [Capaware](http://www.capaware.org/ "Capaware")**

3D-world-viewer.

**18. [Generic Mapping Tools](http://gmt.soest.hawaii.edu/ "Generic Mapping Tools")**

The Generic Mapping Tools are a collection of free software for creating geological or geographical maps and diagrams.

## Kostenpflichtige Desktop GIS Software

**19. ArcGIS, ArcView**

ArcGIS is the generic term for various GIS software products from Esri.

**20. AutoCAD Map3D**

AutoCAD Map3D from Autodesk is a GIS software solution and offers extensive access to all CAD and GIS data and enables its creation and editing.

**21. Aquaevo GIS**

Aquaveo is a GIS software for modeling environmental and water resources.

**22. Bentley Map**

**23. Cadcorp**

Cadcorp's GIS and Web Mapping Software are GIS software products for the creation, analysis and data management of geodata.

**24. Conform**

Conform is a GIS software for merging, visualizing, editing and exporting 3D environments for urban planning, games and simulations.

**25. Dragon / ips**

Dragon / ips is a remote sensing image processing software.

**26. ENVI**

The ENVI image analysis software is used by GIS experts, remote sensing scientists and image analysts to extract meaningful information from images to help them make better decisions.

**27. ERDAS IMAGINE**

ERDAS IMAGINE is software for evaluating remote sensing data, especially graphics and photos.

**28. Field-Map**

Field-Map is a proprietary integrated tool for programmatic field data acquisition by IFER - Monitoring and Mapping Solutions, Ltd. It is mainly used for mapping forest ecosystems and for data collection during field analysis.

**29. Geosoft**

GEOSOFT is one of Germany's leading developers of geodetic computing and organizational software for private and public surveying agencies.

**30. GeoTime**

GeoTime is a geodata analysis software that enables the visual analysis of events over time. The third dimension adds time to a two-dimensional map so that users can see changes in time series data.

**31. Global Mapper**

Geographic information system with distance and area calculation; offers an integrated scripting language, 3D display and GPS tracks.

**32. Golden Software**

Surfer and Mapviewer are two software solutions with a variety of mapping and adaptation options and support any geodata format (including LiDAR data), 3D visualization, as well as volume / distance / area calculations.

**33. Intergraph**

GeoMedia is a GIS software from Intergraph. GeoMedia is a software product family with desktop GIS, web GIS and is mainly aimed at municipalities.

**34. Manifold System**

Manifold System is software for the management of digital maps. Digital maps and remote sensing data can be easily edited.

**35. MapInfo**

MapInfo Professional is a geographic information system software from the US company MapInfo Corporation.

**36. Maptitude**

Maptitude is a mapping software program created by Caliper Corporation that allows users to view, edit, and integrate maps. The software and technology are designed to facilitate the geographic visualization and analysis of contained data or user-defined external data.

**37. Netcad**

NETCAD GIS is a CAD and GIS software that supports international standards and was designed for users of engineering and geographic information systems.

**38. RegioGraph**

RegioGraph is a geomarketing software specializing in questions in the areas of marketing, sales, controlling, logistics and corporate strategy.

**39. RIWA GIS Zentrum**

The RIWA GIS Zentrum is a powerful, web-based geographic information system that has been used in numerous municipal administrations and industrial companies for many years.

**40. Smallworld**

Smallworld GIS is the professional geographic information system for network operators in the energy and water industries.

**41. TNTmips**

TNTmips is a geospatial data analysis system that offers a fully featured GIS, RDBMS and automated image processing system with CAD, TIN, surface modeling, map layout and innovative data publishing tools.

**42. TerrSet ( IDRISI )**

TerrSet is an integrated geographic information system and remote sensing software for monitoring and modeling the Earth system.

**43. [Google Earth Pro](https://www.google.de/earth/download/gep/agree.html "Google Earth Pro")**

## Online GIS

**44. [Bing Maps](http://www.bing.com/maps/ "Bing Maps")**

Bing Maps is an online map service from Microsoft, through which various spatial data can be viewed and spatial services can be used. It is a further development of the MSN Virtual Earth and is part of the Bing search engine.

**45. [Google Maps](http://maps.google.de/ "Google Maps")**

Google Maps is an online map service from the US company Google LLC. The surface of the earth can be viewed as a road map or as an aerial or satellite image, with locations of institutions or known objects also being displayed. The service started on February 8, 2005.

**46. [NASA World Wind](http://worldwind.arc.nasa.gov/java/ "NASA World Wind")**

NASA World Wind is an open source software that enables satellite and aerial images to be displayed on a virtual globe combined with elevation data and to be zoomed in anywhere in the world in 3D graphics and viewed freely from all sides.

**47. [OpenStreetMap](http://www.openstreetmap.org/ "OpenStreetMap")**

OpenStreetMap is a free project, which collects freely usable geodata, structures it and keeps it in a database for everyone to use. This data is under a free license, the Open Database License.

**48. [Wikimapia](http://www.wikimapia.org/ "Wikimapia")**

Wikimapia is a web interface that combines maps with a restricted wiki system without hypertext functions. It allows the user to add information in the form of a note to any position on the earth.