---
layout: "post"
title: "Satellite imagery download - Remote sensing"
date: "2019-05-20"
description: "For those who do not yet know the corresponding contact points for current satellite images, there are a few further links from which you can download global satellite images for the most part free of charge."
category: "GIS"
tags: ["Remote sensing", "Geodata"]
image: "/assets/img/hochaufloesende-satellitenbilder-downloaden.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

For those who do not yet know the corresponding contact points for current satellite images, there are a few further links from which you can download global satellite images for the most part free of charge.

## Download satellite images

1. **Copernicus Open Access Hub**

The [Copernicus Open Access Hub](https://scihub.copernicus.eu/dhus) (Sentinels Scientific Data Hub) provides free and open access to Sentinel-1, Sentinel-2 and Sentinel-5P products.

Sentinel data is also available through Copernicus Data and Information Access Services (DIAS) on multiple platforms.

[https://www.copernicus.eu/de/datenzugriff/dias](https://www.copernicus.eu/de/datenzugriff/dias)

The Sentinel satellites are satellites of [ESA](https://www.esa.int/ESA) as part of the Copernicus program. This program was created in cooperation with the EU and is intended to enable earth observation, particularly in the areas of the environment, transport, economy and security policy.

All Sentinel data available through the Sentinel Data Hub is subject to the [Legal Notice on the use of Copernicus Sentinel Data and Service Information](https://sentinels.copernicus.eu/documents/247904/690755/Sentinel_Data_Legal_Notice).

2. **GEOSS Portal**

The [GEOSS Portal](http://www.geoportal.org/) is a map-based online user interface with which users can download earth observation data and resources from various providers from all over the world.

This portal is also operated by the European Space Agency (ESA).

3. **Worldview - NASA**

[NASA Worldview](https://worldview.earthdata.nasa.gov/) offers an interactive user interface to search for high-resolution and global satellite images. Thematic pictures of forest fires, air quality, flood monitoring and more are also offered.

4. **European Space Imaging**

[European Space Imaging](https://www.euspaceimaging.com/) is the leading provider of VHR (Very High Resolution) satellite images for Europe, North Africa and the CIS countries.

5. **GloVis**

Since 2001, the [USGS Global Visualization Viewer](https://glovis.usgs.gov/) (GloVis) has been available for access to remote sensing data. In 2017, it was redesigned to take account of changing Internet technologies. With easy-to-use navigation tools, users can instantly view and download scenes.

6. **GeoStore - Airbus Defence and Space**

In the [GeoStore](https://www.intelligence-airbusds.com/geostore/), operated by AIRBUS, high-resolution and current satellite images can be ordered.

7. **EOWEB GeoPortal - DLR**

[EOWEB GeoPortal](https://eoweb.dlr.de/egp/) (EGP) is a multi-mission web portal for interactive access to the DLR earth observation database.