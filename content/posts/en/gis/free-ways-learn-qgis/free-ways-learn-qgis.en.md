---
layout: "post"
title: "Free ways to learn QGIS"
date: "2019-04-29"
description: "QGIS is the most commonly used free and open source GIS software. QGIS is constantly being developed by volunteers and fundraising and is a very good alternative to priority GIS software."
category: "GIS"
tags: ["QGIS", "Python", "API"]
image: "/assets/img/qgis-lernen.jpg"
caption: "Photo by USGS on Unsplash"
author: "Max Dietrich"
---

[QGIS](https://www.qgis.org/de/site/ "QGIS") is the most commonly used free and open source [GIS software] (/open-source-proprietary-software-options "GIS software options"). QGIS is constantly being developed by volunteers and fundraising and is a very good alternative to priority GIS software.

Especially for reasons of cost, QGIS is the ideal entry for newcomers to the "[GIS world](/en/gis/geographic-information-system-what-is-gis "What is GIS?")". If you are looking for ways to familiarize yourself with QGIS or to get started with GIS in general, you have come to the right place.

## Learn QGIS

A [QGIS manual](https://docs.qgis.org/3.4/de/docs/user_manual/ "QGIS manual") is of course offered on the official **QGIS website**. The manual basically describes everything from installation, user interface, data processing to analysis tools and the official QGIS manual is a good introduction.

Another [QGIS-Manual](https://www.qgistutorials.com/en/# "QGIS-Manual") is also available from **Ujaval Gandhi**. The absolute basics are also described here. The core topics are spatial analysis, the automation of workflows and web mapping.

This manual is therefore aimed more at advanced users.

You can also find great video tutorials on [YouTube](https://www.youtube.com/channel/UCxs7cfMwzgGZhtUuwhny4-Q "video tutorials") by **Klas Karlsson**. All topics, i.e. covered from basics to advanced workflows.

So if you want to learn more visually, here is a great selection of videos that are offered in English.

## Python scripting in QGIS

If you are interested in Python scripting or general QGIS, it is worth taking a look at [Anita Grasers](https://anitagraser.com/) tutorial series. There, "non-programmers" [Scripting with PyQGIS](https://anitagraser.com/pyqgis-101-introduction-to-qgis-python-programming-for-non-programmers/ "Scripting with PyQGIS") are brought closer.

Her series currently includes ten modules, as well as links to other PyQGIS resources. The topic of PyGIS is structured practically like an online course, which you can take in pieces.

In addition to [PyGIS101](https://anitagraser.com/pyqgis-101-introduction-to-qgis-python-programming-for-non-programmers/ "PyGIS101") you will also find a lot of information about [Movement data in GIS](https://anitagraser.com/movement-data-in-gis/ "Movement data in GIS").