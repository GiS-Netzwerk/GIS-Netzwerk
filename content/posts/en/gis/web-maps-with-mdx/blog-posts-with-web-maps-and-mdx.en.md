---
layout: "post"
title: "Amplify your blog posts with Web-Maps and MDX"
date: "2020-06-24"
description: "For those who do not yet know the corresponding contact points for current satellite images, there are a few further links from which you can download global satellite images for the most part free of charge."
category: "GIS"
tags: ["Web-GIS", "GatsbyJS", "Leaflet", "OpenLayers"]
image: "/assets/img/amplify-your-blog-posts-with-web-maps-and-mdx.jpg"
caption: "by USGS on Unsplash"
author: "Max Dietrich"
---
import WebMapMDX from "./WebMap.js"
import GoTMap from "./GoTMap.js"

With [MDX](https://mdxjs.com/ "MDX") you have the opportunity to write JSX inside Markdown, which also allows you to import every component you want into your blog posts and you dont even have to change any of your existing content. Awsome right?

> Yes, awsome! 👍

If you haven't started with MDX yet i would suggest to have a look at [Adding Components to Markdown with MDX](https://www.gatsbyjs.org/docs/mdx/ "Adding Components to Markdown with MDX"). 
If you feel ready to tackle that challenge there is a guide to [convert your existing site to use MDX](https://www.gatsbyjs.org/blog/2019-11-21-how-to-convert-an-existing-gatsby-blog-to-use-mdx/ "convert your existing site to use MDX"). It's actually super easy.

So your blog/website is now ready to use MDX. All you need now is some Javascript to create a simple Web-Map and import it into your blog post.

> The most common used Javascript-Libraries for creating Web-Maps are [Leafleat(-React)](https://react-leaflet.js.org/ "Leaflet-React") and [OpenLayers](https://taylor.callsen.me/using-reactflux-with-openlayers-3-and-other-third-party-libraries/ "OpenLayers React").

For this example i am going to use Leaflet. My posts are organized like the following:

![blog/post structure](./blog-posts-with-web-maps-and-mdx.en.md.png "blog/post structure")

As you can see there is a "webMap.js which is storing the code for the web-map.

A simple Leaflet map looks like this.

```js
import React from "react"
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import 'leaflet/dist/leaflet.css';

class WebMapMDX extends React.Component {    
    constructor() {
    super();
    this.state = {
      markerPosition: [51.8, 9.0],
      center: [51.8, 9.0],
      zoom: 5,
    };
  }

    render() {

      return (
      <div className="react-leaflet-demo-container">
              {typeof window !== 'undefined' &&
                  <Map 
                  center={this.state.center} 
                  zoom={this.state.zoom} 
                  >
                  <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
                    url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                  />
                  <Marker position={this.state.markerPosition}>
                    <Popup>
                      👋
                    </Popup>
                  </Marker>
                </Map>
              }
      </div>
    );
  }
}

export default WebMapMDX;

```
It's just the basic OpenStreetMap Map with a marker at a specific position.
If you want to dive deeper into Leaflet-React you should have a look at the [examples provided by React-Leaflet](https://react-leaflet.js.org/docs/en/examples "React-Leaflet Eexamples").

Now there is only one thing left.

With ```import WebMapMDX from "./webMap.js"``` you can import the the map into your post and place it anywhere you like with ```<WebMapMDX/>```.
```md
...
caption: "by USGS on Unsplash"
author: "Max Dietrich"
---
import WebMapMDX from "./webMap.js"

With [MDX](https://mdxjs.com/ "MDX") you have the ...

<WebMapMDX/>
```

The result is looking like this:
<WebMapMDX/>

If you dont like the "basic" OpenStreetMap you could also have a look at for example [Leaflet-providers](https://leaflet-extras.github.io/leaflet-providers/preview/ "Leaflet-providers") to find different basemaps or you could just show a map of Westeros with:
```js
import React, { Component } from "react";
import L from "leaflet";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import "./Map.css";


class GoTMap extends Component {
  componentDidMount() {
    const map = this.leafletMap.leafletElement;
    const results = new L.LayerGroup().addTo(map);

  }

  render() {
    const center = [0, 30];
    const northofthewall = center
    return (
      <>
        <Map
          center={center}
          northofthewall={northofthewall}
          zoom="4.5"
          ref={m => {
            this.leafletMap = m;
          }}
        >
          <TileLayer
            attribution='Map by <a href="https://public.carto.com/">Carto</a>'
            url="https://cartocdn-gusc.global.ssl.fastly.net//ramirocartodb/api/v1/map/named/tpl_756aec63_3adb_48b6_9d14_331c6cbc47cf/all/{z}/{x}/{y}.png" />
          <Marker position={northofthewall}>
            <Popup>
                Hold the door!
            </Popup>
          <Marker/>
        </Map>
        
      </>
    );
  }
}

export default GoTMap;
```
<GoTMap/>

