import React from "react"
import { Map, TileLayer, Marker, Popup, GeoJSON } from "react-leaflet";
import 'leaflet/dist/leaflet.css';

class WebMapMDX extends React.Component {    
    constructor() {
    super();
    this.state = {
      markerPosition: [51.8, 9.0],
      center: [51.8, 9.0],
      zoom: 5,
    };
  }

    render() {

      return (
      <div className="react-leaflet-demo-container">
              {typeof window !== 'undefined' &&
                  <Map 
                  center={this.state.center} 
                  zoom={this.state.zoom} 
                  attribution="false"
                  >
                  <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
                    url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                  />
                  <Marker position={this.state.markerPosition}>
                    <Popup>
                    👋
                    </Popup>
                  </Marker>
                </Map>
              }
      </div>
    );
  }
}

export default WebMapMDX;

