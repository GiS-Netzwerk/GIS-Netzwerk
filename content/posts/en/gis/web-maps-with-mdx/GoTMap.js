import React, { Component } from "react";
import L from "leaflet";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import "./Map.css";


class GoTMap extends Component {
  componentDidMount() {
    const map = this.leafletMap.leafletElement;
    const results = new L.LayerGroup().addTo(map);

  }

  render() {
    const center = [0, 30];
    const northofthewall = [45,5]
    return (
      <>
         <Map
          center={center}
          northofthewall={northofthewall}
          zoom="4.5"
          ref={m => {
            this.leafletMap = m;
          }}
        >
          <TileLayer
            attribution='Map by <a href="https://public.carto.com/">Carto</a>'
            url="https://cartocdn-gusc.global.ssl.fastly.net//ramirocartodb/api/v1/map/named/tpl_756aec63_3adb_48b6_9d14_331c6cbc47cf/all/{z}/{x}/{y}.png" />
          <Marker position={northofthewall}>
            <Popup>
                Hold the door!
            </Popup>
          </Marker>
        </Map>
        
      </>
    );
  }
}

export default GoTMap;
