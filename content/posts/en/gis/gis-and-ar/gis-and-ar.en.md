---
layout: "post"
title: "GIS and Augmented Reality (AR)"
date: "2019-04-11"
description: "The current location is determined via satellite navigation, real-time localization and via the connection to a spatial database, geodata is displayed at the current location. This can create significant added value in many scenarios."
category: "GIS"
tags: ["AR"]
image: "/assets/img/gis-und-ar.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

Augmented Reality supplements reality with virtual objects and information.

The current location is determined via satellite navigation, real-time localization and via the connection to a spatial database, geodata is displayed at the current location. This can create significant added value in many scenarios.

## GIS and AR

Many already know AR from the smartphone game [Pokémon GO](https://de.wikipedia.org/wiki/Pok%C3%A9mon_Go), which by the way is on [OpenStreetMap](https://www.openstreetmap.org/) Data based. Current smartphones can now also place objects in the live image of the camera.

All modern cities and towns have extensive and complex underground supply networks, such as water, sewage, electricity, natural gas, telephone or broadband lines. The more facilities run underground, the more complicated it becomes to maintain the entire supply network.

Local workers find it difficult to determine the location of individual underground objects. You need maps with outdated data, which are often no longer accurate. This often leads to unintentional damage to buried lines.

With the help of a current measurement of existing and newly installed lines, exact geodata can be created, which is stored in GIS databases and displayed in a geographic information system.

The Austrian GIS provider Grintec presented the mobile GIS Augview at INTERGEO 2016. The classic map view from a geographic information systemIs connected to the augmented reality view and geodata is visible in 3D.


Installation parts, sliders or line data for a water network that would otherwise be hidden underground can be shown live in the camera image of a smartphone or tablet. <sup> [1] </sup>

Even in the event of flooding, all underground lines can be precisely located.

New planned houses can be shown in 3D on the future property, which enables a realistic presentation of the planning.

All attributes for the objects can also be queried or edited. The data is provided online via a geo-web server, which is why you need a stable internet connection.

Esri has also added augmented reality to its product range.

Especially with 3D city models, GIS has enormous potential in connection with augmented reality. By integrating real-time information in geodata, many problems can be solved more efficiently.

Swell:

<sup>[1]</sup> Grindtec: [https://www.grintec.com/Augview[(https://www.grintec.com/Augview)