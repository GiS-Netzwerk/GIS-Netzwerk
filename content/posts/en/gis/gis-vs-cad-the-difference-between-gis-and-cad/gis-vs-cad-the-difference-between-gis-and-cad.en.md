---
layout: "post"
title: "GIS vs CAD - The difference between GIS and CAD"
date: "2019-02-15"
description: ""
category: "GIS"
tags: ["CAD", "Geodata"]
image: "/assets/img/cad-gis-unterschied.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
author: "Max Dietrich"
---

## The difference between GIS and CAD

[Geographic information system](/en/gis/geographic-information-system-what-is-gis "What is GIS?") (GIS) describes a system for the display and processing of geodata, ie data to which a spatial position has been assigned. With a geographic information system, complex issues can be presented, presented and analyzed in a structured manner.

With CAD systems you can create digital content and/or model it graphically. These are usually plans or drawings, as well as 3D models.

Since the projected data in both systems should be related to the real world, different scales, sizes and levels of detail are used. In GIS this is regulated by different display models, where it is defined which objects are displayed under which conditions. In CAD, standards play a more important role. The main differences between CAD and GIS are:

* [GIS data or geodata](/geodata-what-are-geodata "GIS data or geodata") need a local reference in contrast to CAD data
* In GIS, the focus is on the visualization, maintenance and analysis of data;
* In CAD, a very precise representation plays a more important role with the help of the plans e.g. Components can be produced
* GIS data is very diverse and a wide variety of data formats and sources can come together
* CAD data are mainly saved as DXF or DWG files
* A GIS is much more efficient and flexible in data management than a CAD system

Both systems can also be used in combination because they complement each other perfectly.