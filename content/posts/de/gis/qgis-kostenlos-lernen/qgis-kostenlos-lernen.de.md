---
layout: "post"
title: "QGIS kostenlos lernen"
date: "2019-04-29"
description: "QGIS ist die am häufigsten verwendete freie und Open Source-GIS-Software. QGIS wird ständig von Freiwilligen und durch Fundraising weiterentwickelt und stellt eine sehr gute Alternative zu prioritärer GIS-Software dar."
category: "GIS"
tags: ["QGIS", "Python", "API"]
image: "/assets/img/qgis-lernen.jpg"
caption: "Photo by USGS on Unsplash"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

[QGIS](https://www.qgis.org/de/site/ "QGIS") ist die am häufigsten verwendete freie und Open Source-[GIS-Software](/gis/gis-software-optionen "GIS-Software Optionen"). QGIS wird ständig von Freiwilligen und durch Fundraising weiterentwickelt und stellt eine sehr gute Alternative zu prioritärer GIS-Software dar.

Vorallem aus kostengründen ist QGIS der ideale Einstieg für Neulinge in die "[GIS-Welt](/gis/was-ist-gis "Was ist GIS?")". Falls du nach Wegen suchst, wie du dich mit QGIS vertraut machen oder allgemein in die Thematik GIS einsteigen kannst, bist du hier richtig.

## [](#qgis-lernen)QGIS lernen

Auf der offiziellen **QGIS-Website** wird natürlich ein [QGIS-Handbuch](https://docs.qgis.org/3.4/de/docs/user_manual/ "[QGIS-Handbuch") angeboten. In dem Handbuch ist eigentlich von der Installation, Benutzeroberfläche, Bearbeitung von Daten bis zu Analysewerkzeugen alles grundlegend beschrieben und das offizielle QGIS-Handbuch bietet somit einen guten Einstieg. Dieses Handbuch gibt es in deutsch und englisch.

Ein anderes [QGIS-Handuch](https://www.qgistutorials.com/en/# "QGIS-Handuch") gibt es auch von **Ujaval Gandhi**. Auch hier werden die absoluten Basics beschrieben. Weiterführend sind hier die Kernthemen Räumliche Analysen, die Automatisierung von Workflows, sowie Web-Mapping.

Dieses Handbuch richtet sich also eher an fortgeschrittene Anwender.

Auch auf Youtube findest du klasse [Video-Tutorials](https://www.youtube.com/channel/UCxs7cfMwzgGZhtUuwhny4-Q "Video-Tutorials") von **Klas Karlsson**. Es werden hier ebenfalls alle Themen, d.h. von Grundlagen bis zu fortgeschrittenen Workflows abgedeckt.

Wer also eher visuell lernen möchte, hat hier ein super Angebot an Videos, die auf Englisch angeboten werden.

## [](#python-scripting-in-qgis)Python-Scripting in QGIS

Wenn du dich für Python-Scripting oder allgemein QGIS interessierst, lohnt es sich einen Blick auf [Anita Grasers](https://anitagraser.com/) Tutorial-Reihe zu werfen. Dort wird "Nicht-Programmierern" [Scripting mit PyQGIS](https://anitagraser.com/pyqgis-101-introduction-to-qgis-python-programming-for-non-programmers/ "Scripting mit PyQGIS") näher gebracht.

Ihre Serie umfasst derzeit zehn Module, sowie Links zu weiteren PyQGIS-Ressourcen. Das Thema PyGIS ist hier praktisch wie ein Online-Kurs aufgebaut, den du dir stückchenweise zu Gemüte führen kannst.

Neben [PyGIS101](https://anitagraser.com/pyqgis-101-introduction-to-qgis-python-programming-for-non-programmers/ "PyGIS101") findest du auch viele Informationen zu [Movement data in GIS](https://anitagraser.com/movement-data-in-gis/ "Movement data in GIS").