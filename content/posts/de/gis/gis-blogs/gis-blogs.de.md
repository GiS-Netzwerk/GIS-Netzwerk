---
layout: "post"
title: "Geographie und GIS Blogs"
date: "2019-03-01"
description: ""
category: "GIS"
tags: ["Blogs"]
image: "/assets/img/geographie-und-gis-blogs.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

## [](#gis-und-geo-blogs)GIS und Geo Blogs

Um Neues zu erfahren schaue ich natürlich auf den ein oder anderen Websiten vorbei, die sich mit [GIS](/gis/was-ist-gis "Was ist GIS?") oder Geoinformatik allgemein beschäftigen. Diese findest du hier:

### [](#1-gis-lounge)1\. GIS Lounge

Maps and GIS von Caitlin Dempsey Morais. Sie bloggt bereits seit mehr als 20 Jahren über GIS.

[https://www.gislounge.com/](https://www.gislounge.com/)

### [](#2-gis-geography)2\. GIS Geography

Blog über GIS und Geographie.

[www.gisgeography.com](https://gisgeography.com/)

### [](#3-gis-news)3\. GIS-News

Ein GIS-News Blog von Dr. Franz-Josef Behr, der an der Hochschule für Technik Stuttgart unterrichtet.

[http://www.gis-news.de/](http://www.gis-news.de/)

### [](#4-free-and-open-source-gis-ramblings)4\. Free and Open Source GIS Ramblings

Ein Blog von Anita Graser über QGIS, Open-Source, Analysen und Simulationen.

[https://anitagraser.com/](https://anitagraser.com/)

### [](#5-osmblog)5\. OSMBlog

Berichte und Neuigkeiten rund um [OpenStreetMap](http://openstreetmap.de), ​die freie Wiki-Weltkarte.

[https://blog.openstreetmap.de/](https://blog.openstreetmap.de/)

### [](#6-geospatial-world)6\. Geospatial World

Wie wirkt sich Standort-Lokalisierung auf uns aus?

[https://www.geospatialworld.net](https://www.geospatialworld.net)

### [](#7-gispoint)7\. GIS.Point

Blog vom Herbert Wichmann Verlag über Geoinformatik/GIS, Geodäsie/Vermessung, Photogrammetrie/Fernerkundung, sowie Verkehrsplanung.

[https://gispoint.de/news.html](https://gispoint.de/news.html)

### [](#8-geoawesomeness)8\. Geoawesomeness

Blog über GIS, Geodaten und allem was dazugehört.

[www.geoawesomness.com](https://geoawesomeness.com/)

### [](#9-geobranchende)9\. GEObranchen.de

Geobranchen.de ist nicht wirklich ein Blog, aber bietet sehr umfassende und hilfreiche Verzeichnisse zu GIS-Jobs, -Firmen, -Verbände und -Events an.

[https://www.geobranchen.de/](https://www.geobranchen.de/)

### [](#10-gistimes)10\. GISTimes

GIStimes ist für alles, was sich auf dem Geodatenmarkt abspielt.

[http://www.gistimes.com](http://www.gistimes.com)

### [](#11-gis-professional)11\. GIS Professional

GIS-News und Beiträge über GNSS, Big Data, Addressing, BIM, sowie Smart Cities.

[https://www.gis-professional.com/news](https://www.gis-professional.com/news)

### [](#12-geospatial-solutions)12\. Geospatial-solutions

[http://geospatial-solutions.com/](http://geospatial-solutions.com/)

### [](#13-google-maps-blog)13\. Google Maps Blog

Blog von Google Maps.

[https://www.blog.google/products/maps/](https://www.blog.google/products/maps/)

### [](#14-carto)14\. Carto

Der SaaS-Anbieter CartoDB betreibt auch einen sehr interessanten GIS-Blog.

[https://carto.com/](https://carto.com/)

### [](#15-reddit-rgis-gis-community)15\. Reddit r/gis GIS-Community

Eine Reddit Community über Geoinformationssysteme.

[https://www.reddit.com/r/gis/](https://www.reddit.com/r/gis/)

### [](#16-benjaminspaulding)16\. Benjaminspaulding

Geodaten, Analysen, Programmierung.

[https://www.benjaminspaulding.com/](https://www.benjaminspaulding.com/)

### [](#17-gisuser)17\. GISuser

Neuigkeiten aus dem Bereich GIS und Technologie für Mapping-Experten.

[http://gisuser.com/](http://gisuser.com/)

### [](#18-esri-newsroom)18\. Esri Newsroom

Der Blog von Esri.

[https://www.esri.com/about/newsroom/blog](https://www.esri.com/about/newsroom/blog)

### [](#19-thinkgeoblog)19\. ThinkGeoBlog

GIS-Themen für .NET Developer.

[http://blog.thinkgeo.com/](http://blog.thinkgeo.com/)

Außerdem gibt es auf Wiki.GIS eine noch viel größere Linkliste zu GIS Blogs [List of GIS-related Blogs](http://wiki.gis.com/wiki/index.php/List_of_GIS-related_Blogs)). Diese sind ebenfalls vorwiegend englischsprachig. Wiki.GIS ist übrigens eine sehr umfangreiche GIS Enzyklopädie.

Auch interessant:

*   [10 Geo und GIS Podcasts](https://www.gis-netzwerk.com/nuetzliche-links/10-geo-und-gis-podcasts-um-auf-dem-aktuellen-stand-zu-bleiben/)