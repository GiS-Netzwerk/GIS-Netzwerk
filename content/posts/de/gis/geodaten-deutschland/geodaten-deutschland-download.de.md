---
layout: "post"
title: "Geodaten Deutschland online - Download kostenlos"
date: "2018-04-23"
description: "Hier findest du kostenlose online Geodaten für alle Bundesländer in Deutschland, Satellitenbilder, DEM und mehr."
category: "GIS"
tags: ["Geodaten", "Open-Data"]
image: "/assets/img/geodaten-deutschland-download.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

## Freie bzw. kostenlose Geodaten online von Bund und Ländern

Hier findest du kostenlose online [Geodaten](/gis/was-sind-geodaten "Was sind Geodaten?") für alle Bundesländer in Deutschland, Satellitenbilder, DEM und mehr.

### Geodaten Deutschland

*   [GovData](https://www.govdata.de/)
*   [GDI - Geodateninfrastruktur Deutschland](http://www.geoportal.de/DE/GDI-DE/gdi-de.html%3bjsessionid=0A588398DEAA658A2088AE96F356DD3D?lang=de)
*   [BKG - Open Data des Bundesamtes für Kartographie und Geodäsie](http://www.geodatenzentrum.de/geodaten/gdz_rahmen.gdz_div?gdz_spr=deu&gdz_akt_zeile=5&gdz_anz_zeile=0&gdz_user_id=0)
*   [Postleitzahl Gebiete](http://arnulf.us/PLZ) - [Direkter Download](http://www.metaspatial.net/download/plz.tar.gz)

### Geodaten der Bundesländer

**Baden-Württemberg**

*   [GovData - Filter BW](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Baden-W%C3%BCrttemberg/s/relevance_desc)
*   [Geoportal.de - Filter BW](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Baden-W%25C3%25BCrttemberg+%2528Bundesland%2529&north=49.913503622541&west=6.9934321260051&east=10.878852285442&south=47.403322231778&submit=Suchen)
*   [GDI BW](http://www.geoportal-bw.de/geoportal/opencms/de/index.html)
*   [Open Data Geobasisdaten BW](https://www.lgl-bw.de/lgl-internet/opencms/de/07_Produkte_und_Dienstleistungen/Open_Data_Initiative/)
*   [Open Data Portal BW](https://opendata.service-bw.de/)

**Bayern**

*   [GovData - Filter Bayern](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Bayern/s/relevance_desc)
*   [Geoportal.de - Filter Bayern](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Bayern+%2528Bundesland%2529&north=51.26082162447&west=7.9868900297423&east=15.353934339493&south=46.481214045492&submit=Suchen)
*   [Open Geodata Portal Bayern](http://www.ldbv.bayern.de/produkte/weitere/opendata.html)
*   [Open Data Portal Bayern](https://opendata.bayern.de/;jsessionid=C4B01FCA8250FC639B6E6D29201EE826?0)
*   [GDI Bayern](http://www.gdi.bayern.de/)

**Berlin**

*   [GovData - Filter Berlin](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Berlin/s/relevance_desc)
*   [Geoportal.de - Filter Berlin](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Berlin+%2528Bundesland%2529&north=52.719956743268&west=13.089193919295&east=13.778630669015&south=52.299313588399&submit=Suchen)
*   [FIS Broker](http://fbinter.stadt-berlin.de/fb/index.jsp)
*   [Geoportal Berlin](http://www.stadtentwicklung.berlin.de/geoinformation/)
*   [Open Data Berlin](http://daten.berlin.de/)

**Brandenburg**

*   [GovData - Filter Brandenburg](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Brandenburg/s/relevance_desc)
*   [Geoportal.de - Filter Brandenburg](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Brandenburg+%2528Bundesland%2529&north=53.786167823077&west=11.042722621185&east=15.37741366235&south=51.142397091587&submit=Suchen)
*   [Geoportal Brandenburg](https://geoportal.brandenburg.de/startseite/)
*   [GDI Brandenburg](http://gdi.berlin-brandenburg.de/)

**Bremen**

*   [GovData - Filter Bremen](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Bremen/s/relevance_desc)
*   [Geoportal.de - Filter Bremen](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Bremen+%2528Bundesland%2529&north=53.677037389759&west=8.106087568641&east=9.3428023893345&south=52.939922061149&submit=Suchen)
*   [GDI Bremen](http://www.gdi.bremen.de/)
*   [Open Data - Transparenzportal](http://transparenz.bremen.de/daten-1467)

**Hamburg**

*   [GovData - Filter Hamburg](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Hamburg/s/relevance_desc)
*   [Geoportal.de - Filter Hamburg](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Hamburg+%2528Bundesland%2529&north=54.360640377106&west=8.2204907033727&east=10.5434146125&south=52.996867123102&submit=Suchen)
*   [Geoportal Hamburg-Service](https://gateway.hamburg.de/HamburgGateway/FVP/Application/DienstEinstieg.aspx?fid=59)
*   [GDI Hamburg](http://www.hamburg.de/gdi-hh)
*   [Open Data - Transparenzportal](http://suche.transparenz.hamburg.de/advanced_search)

**Hessen**

*   [GovData - Filter Hessen](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Hessen/s/relevance_desc)
*   [Geoportal.de - Filter HE](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Hessen+%2528Bundesland%2529&north=51.764587007091&west=6.9392171106256&east=10.981561598402&south=49.256064412034&submit=Suchen)
*   [Geodaten onlin](https://www.gds.hessen.de/is-bin/INTERSHOP.enfinity/WFS/HLBG-Geodaten-Site/-/-/-/Default-Start)
*   [GDI Hessen](http://www.geoportal.hessen.de/)

**Mecklenburg-Vorpommern**

*   [GovData - Filter MV](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Mecklenburg-Vorpommern/s/relevance_desc)
*   [Geoportal.de - Filter MV](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Mecklenburg-Vorpommern+%2528Bundesland%2529&north=55.20063207865&west=10.446325361948&east=14.902263260259&south=52.572391879158&submit=Suchen)
*   [Geowebdienst MV](https://www.geoportal-mv.de/portal/Geowebdienste)
*   [GDI MV](https://www.geoportal-mv.de/portal/)

**Niedersachsen**

*   [GovData - Filter Niedersachsen](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Niedersachsen/s/relevance_desc)
*   [Geoportal.de - Filter NS](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Niedersachsen+%2528Bundesland%2529&north=53.839323291181&west=6.980155892742&east=11.219565348476&south=51.32976242389&submit=Suchen)
*   [Geodatensuche Niedersachsen](http://geoportal.geodaten.niedersachsen.de/harvest/srv/de/main.home)
*   [GDI Niedersachsen](http://www.geodaten.niedersachsen.de/metadaten/geodatensuche/geodatensuche-25509.html)

**Nordrhein-Westfalen**

*   [GovData - Filter NRW](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Nordrhein-Westfalen/s/relevance_desc)
*   [Geoportal.de - Filter NRW](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Nordrhein-Westfalen+%2528Bundesland%2529&north=52.701892589287&west=5.4690831273899&east=9.5938066533539&south=50.146953962194&submit=Suchen)
*   [Geodatenportal NRW](https://www.geoportal.nrw/)
*   [Open Geobasisdaten NRW](https://www.opengeodata.nrw.de/produkte/)
*   [Open NRW Data Portal](https://open.nrw/de/dat_kat)

**Rheinland-Pfalz**

*   [GovData - Filter Rheinland-Pfalz](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Rheinland-Pfalz/s/relevance_desc)
*   [Geoportal.de - Filter RLP](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Rheinland-Pfalz+%2528Bundesland%2529&north=51.20649138703&west=5.209590515021&east=9.1983164560472&south=48.64146265854&submit=Suchen)
*   [Open Geodata RLP](https://lvermgeo.rlp.de/de/geodaten/opendata/)
*   [GDI Rheinland-Pfalz](http://www.geoportal.rlp.de/portal/informationen.html)
*   [Open Data Portal RLP](https://daten.rlp.de/)

**Saarland**

*   [GovData - Filter Saarland](https://www.govdata.de/web/guest/suchen/-/searchresult/q/saarland/s/relevance_desc)
*   [Geoportal.de - Filter](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Saarland+%2528Bundesland%2529&north=49.745327785001&west=6.2979698662362&east=7.4541121596794&south=48.990383215908&submit=Suchen)
*   [GDI Saarland](http://geoportal.saarland.de/portal/de/)

**Sachsen**

*   [GovData - Filter Sachsen](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Sachsen/s/relevance_desc)
*   [Geoportal.de - Filter Sachsen](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Sachsen+%2528Bundesland%2529&north=52.224415732281&west=11.455238486196&east=15.654586514315&south=49.572208459386&submit=Suchen)
*   [Geoportal Sachsenatlas](https://geoportal.sachsen.de/)
*   [GDI Sachsen](http://www.gdi.sachsen.de/)
*   [Open Data Sachsen](http://www.opendata.sachsen.de/)

**Sachsen-Anhalt**

*   [GovData - Filter SN](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Sachsen-Anhalt/s/relevance_desc)
*   [Geoportal.de - Filter SN](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Sachsen-Anhalt+%2528Bundesland%2529&north=53.279005431497&west=9.8956029465729&east=14.120012781954&south=50.674038321154&submit=Suchen)
*   [Geodatenportal Sachsen-Anhalt](https://www.lvermgeo.sachsen-anhalt.de/de/geoservice/main.htm)
*   [GDI Sachsen-Anhalt](http://www.lvermgeo.sachsen-anhalt.de/de/main.htm)

**Schleswig-Holstein**

*   [GovData - Filter SH](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Schleswig-Holstein/s/relevance_desc)
*   [Geoportal.de - Filter SH](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Schleswig-Holstein+%2528Bundesland%2529&north=55.462423501163&west=7.3962293917913&east=11.806835105297&south=52.936437750899&submit=Suchen)
*   [DigitalerAtlas Nord](http://danord.gdi-sh.de/viewer/resources/apps/Anonym/index.html?lang=de)
*   [SH-MIS Geodatenportal](http://www.sh-mis.schleswig-holstein.de/catalog/Start.do;jsessionid=021063574A842A9D75DA2BDDDDE863CC.nodeTC01)
*   [GDI Schleswig-Holstein](http://www.gdi-sh.de/DE/GDISH/gdish_node.html)

**Thüringen**

*   [Geoportal.de - Filter Thüringen](http://www.geoportal.de/DE/Geoportal/Suche/suche.html?lang=de&baseURL=+http%253A%252F%252Fwww.geoportal.de%252FDE%252FGeoportal%252FSuche%252Fsuche.html&what=&where=Th%25C3%25BCringen+%2528Bundesland%2529&north=52.20259255621&west=9.3100571038036&east=13.402034394332&south=49.619308739586&submit=Suchen)
*   [Open Geodata Thüringe](http://www.geoportal-th.de/Downloadbereiche/DownloadKataloge/TabId/110/PID/617/CategoryID/30/CategoryName/OPENDATA/Default.aspx)
*   [GDI Thüringen](http://www.thueringen.de/th9/tmil/kv/gis/index.aspx)
*   [GovData - Filter Thüringen](https://www.govdata.de/web/guest/suchen/-/searchresult/q/Th%C3%BCringen/s/relevance_desc)

### Weitere globale Datenbestände im Internet

**DEM / SRTM**

*   [LP DAAC Global Data Explorer](https://gdex.cr.usgs.gov/gdex/)
*   [SRTM elevation data](http://srtm.csi.cgiar.org/ "SRTM elevation data")
*   [USGS EarthExplorer](http://earthexplorer.usgs.gov/ "USGS EarthExplorer")

**Openstreetmap Daten**

*   [Geofabrik](https://download.geofabrik.de/)
*   [OpenStreetMapData](http://openstreetmapdata.com/data "OpenStreetMapData")

**Sonstige**

*   [ArcGIS Hub - Open Data](https://hub.arcgis.com/pages/open-data "ArcGIS Hub - Open Data")
*   [NCEIA](https://www.ncei.noaa.gov/access) - Umweltrelevante Daten
*   [GEBCO](https://www.gebco.net/) - Daten zur Bathymetrie der Ozeane
*   [NASA Earthdata](https://search.earthdata.nasa.gov/search)
*   [Natural Earth Data](http://www.naturalearthdata.com/ "Natural Earth Data")
*   [DIVA-GIS Data](http://www.diva-gis.org/Data "DIVA-GIS Data")
*   [USGS EarthExplorer](http://earthexplorer.usgs.gov/ "USGS EarthExplorer")
*   [SimpleGeo Public Spaces](https://archive.org/details/2011-08-SimpleGeo-CC0-Public-Spaces "SimpleGeo Public Spaces")
*   [FAO GeoNetwork](http://www.fao.org/geonetwork "FAO GeoNetwork")
*   [Global Map](https://globalmaps.github.io/ "Global Map")
*   [Open Topography](http://www.opentopography.org/ "Open Topography")
*   [UNEP Environmental Data Explorer](http://geodata.grid.unep.ch/ "UNEP Environmental Data Explorer")
*   [NEO Earth Observations (NASA)](http://neo.sci.gsfc.nasa.gov/ "NEO Earth Observations")
*   [Terra Populus](https://www.terrapop.org/ "Terra Populus")
*   [Socioeconomic Data and Applications Center (SEDAC)](http://sedac.ciesin.columbia.edu/data/sets/browse "Socioeconomic Data and Applications Center")
*   [HydroSHEDS](https://hydrosheds.cr.usgs.gov/dataavail.php "HydroSHEDS")
