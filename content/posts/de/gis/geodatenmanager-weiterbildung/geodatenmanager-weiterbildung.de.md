---
layout: "post"
title: "Geodatenmanager Weiterbildung - Universität Tübingen"
date: "2018-04-15"
description: "Als Geodatenmanager/in sind Sie verantwortlich für den Aufbau und die Verwaltung einer GDI und erstellen Karten aus räumlichen Basisinformationen"
category: "GIS"
tags: ["Geodatenmanager", "Geoinformatik", "Weiterbildung"]
image: "/assets/img/geodatenmanager-weiterbildung-uni-tuebingen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Die Universität Tübingen bietet ein Zertifikatsstudium "Geodatenmanager/in" an, dass ein umfassendes Wissen über die Geoinformatik liefert und sich hauptsächlich an Berufstätige richtet, die ihr Wissen vertiefen möchten. Mit dem Abschluss des vollständigen Zertifikatstudiums erhält man das Diploma of Advanced Studies (DAS) Geodatenmanager.

## Professioneller Umgang mit Geodaten ist wichtiger denn je

Als Geodatenmanager/in sind Sie verantwortlich für den Aufbau und die Verwaltung einer Geodateninfrastruktur (GDI) und erstellen aussagekräftige Karten und Planwerke aus räumlichen Basisinformationen.

Durch Analyse, Interpretation und Darstellung veredeln Sie [Geobasisdaten](/gis/was-sind-geodaten/) zu Geofachdaten. Entstanden ist dieses neue Berufsbild aus den aktuellen Anforderungen der EU-Richtlinie INSPIRE und den klassischen Aufgabenfeldern der Geoinformatik. Auf diese Anforderungen an neue qualifizierte Experten reagiert die Universität Tübingen mit dem Zertifikatsstudium Geodatenmanager/in.

Dieses Weiterbildungsstudium richtet sich an Berufstätige, die mit räumlichen Informationen arbeiten.

## Was kann ein/e Geodatenmanager/in am Ende der Ausbildung?

Sie erhalten einen fundierte über die Grundlagen der Geoinformatik, deren Anwendungsmöglichkeiten und Einblick in die wichtigsten Programmpakete. Wir gehen folgenden Fragen nach:

*   Welche Daten brauche ich?
*   Woher bekomme ich sie?
*   Wie kann ich die Daten nach den Kriterien Auflösung, Maßstab und Projektion technisch korrekt bearbeiten?
*   Wie verwalte und organisiere ich meine Daten und Projekte?

Sie lernen zunächst die wichtigsten aktuellen [Softwarepakete](/gis-software-optionen-open-source-kostenlos-und-kostenpflichtig "GIS-Software Optionen") kennen. Darauf folgend können Sie sich sowohl in kommerzielle wie Open Source Produkte vertieft einarbeiten

## Module des Zertifikatsstudiums

![Module des Zertifikatstudiums](/static/3f28db2f272a76a890c04b3aa9b4a736/fec90/csm_2016_DAS_Geodaten_9bdbcc4602.jpg "Module des Zertifikatstudiums")
Bildquelle: Universität Tübingen

Fundierte Kenntnisse von "GIS 1" und "GIS 2" sind für die Teilnahme an den Zertifikaten "Fernerkundung" und "Geodaten" obligatorisch. Sie können durch einen Kursbesuch oder fundierte berufspraktische Erfahrungen nachgewiesen werden. Über die Anerkennung entscheidet das Geographische Institut.

## Das Baukastenprinzip

Sie interessiert nur ein Modulthema? Dann buchen Sie nur ein Weiterbildungsmodul.

Sie möchten grundlegendes Know-how in einem Spezialgebiet erwerben? Dann belegen Sie eine ganze Zertifikatslinie.

Ein Certificate of Advanced Studies (CAS) kann zu folgenden Themenfeldern erworben werden:

*   [_Geographische Informationssysteme_](/gis/was-ist-gis "Was ist GIS?")
*   _Fernerkundung_
*   [_Geodaten_](/gis/was-sind-geodaten/ "Was sind Geodaten?")

Mit dem Abschluss aller drei Zertifikate erhalten Sie das Diploma of Advanced Studies (DAS) Geodatenmanager/in.

Die fachliche Federführung über das Zertifikatsstudium liegt bei [Prof. Dr. Volker Hochschild.](http://www.uni-tuebingen.de/en/92921 "Öffnet externen Link in neuem Fenster")

Sie möchten den Flyer herunterladen? Dann klicken Sie [hier.](http://www.uni-tuebingen.de/index.php?eID=tx_securedownloads&p=46108&u=0&g=0&t=1523908348&hash=421bcb13a894c2235a2760deb2fa654cfd2556cb&file=/fileadmin/Uni_Tuebingen/Zielgruppen/Tuebinger_Zentrum_fuer_Wissenschaftliche_Weiterbildung/Dokumente/2016_08_FlyerGeodatenmanager.pdf "Leitet Herunterladen der Datei ein")

Quelle: [Universität Tübingen](http://www.uni-tuebingen.de/zielgruppen/weiterbildung/programm/geodatenmanagerin.html)