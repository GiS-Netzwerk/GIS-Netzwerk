---
layout: "post"
title: "GIS Firmen und GIS Dienstleister Verzeichnis"
date: "2018-05-07"
description: "Hier findest du ein Verzeichnis für Firmen die irgendwie was mit GIS am Hut haben."
category: "GIS"
tags: []
image: "/assets/img/gis-firmen-und-dienstleister.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

## GIS Dienstleister Verzeichnis

Hier findest du ein Verzeichnis für Firmen die irgendwie was mit [GIS](/gis/was-ist-gis "Was ist GIS?") am Hut haben. Dies umfasst GIS-Dienstleistungen, Softwareentwicklung und vieles mehr.

+ **[RIWA-GIS](http://www.riwa-gis.de/)**

    Dienstleistungen wie Internet _GIS_, Web _GIS_ und Geoinformationen für kommunale Verwaltungen, Zweckverbände, Stadtwerke und Industriebetriebe.

+ **[AGIS GmbH, Frankfurt a.M.](http://www.geoas.de)**

    Softwareentwicklung, Geodaten, Dienstleistungen, Schulungen

+ **[alta4 Geoinformatik AG](http://www.alta4.com)**

+ **[ahu AG](http://www.ahu.de)**

+ **[Asseco BERIT GmbH](http://www.asseco-berit.de)**

    Deutschland

+ **[Asseco BERIT AG](http://www.asseco-berit.ch)**

    Schweiz

+ **[BM Vektor](http://www.bm-vektor.de)**

    Geodatenservice ' Vermessung & Trassierung ' GIS/NIS Beratung

+ **[Breustedt GmbH Informationssysteme](http://www.breustedt-gmbh.de)**

    GIS, Softwareentwicklung, Fachschalen, Schulungen, Support

+ **[Contelos GmbH](http://www.contelos.de)**

    Autodesk GIS: Software, Integration, Entwicklung und Support

+ **[con terra](http://www.conterra.de)**

+ **[CPA Geo-Information](http://www.supportgis.de)**

+ **[TerraImaging B.V. Berlin](http://www.terraimaging.de)**

+ **[das byro Marchfeld](http://www.das-byro.de)**

    GIS, Kartographie u. Fernerkundung

+ **[DDS Digital Data Services GmbH, Karlsruhe](http://www.ddsgeo.de)**

    Geodaten, Geomarketing, Beratung für Geomarketing-Anwendungen mit ORACLE, Microsoft Virtual Earth und Google Earth

+ **[DIGITERRA](http://www.digiterra.de)**

+ **[disy Inforamtionssysteme GmbH](http://www.disy.net)**

    Erstellung von Spatial-Reporting-Lösungen

+ **[Dr. Neumann Consulting - Geospatial Services](http://www.geospatial-services.de)**

    Datenmigration, GIS-Dienstleistung, Kartografie

+ **[Eagle Eye Earth](http://www.eagle-eye-earth.com)**

    3D GIS Software Entwicklung: kundenspezifische Visualisierungs- und Annwendungssoftware; Simulationen

+ **[EFTAS Fernerkundung Technologietransfer GmbH](http://www.eftas.com)**

    GIS, Geodaten, GPS-gestützte Kartierungen, Luft- und Satellitenbildauswertung, Digitale Photogrammetrie, Schulung/Consulting

+ **[entera - Ingenieurgesellschaft für Planung und Informationstechnologie](http://www.entera.de)**

+ **[focus:GIS - Büro für Geoinformatik](http://www.focus-gis.de)**

+ **[Fichtner IT Consulting](http://www.fit.fichtner.de)**

+ **[GDF Hannover GbR](http://www.gdf-hannover.de)**

+ **[GDI-Service Rostock](http://www.gdi-service.de)**

    WebGIS Entwicklungen für öffentliche Verwaltungen und Forschungseinrichtungen mit [kvwmap](http://www.kvwmap.de), Fahrzeugortung und Arbeitszeitabrechnung für [PTC GPS GmbH](http://www.ptc-gps-fahrzeugortung.de)

+ **[GDV Gesellschaft für geografische Datenverarbeitung mbH](http://www.gdv.com)**

+ **[Geo-Ref.net](http://geo-ref.net/ph/service.htm)**

    geo-ref.net ist eine kleine Gruppe internationaler Berater, die normalerweise für internationale Organisationen in Projektverträgen in verschiedenen Ländern tätig sind

+ **[GeoSeven ' Pache, Giljohann & Partner](http://www.geo7.com)**

+ **[GeoData+ GmbH](http://www.geodataplus.de)**

    Geoinformationsttechnologien mit frox und ArcGIS, moderne WebGIS-Lösungen, innovative Mobile- und Desktop-Anwendungen

+ **[GEOLOCK GmbH - WebGIS-Lösungen](http://www.geolock.de)**

+ **[GEO-Consortium](http://www.geo-consortium.de)**

+ **[Geoinformatikbüro Dassau GmbH](http://www.gbd-consult.de)**

    Beratung, Support, Schulung, Programmierung, GIS Analysen mit QGIS und GRASS.

+ **[geomer GmbH](http://www.geomer.de)**

    Risikomanagement, Geomarketing, Daten

+ **[geOps GeoInformatics](http://www.geops.de)**

+ **[geoSYS](http://www.geosysnet.de)**

+ **[Geoventis GmbH](http://www.geoventis.de)**

+ **[GI Geoinformatik GmbH](http://www.gi-geoinformatik.de/)**

+ **[GIS Consult GmbH](http://www.gis-consult.de)**

+ **[GIStec GmbH](http://www.gistec-online.de)**

+ **[gis-trainer](http://www.gis-trainer.de)**

+ **[hydrotec](http://www.hydrotec.de)**

+ **[IBB Ingenieurbüro Battefeld](http://www.battefeld.com)**

    GIS Lösungen mit BricsCAD und AutoCAD für Gas, Wasser, Strom, Kanal, Kataster, Geodatenkonvertierung

+ **[imp GmbH - Gesellschaft für Geodatenservice](http://www.imp-gmbh.de)**

    GIS-Service, Geodatenmanagement, Migration, Schulung, Fachschalenanpassung, 3D-Modellierung und Visualisierungen

+ **[Ingenieurbüro I. Lehmann GbR](http://ibil.de)**

    GIS-Service, Geodatenmanagement

+ **[infas GEOdaten GmbH](http://www.infas-geodaten.de)**

+ **[Infraplan Syscon](http://www.infraplan.de)**

+ **[INTEND Geoinformatik GmbH](http://www.intend.de)**

+ **[Intevation GmbH](http://www.intevation.de/geospatial)**

+ **[Intergraph (Deutschland) GmbH](http://www.intergraph.de/sgi)**

+ **[iNovaGIS oHG Kassel](http://www.inovagis.com)**

+ **[ISB AG](http://www.isb-ag.de)**

Softwareentwicklung, GIS-Consulting

+ **[IT-Consult Halle GmbH](http://www.itc-halle.de/content.asp?f=cont&page=22)**

+ **[ITS Informationstechnik Service GmbH](http://www.its-service.de)**

+ **[Kappasys FOSSGIS Enterprise Solutions Solothurn/Schweiz](http://www.kappasys.ch)**

    GIS-Consulting, Geodaten-Infrastrukturen, Quantum-GIS und PostgreSQL Schulungen, Quantum-GIS Fachschalen-Entwicklung

+ **[Lambda Media](http://www.lambdamedia.de)**

    Webmapping, Mapserver, Visualisierung von Geodaten im Internet

+ **[MapChart GmbH](http://www.mapchart.com)**

    Saas, GIS-on-Demand, Location intelligence

+ **[MapMedia](http://www.mapmedia.de)**

+ **[megatel GmbH](http://www.megatel.de/)**

+ **[Mettenmeier GmbH](http://www.mettenmeier.de/)**

+ **[mobileGIS.at](http://www.mobileGIS.at/)**

    Soft- und Hardwarelösungen zur mobilen Geodatenerfassung und Kontrolle

+ **[Moskito-GIS GmbH](http://www.moskito-gis.de) [Moskito-GIS](http://giswiki.org/wiki/Moskito-GIS "Moskito-GIS")**

+ **[mundialis GmbH & Co. KG](http://www.mundialis.de)**

Open Source GIS Entwicklung, big geodata analysis, Fernerkundung mit Sentinel, Landsat, etc.

+ **[MV Kommunalberatung GmbH](http://www.mv-kommunalberatung.de)**

    GIS-Dienstleistungen

+ **[nature-consult](http://www.nature-consult.de)**

    Freie GIS-Lösungen mit GRASS und Quantum GIS

+ **[norBIT GmbH](http://www.norbit.de/)**

    Desktop-GIS, webGIS, Softwareentwicklung, Dienstleistungen, Freie Software

+ **(http://www.oagis.com)**

+ **[Omniscale](http://omniscale.de)**

+ **[Paulick Consult](http://www.urbeli.com)**

    GeoInformatik, Messtechnik u. Tauchservice

+ **[PLANUNGSGRUPPE ÖKOLOGIE + UMWELT GmbH](http://www.planungsgruppe-hannover.de)**

    Planung + GIS-Dienstleistung

+ **[plan-GIS GmbH](http://www.plan-gis.de)**

    Gesellschaft für digitale Planung

+ **[Punchbyte Software](http://www.punchbyte.de)**

+ **[CSC Ploenzke AG](http://de.country.csc.com/de/)**

+ **[POPPENHÄGER GRIPS GMBH](http://www.grips.de)**

    Ihr GIS Kompetenzzentrum, Neunkirchen/Saar

+ **[PRO DV Software AG](http://www.prodv.de)**

+ **[raumbezug GbR Geodaten - Geoinformationen](http://www.raumbezug.eu)**

+ **[regis-online.de](http://www.regis-online.de)**

+ **[Spatial Services GmbH](http://www.spatial-services.com/)**

    Geoinformatik Lösungen und Dienste - GIS Consulting

+ **[SYNERGIS Unternehmensgruppe](http://www.mysynergis.com)**

+ **[terrestris](http://www.terrestris.de)**

+ **[TopoL Deutschland - Arbeitsgemeinschaft](http://www.topol.de)**

+ **[TopoSys GmbH - Airborne Laser Scanning and Sensor Systems](http://www.toposys.com)**

+ **[torresin & partner - Geoinformation - Geomedienkompetenz - räumliche Nachhaltigkeit](http://www.torr.de)**

+ **[uismedia Lang & Müller](http://www.uismedia.de)**

+ **[IDU mbH](http://www.idu.de)**

    mit den Produkten [IWAN](http://www.webmapserver.de), [cardo](http://www.cardogis.com) und [lucida.map](http://webs.idu.de/idu/lucida)

+ **[VIWOTEC GmbH](http://www.viwotec.de)**

+ **[virtualcitySYSTEMS GmbH](http://www.virtualcitysystems.de)**

    Landxplorer Schulungen & Erstellung von 3D-Stadtmodellen

+ **[WhereGroup GmbH & Co. KG](http://www.wheregroup.com/)**

+ **[WIGeoGIS Gesellschaft für digitale Wirtschaftsgeographie mbH](http://www.wigeogis.de)**

+ **[aphos Leipzig AG](http://www.aphos.de/)**

    Digitale photogrammetrische Datenerfassung für GIS- und CAD-Systeme, Lage- und Höhenpläne, georeferenzierte Orthophotos

+ **[GIS-Mapping S. Seyfarth](http://www.gis-mapping.de/)**

    Geodatenbanken, Vektorisierung und Referenzierung von Karten, Satellitenbildern, Luftbildern sowie Lageplänen

+ **[HÖPFINGER GmbH & Co.KG - Dienstleistungen für Infrastruktur](http://www.hoepfinger.de/)**

    Dienstleistungen für Infrastruktur

+ **[AED-SYNERGIS GmbH](http://www.aed-synergis.de/)**

    Geoinformationssysteme, Vermessung & Kataster, Forst & Umwelt, Infrastruktur & CAFM, Tools & Schnitstellen

Auf GIS-Wiki findest du noch mehr: [GIS-Wiki](http://giswiki.org/wiki/GIS_Firmen/B%C3%BCros)