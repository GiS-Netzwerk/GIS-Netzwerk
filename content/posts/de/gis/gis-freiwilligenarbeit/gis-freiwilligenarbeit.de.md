---
layout: "post"
title: "GIS Freiwilligenarbeit - Mit GIS Von Zuhause die Welt verbessern"
date: "2019-04-05"
description: "GIS Freiwilligenarbeit bietet eine gute Möglichkeit sich persönlich und beruflich weiterzuentwickeln."
category: "GIS"
tags: ["Jobs", "Remote work"]
image: "/assets/img/gis-freiwilligenarbeit.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

[GIS](/gis/was-ist-gis "Was ist GIS?") Freiwilligenarbeit bietet eine gute Möglichkeit sich persönlich und beruflich weiterzuentwickeln.

Außerdem kannst du dich für einen guten Zweck engagieren. Die Projekte kannst du später auch in ein hübsches Portfolio packen und so mit extra Punkten bei einer Bewerbung dich gegenüber Mitbewerbern hervorheben.

Was noch besser ist, ist dass du bei diesen Organisationen mit einem PC Zuhause mitmachen kannst und nicht durch die Welt reisen musst.

## [](#openstreetmap)OpenStreetMap

![OpenStreetMap](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Openstreetmap_logo.svg/256px-Openstreetmap_logo.svg.png "OpenStreetMap")

[https://www.openstreetmap.de/](https://www.openstreetmap.de/)

OpenStreetMap ist ein internationales Projekt, dass 2004 gegründet wurde.

Ziel von OSM ist es eine freie Weltkarte zu erschaffen und diese allen kostenfrei zur Verfügung zu stellen. Die Daten werden von Freiwillligen (auch **Mapper** genannt) gesammelt. Es werden Daten über Straßen, Eisenbahnen, Flüsse, Wälder, Häuser, usw. erhoben.

### [](#wie-du-mitmachen-kannst)Wie du mitmachen kannst

Es gibt viele verschiedene Möglichkeiten zu OpenStreetMap beizutragen, vom Melden kleiner Fehler in der Karte, über das Vervollständigen bestehender Daten bis zum Abzeichnen neuer Gebäuden aus Luftbildern und dem Erfassen von Wegen und interessanten Punkten mit dem GPS-Gerät. Unsere Anleitungen helfen dir beim Benutzen der passenden Programme und dem Eintragen von Daten. (OpenStreetMap)

[Mitmachen...](https://www.openstreetmap.de/faq.html#wie_mitmachen)

## [](#humanitarian-openstreetmap-team)Humanitarian OpenStreetMap Team

![Humanitarian OpenStreetMap Team](https://www.hotosm.org/images/hot-logo-icon-nav.svg "Humanitarian OpenStreetMap Team")

[https://www.hotosm.org/](https://www.hotosm.org/)

Das Humanitarian OpenStreetMap Team (HOT) ist ein internationales Team, das sich dem Mapping bzw. der Kartierung von humanitären Aktionen und der Entwicklung von Gemeinschaften widmet. Mit den Daten werden Risiken in den verringert und es wird an einer nachhaltigen Entwicklung gearbeitet.

### [](#gis-freiwilligenarbeit-mit-hot)GIS Freiwilligenarbeit mit HOT

Als **Mapping Volunteer** kannst du wie bei OpenStreetMap Daten für Karten erheben. **Humanitarian and GIS Professionals** helfen außerdem noch in zusätzlichen Bereichen, wie Datenverarbeitung, Validierung von Karten oder erstellen komplett neue Karten und Visualisierungen.

[Mitmachen...](https://www.hotosm.org/volunteer#humanitarian-and-gis-professionals)

## [](#standby-task-force)Standby Task Force

![Standby Task Force](https://www.standbytaskforce.org/wp-content/uploads/2016/02/cropped-Logo_SBTF_RED-03-450x203.png "Standby Task Force")

[https://www.standbytaskforce.org/](https://www.standbytaskforce.org/)

Standby Task Force ist ein globales Netzwerk aus geschulten und erfahrenen Freiwilligen, die online zusammenarbeiten.

Die Standby Task Force ist eine gemeinnützige Organisation die 2010 gegründet wurde.

Standby Task Force war seitdem bei vielen Naturkatastrophen aktiv und die Freiwilligen haben viele humanitäre Organisationen bei der Wahlbeobachtung und bei anderen Projekten unterstützt.

### [](#freiwillig-bei-standby-task-force)Freiwillig bei Standby Task Force

Du solltest hier bereits Berufserfahrung in den Bereichen GIS-Management, Katastrophenmanagement und anderen technischen Bereichen mitbringen

[Mitmachen...](https://www.standbytaskforce.org/help-us/volunteer-with-us/)

## [](#giscorps)GISCorps

![GISCorp](https://www.urisa.org/clientuploads/directory/graphics/gc_logo.jpg "GISCorp")

[https://www.giscorps.org/](https://www.giscorps.org/)

GISCorps koordiniert kurzfristige, freiwillige GIS-Dienste für benachteiligte Gemeinschaften.

Die Projekte variieren je nach Bedarf der Partneragentur und können alle Aspekte von GIS einschließen, einschließlich Analyse, Kartografie, App-Entwicklung, Bedarfsanalysen, technische Workshops usw.

Zu den Leistungsbereichen gehören humanitäre Hilfe, Katastrophenschutz, Umweltschutz, Gesundheit und Gesundheit Personaldienstleistungen, GIS-Ausbildung und Crowdsourcing von Experten. GISCorps wird von Einzelspenden, Unternehmen und anderen gemeinnützigen Gruppen mit ähnlichen Zielen unterstützt.

### [](#engagieren)Engagieren

Bei GISCorps gibt es mehre Möglichkeiten sich zu beteiligen. Du kannst dich als Freiwilliger bewerben und so aktiv GIS-Projekte unterstützen. Eine einjährige persönliche ArcGIS Lizenz bekommst du hier kostenlos dazu, sofern du angenommen wirst.

Außerdem kannst du das Projekt natürlich mit Spenden unterstützen.

[Mitmachen...](https://www.giscorps.org/become-a-volunteer/)