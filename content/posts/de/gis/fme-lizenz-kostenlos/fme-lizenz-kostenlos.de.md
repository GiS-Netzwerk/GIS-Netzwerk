---
layout: "post"
title: "FME-Lizenz kostenlos für den privaten Gebrauch"
date: "2019-04-22"
description: "FME (Feature Manipulation Engine) ist ein mächtiges und das meistgenutzte Spatial ETL-Tool zur Migration und Bearbeitung von Geodaten."
category: "GIS"
tags: ["FME", "Geodaten"]
image: "/assets/img/fme-lizenz-kostenlos.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

## FME Home Lizenz

FME (Feature Manipulation Engine) ist ein mächtiges und das meistgenutzte Spatial ETL-Tool zur Migration und Bearbeitung von [räumlichen Daten](/gis/was-sind-geodaten "Was sind Geodaten?") und nicht-räumlichen Daten. Dabei ist die Software sehr flexibel und leistungsfähig. Es kann auch ohne Probleme mit sehr großen Datenmengen umgehen.

Feature Manipulation Engine unterstützt über 300 verschiedene Datenquellen wie [GIS](/gis/was-ist-gis "Was ist GIS?")-Datenbanken] (PostGIS, MySQL, Oracle, natürlich auch die meisten nicht-räumlichen Datenbanken), CAD-Dateien(DWG, DXF), Rasterdaten, Web Services, Koordinatenlisten, XML, KML, GML, GeoJSON und noch viel mehr.

Die Bedienung der Software ist sehr einfach und wird über eine grafische Oberfläche ermöglicht, in der man das Quell- und Zieldatenmodell bzw. Format angibt. Auch komplexe Verarbeitungsprozesse werden dadurch sehr übersichtlich.

Dazwischen können unzählige sogenannte [Transformer](https://cdn.safe.com/resources/fme/FME-Transformer-Reference-Guide.pdf) eingebaut werden, mit denen man die Daten vor dem Inport in eine neue Datenquelle bearbeiten kann. Dieser Workflow kann auch mit Python oder SQL Skripts ergänzt werden.

Safe Software bietet FME in drei Varianten an.

*   FME Desktop
*   FME Server
*   FME Cloud

Das klingt super, aber du hast noch keine Ahnung davon?

Unter [Free Licenses for Home Use](https://www.safe.com/free-fme-licenses/home-use/) kannst du eine kostenlose Lizenz für FME Desktop beantragen. Sofern du Student bist kannst du [hier](https://www.safe.com/free-fme-licenses/students/) eine separate Lizenz beantragen.

**Diese Lizenz ist natürlich nur für persönliche und keine kommerziellen Projekte.**

Den Antrag zu stellen ist sehr einfach. Du musst nur deinen Namen, deine E-Mail-Adresse, ggfs. deine Firma angeben, sowie wie du die Lizenz nutzen wirst. Hier reicht es aus einfach nur zu schreiben, dass du das Programm kennenlernen und es natürlich lernen willst.

Sobald der Antrag angenommen wurde erhältst du eine E-Mail mit dem Lizenzschlüssel.

Auf der Seite [Downloads](https://www.safe.com/support/support-resources/fme-downloads/) kannnst du dir dann die Desktopversion herunterladen und nach der Installation deinen zugesendeten Lizenzschlüssel eingeben. Die Lizenz ist ein Jahr (für Studenten vier Monate) gültig und kann beliebig erweitert werden.

Es gibt eine [Knowledge Base](https://knowledge.safe.com/page/knowledge-base) auf der du tausende Tutorials findest.

Für komplexere Problemstellungen bietet es sich auch an, einen Blick auf [gis-stackexchange.com](https://knowledge.safe.com/page/knowledge-base) zu werfen.
