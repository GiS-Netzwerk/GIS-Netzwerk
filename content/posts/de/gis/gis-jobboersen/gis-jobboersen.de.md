---
layout: "post"
title: "GIS Jobbörsen - Auf der Suche nach einem neuen GIS Job?"
date: "2019-02-25"
description: ""
category: "GIS"
tags: ["Jobs"]
image: "/assets/img/gis-jobboersen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

## GIS Jobbörsen

### D-A-CH

Du bist bereits beruflich im Bereich [GIS](/gis/was-ist-gis "Was ist GIS?"), Geographie, Vermessung oder Geoinformatik tätig oder würdest dort gerne arbeiten? Dann findest du hier eine Liste mit ausgewählten GIS Jobbörsen, die interessante GIS Jobs anbieten.

*   [**Z_GIS Jobbörse**](https://groups.google.com/forum/#!forum/geospatial-job-offer)

Eine kleine Jobbörse der Universität Salzburg via Google Groups mit sehr interessanten Stellenangeboten

*   [**Geojobs**](https://www.geojobs.de/)

Jobbörse der Website GEObranchen.de mit guten GIS-Verzeichnissen

*   [**Runder Tisch GIS**](https://rundertischgis.de/jobboerse.html)

Jobbörse des größten ehrenamtlich organisierten Netzwerks zum Thema Geoinformation in Deutschland und

*   [**MyGeo**](http://www.mygeo.info/geojobs.html)

Jobs und Stellen in den Geowissenschaften von MyGeo

### International

*   [GISJobs](https://www.gisjobs.com/)
*   [MyGISJobs](https://www.mygisjobs.com/)

Du bist an Weiterbildungen zum Geoinformatiker oder Geodatenmanager interessiert? Dann wirst du hier fündig:

1.  [UNIGIS Weiterbildung](/gis/unigis-professional-weiterbildung)
2.  [Geodatenmanager Weiterbildung](/gis/geodatenmanager-weiterbildung)