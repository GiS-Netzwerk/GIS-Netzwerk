---
layout: "post"
title: "GIS Anwendungen - Welche GIS-Anwendungen gibt es?"
date: "2019-02-28"
description: "Um mit digitalen Karten bzw. Informationen (Geodaten) arbeiten zu können, wird ein Geoinformationssystem ( GIS ) eingesetzt. Mit diesem GIS können Geodaten erfasst, bearbeitet, analysiert und ansprechend dargestellt werden."
category: "GIS"
tags: []
image: "/assets/img/gis-anwendungen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Um mit digitalen Karten bzw. Informationen ([Geodaten](/gis/was-sind-geodaten)) arbeiten zu können, wird ein [Geoinformationssystem ( GIS )](/gis/was-ist-gis "Was ist GIS?") eingesetzt. Mit diesem GIS können Geodaten erfasst, bearbeitet, analysiert und ansprechend dargestellt werden. Mittlerweile gibt es viele gute Anbieter von Geoinformationssystemen, wobei die zwei bekanntesten wohl QGIS (Open Source) und ArcGIS von Esri sein dürften.

Nun hat man sich für ein GIS entschieden, allerdings stellt sich noch die Frage welche zusätzlichen GIS Anwendungen (auch Fachschalen genannt) man benötigt bzw. welche es überhaupt gibt. Darauf möchte ich in diesem Beitrag weiter eingehen.

Grundsätzlich gibt es GIS Anwendungen für folgende Branchen:

*   Banken
*   Bildung
*   Infrastrukturentwicklung
*   Katastrophenmanagement
*   Landwirtschaft
*   Logistik
*   Marketing
*   Medizin
*   Telekommunikation
*   Tourismus
*   Verbrechenskartierung
*   Verkehr
*   Versicherung
*   Wirtschafte Entwicklung

Für jede dieser Branche bieten GIS-Dienstleister unterschiedliche GIS Anwendungen an und passen diese auch individuell an die Bedürfnisse der Kunden an.

Folgend möchte ich auf ein paar Anwendungen insbesondere für Kommunen (Kommunal GIS) genauer eingehen.

## Baumkataster

Ein Baumkataster unterstützt Kommunen und Baumpflegebetrieben bei der Erfassung, Kontrolle und Verwaltung eines Baumbestands. Bäume können in Baumgruppen eingeteilt und außerdem können verschiedene Sachendaten oder Medien zu den Bäumen hinzugefügt werden:

*   Nummer, Art, Höhe, Kronnendurchmesser, Versiegelungsgrad, Bodenart, Schäden, durchgeführte Pflegemaßnahmen oder Kontrollen und Bilder, um nur ein paar aufzuzählen.

Der Trend geht hier in Richtung mobiler Lösungen. Das heißt Apps, bei denen man unterwegs auf Kontrollen oder Pflegemaßnahmen direkt Daten über ein Tablet in das GIS einpflegen kann. Diese Daten werden online gespeichert und können dann später im Büro nocheinmal korrigiert oder überarbeitet werden.

## Bebauungspläne / Flächennutzungspläne

Bebauungs- und Flächennutzungspläne können in einem GIS einfach verwaltet und ausgewertet werden.

Es können Geltungsbereiche, je nach Rechtskraft, unterschiedlich farblich dargestellt werden, Änderungen mit dem Hauptplan verknüpft, und Textdateien, wie Ergänzungen zur Satzung angefügt werden.

Außerdem können analoge Bebauungspläne aufbereitet (gescannt, georeferenziert) werden und im GIS lagegenau dargestellt werden. Dies geht zum Beispiel mit PDF oder CAD Dateien.

Als Ergebnis erhält man pro Bebaaungsplan einen Datensatz, an dem alle zugehörigen Dateien und Änderungen verknüpft sind und kann diesen ansprechend oder übersichtlich darstellen.

## Liegenschaftskataster (ALKIS)

Das Amtliche Liegenschaftskatasterinformationssystem (ALKIS) ersetzt die Automatisierte Liegenschaftskarte (ALK) und das Automatisierte Liegenschaftsbuch (ALB) in Deutschland.

Durch die Zusammenführung wird der Verwaltungsablauf in öffentlichen Behörden immens vereinfacht. Im ALKIS sind alle bekannten Eigentümer von Grundstücken (bzw. Flurstücken) detailliert erfasst.

So können zum Beispiel bei einer Baumaßnahme in einem bestimmten Bereich alle betroffenen Bürger ermittelt und sehr einfach angeschrieben werden, indem man mithilfe von Vorlagen für Schreiben und einer Selektierung von allen Bürgern in diesem Bereich (in dieser Fläche im GIS) automatisch Berichte erstellt.

## Versiegelungskataster (gesplittete Abwassergebühren)

In einem Versiegelungskataster werden alle versiegelten Flächen eines Grundstücks ermittelt. Dies geschieht über eine vorher stattgefunde Befliegung, bei der hochauflösende Bilder aufgenomme werden, oder über eine Digitalisierung von Satellitenbildern.

Alle Flurstücke werden zu einem Grundstück zusammengefasst und die versiegelten Flächen dieser Flurstücke mit dem Grundstück verknüpft. So können Kommunen die gesplittete Niederschlagswassergebühr ermitteln. Durch die Verknüpfung mit einer ALKIS Anwendung können automatisch Gebührenbescheide erstellt und an die jeweiligen Bürger versendet werden.

## Versorgungsnetze

### Wasser


Wasserversorgungsnetze lassen sich in einem GIS digital verwalten. Es können Hydrantenpläne automatisch erstellt werden, die zum Beispiel für die örtliche Feuerwehr im Ernstfall sehr hilfreich sein können.

Durchgeführte Reparaturen können an Leitungen digital gespeichert werden, sodass man immer einen Überblick behält, welche Leitungen bereits saniert wurde und welche in nächster Zeit saniert werden sollten.

### Abwasser

Viele Kommungen sind gesetzlich verpflichtet ein Kanalkataster zu führen.

Erstellt wird ein Kanalkataster entweder aus analogen Daten wie Plänen, die digitalisiert werden, oder über eine vorher stattgefunde Vermessung.

In einem Kanalkataster können Daten wie Haltungslänge, Tiefe, Leitungsdurchmesser Material, etc. gespeichert, verwaltet und analysiert werden.

Auch große Konzerne führen mittlerweile Abwasserkataster für Ihre Firmengelände.

### Strom /Gas / Breitband / Straßenbeleuchtung

Ermöglicht die Erfassung, Verwaltung und Analyse aller Versorgungsnetze.

## Baum und Grünflächen

Ermöglicht den Aufbau eines Baum- und Grünflächenkataster zur weiteren Planung und Pflege des Bestands.

## Fazit

Vorallem in Kommunen steht die Wichtigkeit eines Geoinformationssystems außer Frage. Es wird aufgrund geringeren Verwaltungsaufwands extrem viel Zeit eingespart.

Die verschiedenen Fachschalen lassen sich problemlos miteinander kombinieren und dadurch werden Arbeitsabläufe wesentlich effizienter.

Aber auch in der privaten Wirtschaft, z.B. im Immobilienmarkt, in der Landwirtschaft oder auch in der Archäologie werden immer häufiger die Vorteile eines Geoinformationssystems ( GIS ) erkannt und eingesetzt. Besonders mobile Apps, die mit einem GIS kombiniert werden, erfreuen sich großer Beliebtheit.

Falls du noch mehr Anwendungen sehen willst, schau doch bei GIS Geography vorbei. Dort findest du [1000 GIS Applications & Uses - How GIS Is Changing the World](https://gisgeography.com/gis-applications-uses/).