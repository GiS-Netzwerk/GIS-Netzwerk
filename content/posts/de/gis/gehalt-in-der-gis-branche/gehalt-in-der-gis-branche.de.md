---
layout: "post"
title: "Gehalt und Jobs in der GIS Branche"
date: "2019-03-15"
description: "Du fragst dich welche GIS Jobs es überhaupt gibt, was man macht und wieviel man verdient? Dann bist du hier richtig."
category: "GIS"
tags: ["Gehalt"]
image: "/assets/img/gehalt-und-jobs-in-der-gis-branche.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---


## [](#gehalt-und-t%C3%A4tigkeiten-bei-der-gis-arbeit)Gehalt und Tätigkeiten bei der GIS Arbeit

Du fragst dich welche [GIS](/gis/was-ist-gis "Was ist GIS?") Jobs es überhaupt gibt, was man macht und wieviel man verdient? Dann bist du hier richtig.

## [](#gis-analyst)Geoinformatiker

Ein **Geoinformatiker** oder [**Geodatenmanager**](/gis/geodatenmanager-weiterbildung-universitat-tubingen) ist ein Spezialist im Umgang mit Geodaten und hilft Projektleitern indem er technische Unterstützung bei der Umsetzung von GIS Projekten leistet.

Dabei verwendet er natürlich [GIS-Systeme](/gis/gis-software-optionen "GIS-Software Optionen") und andere ergänzende Tools bzw. Werkzeuge um bei Projekten innovative Lösungen zu erzielen.

**Aufgaben**

*   Mitwirken an Projekten mit einem Fokus auf Geodatenverarbeitung und Datenqualitätskontrolle
*   Definiert Aufgaben innerhalb bestehender Projekte um diese effektiv und termingerecht abzuschließen
*   Entwicklung und Wartung von GIS-Datenbanken
*   Erstellung von thematischen Karten um Probleme aus den Daten zu analysieren
*   Entwicklung von Strategien zur Optimierung der Geodateninfrastruktur
*   Erstellung von Datensätzen in allen möglichen Formaten, die bei der Problemlösung helfen, wie z.B PDF's, Online Karten, Shapefiles, KML und XML Dateien usw.
*   Übernahme der technischen Teilprojektleitung und Umsetzungssteuerung in komplexen Projekten

**Gehalt**

3.597 € - 5.700 €<sup>1</sup>

**Voraussetzung**

Abgeschlossenes Hochschulstudium mit Schwerpunkt Informatik, Geoinformatik oder eine vergleichbare Qualifikation

## [](#gis-sachbearbeiter--gis-fachkraft)GIS-Sachbearbeiter / GIS-Fachkraft

Als GIS Fachkraft absolvierst du technische Aufgaben, wie die Fortführung von Bestandsdaten mithilfe von Vermessungsdaten und dokumentierst diese.

Du erfasst analoge Bestandspläne für die Darstellung in Geoinformationssystem und bist für die Qualitätssicherung verantwortlich. Außerdem beteiligst du dich an der Weiterentwicklung von internen Prozessen wie Workflows usw.

**Das musst du können:**

1.  Projektionen beherrschen
2.  Vektor/Raster/DEM Verarbeitung
3.  Geodatenverarbeitungs Tools anwenden (z.B. FME)
4.  Du solltest dich ein wenig mit Python, SQL oder einer anderen Programmiersprache auskennen

**Gehalt**

2.441 € - 4.032 €<sup>1</sup>

**Voraussetzung**

Berufsausbildung in den Bereichen Geomatik, Vermessung oder Technisches Zeichnen

## [](#geomatiker)Geomatiker

Geomatiker ist noch ein relativ junger Beruf. Seit 2010 gibt es die dreijährige Ausbildung zum Geomatiker. Als Geomatiker erfasst du Geodaten, du digitalisiert analoge Daten und erstellt aus diesen Geodaten Pläne, Karten oder andere Datenmodelle.

Du bist fit in Photoshop, InDesign und anderen Anwendungsprogramme mit denen du die Pläne oder digitale Karten bearbeitest oder erstellst.

Ebenso betreust du Kunden, kennst dich in Datenmanagement und allem was zum Betrieb eines Geoinformationssystems dazugehört, aus.

**Gehalt**

2.220 € - 3663 €<sup>1</sup>

## [](#gis-administrator)GIS-Administrator

Als GIS Administrator arbeitest du an der Bereitstellung, Entwicklung und Wartung der GIS Software. Du erkennst erforderliche Änderungen und bist in der Lage diese mit einem Konzept umzusetzen.

GIS-Administratoren sind außerdem für eine funktionierende Oberfläche des GIS Systems zuständig.

Auch die Erstellung von Dokumentationen zum GIS gehören zu den Aufgaben eines GIS-Administrators. Dazu vermittelst du Anwendern des Programms erforderliche Kenntnisse um effektiv arbeiten zu können. Außerdem bist du für Datenmanagement zuständig.

**Gehalt**

3.119 € - 4.687 €<sup>1</sup>

**Voraussetzung**

Berufsausbildung, Weiterbildung, Studium und mindestens 3 Jahre relevante Berufserfahrung.

## [](#gis-entwickler-bzw-gis-developer)GIS-Entwickler bzw. GIS-Developer

GIS-Developer sind für die Weiterentwicklung des Geoinformationsystems verantwortlich. Sie haben viel Kontakt mit Kunden und erstellen Konzepte für Updates, die sich aus Anforderungen von Kunden oder Kollegen ergeben und setzen diese termingerecht um.

Als GIS Developer bist du Experte in Javascript, SQL, .NET und C#.

**Gehalt**

3.418 € - 4.862 €<sup>1</sup>

**Voraussetzung**

Abgeschlossenes Hochschulstudium mit Schwerpunkt Informatik, Geoinformatik oder eine vergleichbare Qualifikation.

Falls du nun gleich einen GIS Job willst hab ich hier ein paar GIS Jobbörsen für dich rausgesucht: [GIS Jobbörsen - Auf der Suche nach einem neuen GIS Job?](/gis/gis-jobborsen)

_(GIS Salary Pyramid; [https://gisgeography.com/gis-salary-expectations-gis-career/](https://gisgeography.com/gis-salary-expectations-gis-career/))_

## [](#welche-faktoren-wirken-sich-auf-das-gehalt-aus)Welche Faktoren wirken sich auf das Gehalt aus?

Grundsätzlich ist das Einstiegsgehalt natürlich von einer Vielzahl von Faktoren abhängig und lässt sich nicht pauschal bemessen.

*   **Position**

Welche fachlichen (Hard Skills) und Soft Skills sind Vorraussetzung für die Position? Wird ein Hochschulstudium benötigt oder reicht eine Berufsausbildung. Welche Aufgaben umfasst die Position? Personal- und Budgetverantwortung sind ein sehr wichtiger Faktor. Je größer das Team und/oder das Budget, desto höher das Gehalt.

*   **Ausbildung**

Welche Ausbildung wird für die Position benötigt? Eine abgeschlossense Berufsausbildung oder ein Hochschulstudium? Fachkräfte mit akademischen Hintergrund erhalten bis zu 70% mehr Gehalt als jemand ohne Studienabschluss (siehe Gehaltsanalyse Gehalt.de). Sind spezielle Weiterbildungen für die Stelle notwendig?

*   **Betrieb**

Das Gehalt varriert auch sehr stark von Betrieb zu Betrieb.

Hauptfaktoren dabei sind die Betriebsgröße und die Branche des Betriebs. Wieviele Mitarbeiter sind in dem Unternehmen beschäftigt? Mehr Mitarbeiter bedeutet immer mehr Gehalt. Der Unterschied kann enorm varrieren, obwohl jemand die gleichen Arbeiten erledigt. Auch wird in bestimmten Branchen generell mehr gezahlt als in anderen.

*   **Region**

Auch die Region spielt eine erhebliche Rolle bei den Einstiegsgehältern. In Ballungsräumen werden generell höhere Gehälter gezahlt. Mit 40.000€ pro Jahr kommt man in z.B. München gerade so über die Runden, während man im Gegensatz dazu in den neuen Bundesländern mit 40.000€ ein "gehobenes" Leben führen kann.

[Hier erfährst du, was noch alles das Einstiegsgehalt beeinflusst und ob dein Gehalt hoch genug ist.](https://www.gehalt.de/arbeit/die-entscheidenden-einflussgroessen-auf-das-gehalt)

Quellen:

*   **<sup>1</sup> **Gehaltsangaben von [https://www.gehalt.de](https://www.gehalt.de)