---
layout: "post"
title: "8 Open-Source Web-GIS Anwendungen"
date: "2019-04-24"
description: "Du möchtest wissen mit welchen Open-Source Web-GIS-Anwendungen Geodaten über das Internet geteilt werden? Dann erfährst du hier mehr"
category: "GIS"
tags: ["Web-GIS",]
image: "/assets/img/open-source-web-gis-anwendungen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Du möchtest wissen mit welchen Open-Source Web-[GIS](/gis/was-ist-gis "Was ist GIS?") Anwendungen Geodaten über das Internet geteilt werden?

## [](#web-gis-anwendungen)**[Web GIS Anwendungen](/wms-web-map-service-und-wmts)**

### [](#1-geoserver)1\. GeoServer

GeoServer ist ein Open-Source-Server zum Teilen von Geodaten.

[http://geoserver.org/](http://geoserver.org/)

### [](#2-degree)2\. degree

deegree ist eine Open-Source-Software für Geodateninfrastrukturen und das Geospatial-Web.

[https://www.deegree.org/](https://www.deegree.org/)

### [](#3-featureserver)3\. FeatureServer

FeatureServer ist eine Implementierung eines RESTful Geographic Feature Service.

[http://featureserver.org/](http://featureserver.org/)

### [](#4-mapguide-open-source)4\. MapGuide Open Source

MapGuide Open Source ist eine webbasierte Plattform, mit der Benutzer Web-Mapping-Anwendungen und Geodatendienste entwickeln und bereitstellen können.

[http://mapguide.osgeo.org/](http://mapguide.osgeo.org/)

### [](#5-mapserver)5\. MapServer

MapServer ist eine [Open-Source-Plattform](/tags/open-source) zur Veröffentlichung von [Geodaten](/geodaten-was-sind-geodaten) und interaktiven Kartenanwendungen im Web.

[https://www.mapserver.org/](https://www.mapserver.org/)

* * *

## [](#javascript-bibliotheken)**Javascript Bibliotheken**

### [](#6-openlayers)6\. OpenLayers

[OpenLayers](/gis/openlayers-web-map "OpenLayers") ist eine JavaScript-Bibliothek, die es ermöglicht, Geodaten im Webbrowser anzuzeigen. Bei OpenLayers handelt es sich um eine Programmierschnittstelle, die eine clientseitige Entwicklung unabhängig vom Server zulässt.

[https://openlayers.org/](https://openlayers.org/)

### [](#7-leaflet)7\. Leaflet

[Leaflet](/gis/react-leaft-erste-schritte "Leaflet") ist eine freie JavaScript-Bibliothek, mit der WebGIS-Anwendungen erstellt werden können. Die Bibliothek verwendet HTML5, CSS3 und unterstützt somit die meisten Browser.

[https://leafletjs.com/](https://leafletjs.com/)