---
layout: "post"
title: "Einfacher Download für 30-Meter SRTM Tiles"
date: "2019-04-20"
description: "Derek Watkins hat eine einfache und benutzerfreundliche Oberfläche zum Herunterladen von 30-Meter-SRTM-Daten entwickelt. Um 30-Meter-SRTM Tiles herunterzuladen, muss man nur auf den gewünschten Bereich Zoomen und die gewünschte Kachel anklicken"
category: "GIS"
tags: ["Fernerkundung"]
image: "/assets/img/meter-srtm-download.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

[Derek Watkins](https://twitter.com/dwtkns) hat eine einfache und benutzerfreundliche Oberfläche zum Herunterladen von 30-Meter-SRTM-Daten entwickelt.

![30-Meter SRTM Tile Download](30-Meter-SRTM-Tile-Download.jpg)

[https://dwtkns.com/srtm30m/](https://dwtkns.com/srtm30m/)

Um 30-Meter-SRTM Tiles herunterzuladen, muss man nur auf den gewünschten Bereich Zoomen und die gewünschte Kachel anklicken.

Diese kann man dann entweder sofort herunterladen oder sich eine Vorschau des DEM anzeigen lassen.

Die SRTM-Daten werden als SRTMHGT-Dateien mit einer Auflösung von einer Winkelsekunde (3601 x 3601 Pixel) gepackt und von den NASA-Servern aberufen.

Die Schnitstelle wurd mit Mapbox GL JS und Carto DB erstellt

## [](#90-meter-srtm-daten)90-Meter SRTM-Daten

Sie suchen nach 90-Meter-SRTM-Daten?

[https://dwtkns.com/srtm/](https://dwtkns.com/srtm/)

Watkins bietet auch den SRTM Tile Grabber an, der Links zu GeoTIFF-Dateien enthält, die mit CIAT-CSI erstellt wurden. Es gibt eine entsprechende KMZ-Datei, die einen 90-Meter-Index für SRTM-Daten enthält.

### [](#auch-interessant)Auch interessant:

*   [Geodaten online - Download kostenlos](/gis/geodaten-deutschland-download)
*   [Was sind Geodaten?](/gis/-was-sind-geodaten)
