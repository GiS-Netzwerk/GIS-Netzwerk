---
layout: "post"
title: "GIS Software Optionen - Open Source, kostenlos und kostenpflichtig"
date: "2018-04-23"
description: "Liste mit Open Source, kostenloser und kostenpflichtiger GIS Software Optionen"
category: "GIS"
tags: ["Software"]
image: "/assets/img/gis-software-optionen.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---


Hier findest du eine Liste mit Open Source, kostenloser und kostenpflichtiger [GIS](/gis/was-ist-gis "Was ist GIS?") Software Optionen von verschiedenen GIS Software Anbieter.

## Kostenlose und Open Source Desktop GIS Software

**1\. [QGIS](https://www.qgis.org "QGIS")**

Ein freies Open-Source-Geographisches-Informationssystem.  
Erstellen, bearbeiten, anzeigen, analysieren räumlicher Information unter Windows, Mac, Linux, BSD und Android).

**2\. [GRASS GIS](https://grass.osgeo.org/ "GRASS GIS")**

GRASS GIS ist eine hybride, modular aufgebaute Geoinformationssystem-Software mit raster- und vektororientierten Funktionen.

**3\. [SAGA GIS](http://www.saga-gis.org/ "SAGA GIS")**

Software für automatisierte geowissenschaftliche Analysen. Das SAGA-Projekt wird hauptsächlich am Fachbereich Geographie der Universität Hamburg entwickelt.

**4\. [JUMP GIS/OpenJUMP](http://www.openjump.org/ "JUMP GIS/OpenJUMP")**

Auf Java basierendes [Open-Source](/tags/open-source) GIS.

**5\. [GeoDa](https://geodacenter.github.io/ "GeoDa")**

GeoDa ist eine kostenloses Open-Source Software und dient als Einführung in die Geodatenanalyse.

**6\. [gvSIG](http://www.gvsig.com/en "gvSIG")**

Open-Source Desktop, Online und Mobile GIS.

**7\. [MapmakerPro](http://www.mapmaker.com/v4/Map%20maker%204.Pro.htm "MapmakerPro")**

MapMaker richtet sich an Spezialisten, die Karten erstellen müssen. Zum Beispiel Förster, Archäologen, Rettungsdienste, usw.

**8\. [DIVA GIS](http://www.diva-gis.org/ "DIVA GIS")**

DIVA-GIS ist ein kostenloses GIS zur Kartierung und Analyse geographischer Daten.

**9\. [TerraLib](http://www.dpi.inpe.br/terralib_previous/index.php "TerraLib")**

TerraLib ist eine Open-Source-GIS-Softwarebibliothek, die die Entwicklung kundenspezifischer geographischer Anwendungen unterstützt.

**10\. [Kalypso](https://sourceforge.net/projects/kalypso/ "Kalypso")**

Kalypso ist ein Open-Source-Modellierungsprogramm. Der Schwerpunkt liegt auf numerischen Simulationen im Wasermanagement und in der Ökologie.

**11\. [OrbisGIS](http://orbisgis.org/ "OrbisGIS")**

OrbisGIS ist ein plattformübergreifendes Open-Source-GIS, das von der Forschung und für die Forschung entwickelt wurde.

**12\. [OzGIS](https://sourceforge.net/projects/ozgis/ "OzGIS")**

OzGIS ist ein GIS zur Analyse und Darstellung von Statistiken.

**13\. FalconView**

FalconView ist ein vom Georgia Tech Research Institute entwickeltes GIS.

**14\. [ILWIS](https://www.itc.nl/ilwis/ "ILWIS")**

Das integrierte Informationssystem für Land und Wasser (ILWIS) ist eine PC-basierte GIS- und Fernerkundungssoftware, die von ITC bis Release 3.3 im Jahr 2005 entwickelt wurde.

**15\. [MapWindow GIS](https://www.mapwindow.org/ "MapWindow GIS")**

MapWindow GIS ist eine Open Source GIS-Desktopanwendung, die von einer großen Anzahl von Benutzern und Organisationen auf der ganzen Welt verwendet wird.

**16\. [Whitebox GAT](https://www.uoguelph.ca/~hydrogeo/Whitebox/ "Whitebox GAT")**

Whitebox Geospatial Analysis Tools ist ein Open Source und plattformübergreifendes Geoinformationssystem und Fernerkundungssoftwarepaket.

**17\. [Capaware](http://www.capaware.org/ "Capaware")**

3D-World-Viewer.

**18\. [Generic Mapping Tools](http://gmt.soest.hawaii.edu/ "Generic Mapping Tools")**

Die Generic Mapping Tools sind eine Sammlung freier Software, zur Erstellung von geologischen oder geographischen Karten und Diagrammen.

## Kostenpflichtige Desktop GIS Software

**19\. ArcGIS, ArcView**

ArcGIS ist der Oberbegriff für verschiedene GIS Softwareprodukte von Esri.

**20\. AutoCAD Map3D**

AutoCAD Map3D von Autodesk ist eine GIS Software Lösung und bietet umfassenden Zugriff auf alle CAD- und GIS-Daten und ermöglicht dessen Erstellung und Bearbeitung.

**21\. Aquaevo GIS**

Aquaveo ist ein Software-Unternehmen für die Modellierung von Umwelt- und Wasserressourcen

**22\. Bentley Map**

**23\. Cadcorp**

Cadcorp's GIS und Web Mapping Software sind GIS Software Produkte zur Erstellung, Analyse und dem Datenmanagment von Geodaten.

**24\. Conform**

Conform ist eine GIS Software zum Zusammenführen, Visualisieren, Bearbeiten und Exportieren von 3D-Umgebungen für Stadtplanung, Spiele und Simulationen.

**25\. Dragon / ips**

Dragon / ips ist eine Fernerkundungs-Bildverarbeitungssoftware.

**26\. ENVI**

Die ENVI-Bildanalysesoftware wird von GIS-Experten, Fernerkundungswissenschaftlern und Bildanalytikern verwendet, um aussagekräftige Informationen aus Bildern zu extrahieren, um bessere Entscheidungen treffen zu können.

**27\. ERDAS IMAGINE**

ERDAS IMAGINE ist eine Software zur Auswertung von Fernerkundungs-Daten, speziell von Grafiken und Photos.

**28\. Field-Map**

Field-Map ist ein proprietäres integriertes Werkzeug für die programmatische Felddatenerfassung von IFER - Monitoring and Mapping Solutions, Ltd. Es wird hauptsächlich zur Zuordnung von Waldökosystemen und zur Datenerfassung während der Feldanalyse verwendet.

**29\. Geosoft**

GEOSOFT gehört zu Deutschlands führenden Entwicklern geodätischer Rechen- und Organisationssoftware für private und öffentliche Vermessungsstellen.

**30\. GeoTime**

GeoTime ist eine Geodatenanalyse-Software, die die visuelle Analyse von Ereignissen im Zeitverlauf ermöglicht. Als dritte Dimension wird einer zweidimensionalen Karte Zeit hinzugefügt, sodass Benutzer Änderungen in Zeitreihendaten sehen können.

**31\. Global Mapper**

Geographisches Informationssystem mit Distanz- und Gebietsberechnung; bietet eine integrierte Skriptsprache, 3D-Darstellung und GPS-Tracks.

**32\. Golden Software**

Surfer und Mapviewer sind zwei Software Lösungen mit einer Vielzahl von Mapping- und Anpassungsoptionen und unterstützen jegliche Geodatenformate (unter anderem LiDAR-Daten), 3D-Visualisierung, sowie Volumen- / Entfernungs- / Flächenkalkulationen.

**33\. Intergraph**

GeoMedia ist eine GIS Software von Intergraph. GeoMedia ist eine Software-Produktfamilie mit Desktop-GIS, Web-GIS und richtet sich hauptsächlich an Kommunen..

**34\. Manifold System**

Manifold System ist eine Software zum Management digitaler Landkarten. Es lassen sich digitale Karten und Fernerkundungsdaten einfach bearbeiten

**35\. MapInfo**

MapInfo Professional ist eine Geoinformationssystem-Software des US-amerikanischen Unternehmens MapInfo Corporation

**36\. Maptitude**

Maptitude ist ein von Caliper Corporation erstelltes Mapping-Softwareprogramm, mit dem Benutzer Karten anzeigen, bearbeiten und integrieren können. Die Software und die Technologie sollen die geografische Visualisierung und Analyse von enthaltenen Daten oder benutzerdefinierten externen Daten erleichtern

**37\. Netcad**

NETCAD GIS ist eine CAD- und GIS Software, die internationale Standards unterstützt und für Benutzer von Ingenieur- und geographischen Informationssystemen konzipiert wurde.

**38\. RegioGraph**

RegioGraph ist eine Geomarketing-Software, spezialisiert auf Fragestellungen in den Businessbereichen Marketing, Vertrieb, Controlling, Logistik und Unternehmensstrategie.

**39\. RIWA GIS Zentrum**

Das RIWA GIS-Zentrum ist ein leistungsstarkes, webbasierendes geografisches Informationssystem, das seit vielen Jahren in zahlreichen kommunalen Verwaltungen und Industriebetrieben im Einsatz ist.

**40\. Smallworld**

Smallworld GIS ist das professionelle Geoinformationssystem für Netzbetreiber in der Energie- und Wasserwirtschaft.

**41. TNTmips**

TNTmips ist ein Geodatenanalysesystem, das ein voll ausgestattetes GIS-, RDBMS- und automatisiertes Bildverarbeitungssystem mit CAD-, TIN-, Oberflächenmodellierungs-, Kartenlayout- und innovativen Datenveröffentlichungs-Tools bietet.

**42\. TerrSet ( IDRISI )**

TerrSet ist ein integriertes geografisches Informationssystem und eine Fernerkundungssoftware zur Überwachung und Modellierung des Erdsystems.

**43\. [Google Earth Pro](https://www.google.de/earth/download/gep/agree.html "Google Earth Pro")**

## [](#online-gis)Online GIS

**44\. [Bing Maps](http://www.bing.com/maps/ "Bing Maps")**

Bing Maps ist ein Online-Kartendienst von Microsoft, durch den sich verschiedene raumbezogene Daten betrachten und raumbezogene Dienste nutzen lassen. Es handelt sich um eine Weiterentwicklung des MSN Virtual Earth und ist Teil der Suchmaschine Bing.

**45\. [Google Maps](http://maps.google.de/ "Google Maps")**

Google Maps ist ein Online-Kartendienst des US-amerikanischen Unternehmens Google LLC. Die Erdoberfläche kann als Straßenkarte oder als Luft- oder Satellitenbild betrachtet werden, wobei auch Standorte von Institutionen oder bekannten Objekten angezeigt werden. Der Dienst wurde am 8\. Februar 2005 gestartet.

**46\. [NASA World Wind](http://worldwind.arc.nasa.gov/java/ "NASA World Wind")**

NASA World Wind ist eine Open-Source-Software, die es ermöglicht, Satelliten- und Luftbilder auf einem virtuellen Globus kombiniert mit Höhendaten anzuzeigen und jeden beliebigen Ort der Erde in 3D-Grafik heranzuzoomen und frei von allen Seiten zu betrachten.

**47\. [OpenStreetMap](http://www.openstreetmap.org/ "OpenStreetMap")**

OpenStreetMap ist ein freies Projekt, das frei nutzbare Geodaten sammelt, strukturiert und für die Nutzung durch jedermann in einer Datenbank vorhält. Diese Daten stehen unter einer freien Lizenz, der Open Database License.

**48\. [Wikimapia](http://www.wikimapia.org/ "Wikimapia")**

Wikimapia ist eine Weboberfläche, die Karten mit einem eingeschränkten Wikisystem ohne Hypertextfunktionen kombiniert. Es erlaubt dem Benutzer, Informationen in Form einer Notiz an jede Position der Erde hinzuzufügen.