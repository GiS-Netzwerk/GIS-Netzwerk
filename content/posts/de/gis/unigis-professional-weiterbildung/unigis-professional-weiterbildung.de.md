---
layout: "post"
title: "UNIGIS Weiterbildung - Geoinformatik"
date: "2018-05-06"
description: "Die Universität bietet attraktive Weiterbildungsmöglichkeiten für Geoinformatik"
category: "GIS"
tags: ["Geoinformatik", "Weiterbildung"]
image: "/assets/img/unigis-weiterbildung-geoinformatik.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

## Berufsbegleitendes Fernstudium mit UNIGIS

Die Universität Salzburg bietet attraktive Weiterbildungen in Geoinformatik und [GIS](/gis/was-ist-gis "Was ist GIS?") an, die fundiertes Wissen vermitteln und in vielen Fachbereichen unverzichtbare Qualifikationen geworden sind. Zur Auwahl stehen zwei Weiterbildungsmöglichkeiten, die berufsbegleitend als Fernstudium absolviert werden können:

*   **UNIGIS professional**

und weiterführend

*   **UNIGIS MSc**

## UNIGIS professional

UNIGIS professional vermittelt praxisorientierte Kenntnisse zu GIS und richtet sich an alle, die möglichst schnell einen umfassenden Einblick in die Methodik und den Einsatz von Geoinformatik benötigen.

Der Universitätslehrgang wird als berufsbegleitendes Fernstudium angeboten und führt zu dem Zertifikat "Akademische/r GeoinformatikerIn". Die Lehrgangsdauer beträgt ca. 1 Jahr (berufsbegleitend, durchschnittliche wöchentliche Arbeitsbelastung etwa 12-15 Stunden).

UNIGIS professional besteht aus 7 Studienmodulen und einem Wahlpflichtfach. Module sind inhaltlich zusammenhängende Abschnitte und entsprechen jeweils einer vierstündigen Lehrveranstaltung an der Universität Salzburg. Besuchen Sie das UNIGIS professional "Schnuppermodul" um eine kleine "Kostprobe" zu machen.

### Übersicht:

*   [Studienführer UNIGIS professional](https://www.unigis.at/files/UNIGIS%20Studienfuehrer%20prof.pdf "Studienführer UNIGIS professional")
*   [Übersicht Studienablauf](https://www.unigis.at/files/Uebersicht_Studienablauf_UNIGIS_professional.pdf "Übersicht Studienablauf professional")
*   [Studienplan](https://www.unigis.at/files/curriculum_unigis-prof.pdf "Studienplan professional")
*   [Schnuppermodul](https://www.unigis.at/schnuppermodul/index.html "Schnuppermodul")
*   [Wahlpflichtfach - optionale Module](https://www.unigis.at/index.php/club-unigis/weiterbildung#optionale_module "Wahlpflichtfach - optionale Module - professional")
*   [Häufig gestellte Fragen](https://www.unigis.at/index.php/fernstudien/faq "Häufig gestellte Fragen")

## UNIGIS MSc

UNIGIS MSc zielt als postgradualer, zweijähriger Studiengang mit dem akademischen Abschluss Master of Science (Geographical Information Science & Systems) auf die umfassende Vermittlung fundierter Kenntnisse zu Ansatz und Einsatz von GeoInformatik. Absolventen sind insbesondere zur Leitung von Projekten, Arbeitsgruppen und Abteilungen qualifiziert.

Das Studienprogramm des Universitätslehrgangs besteht aus 12 Fächern, wobei die ersten 9 Lehrveranstaltungen als (Pflicht-)Module bezeichnet werden. Die verbleibenden Fächer 'Studienbegleitung und wissenschaftliches Arbeiten', das Wahlpflichtfach 'Angewandte Geoinformatik' sowie die 'Master Thesis' sind individuell in Abstimmung mit der Lehrgangsleitung planbar.

Der Durchschnittswert wöchentlicher Arbeitsbelastung liegt bei ca. 12-15 Stunden in Abhängigkeit von Modulinhalt und persönlichen Vorkenntnissen. Die Kommunikation zwischen Teilnehmern und Lehrgangsteam bzw. den jeweiligen Lehrenden erfolgt durch Diskussionsforen und Online-Meetings.

### Übersicht:

*   [Studienführer UNIGIS MSc](https://www.unigis.at/files/UNIGIS%20Studienfuehrer%20MSc.pdf "Studienführer UNIGIS MSc")
*   UNIGIS MSc 2018 beginnt am 1\. März 2018 mit einführenden Studientagen (1.-2\. März) in Salzburg
*   [Übersicht Studienablauf](https://www.unigis.at/files/Uebersicht_Studienablauf_UNIGIS_MSc.pdf "Übersicht Studienablauf MSc")
*   [Studienplan](https://www.unigis.at/files/curriculum_unigis-msc.pdf "Studienplan MSc")
*   [Wahlpflichtfach - optionale Module](https://www.unigis.at/index.php/club-unigis/weiterbildung#optionale_module "Wahlpflichtfach - optionale Module - MSc")
*   [Häufig gestellte Fragen](https://www.unigis.at/index.php/fernstudien/faq "Häufig gestellte Fragen")

Quelle: [UNIGIS Salzburg](https://www.unigis.at/index.php/fernstudien "UNIGIS Salzburg")
