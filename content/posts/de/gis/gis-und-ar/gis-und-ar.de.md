---
layout: "post"
title: "GIS und Augmented Reality (AR)"
date: "2019-04-11"
description: "Über Satellitennavigation und Echtzeit-Lokalisierung wird der aktuelle Standort ermittelt und über die Verbindung zu einer räumlichen Datenbank, werden Geodaten am momentanen Standort eingeblendet"
category: "GIS"
tags: ["AR"]
image: "/assets/img/gis-und-ar.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Augmented Reality ergänzt die Realität um virtuelle Objekte und Informationen.

Über Satellitennavigation und Echtzeit-Lokalisierung wird der aktuelle Standort ermittelt und über die Verbindung zu einer räumlichen Datenbank, werden [Geodaten](/gis/was-sind-geodaten "Was sind Geodaten") am momentanen Standort eingeblendet. Dadurch kann in vielen Szenarien ein erheblicher Mehrwert geschaffen werden.

## [](#gis-und-ar)GIS und AR

Viele kennen AR bereits aus dem Smartphone Spiel [Pokémon GO](https://de.wikipedia.org/wiki/Pok%C3%A9mon_Go), das übrigens auf [OpenStreetMap](https://www.openstreetmap.org/) Daten basiert. Aktuelle Smartphones können mittlerweile auch in das Livebild der Kamera Objekte platzieren.

Alle modernen Städte und Gemeinden verfügen über weitreichende und komplexe unterirdische Versorgungsnetze, wie zum Beispiel Wasser-, Abwasser, Strom-, Erdgas, Telefon- oder Breitbandleitungen. Je mehr Einrichtungen unterirdisch verlaufen, desto komplizierter wird es das gesamte Versorgungsnetz zu warten.

Arbeiter vor Ort können nur schwer die Lage einzelner unterirdischer Objekte bestimmen. Sie sind auf Karten mit veralteten Daten angewiesen, die oft nichtmehr genau sind. Dies führt häufig zu unbeabsichtigten Schäden an vergrabenen Leitungen.

Mithilfe einer aktuellen Vermessung bestehender und neu verlegter Leitungen können exakte Geodaten erstellt werden, die in GIS-Datenbanken gespeichert und in einem Geoinformationssystem dargestellt werden.

Der österreichische GIS-Anbieter Grintec präsentierte auf der INTERGEO 2016 das mobile GIS Augview. Die klassische Kartenansicht aus einem [Geoinformationssystem](/gis/was-ist-gis "Was ist GIS?") wird dabei mit der Augmented-Reality-Ansicht verbunden und Geodaten werden in 3D sichtbar.

![Augview_Grintec](/static/dc422fa39aeec1f592fd8aadb609ae80/14c49/Still023_QUELLE_-Augview_Grintec.png "Augview_Grintec")

Es können Einbauteile, Schieber oder Leitungsdaten zu einem Wassernetz, die sonst unterirdisch verborgen wären, live in das Kamerabild eines Smartphones oder Tablets eingeblendet werden.<sup>[1]</sup>

Auch bei Überschwemmungen können alle unterirdischen Leitungen exakt lokalisiert werden.

Neue geplante Häuser können 3D auf das zukünftige Grundstück eingeblendet werden, was eine realitätsgetreue Präsentation der Planung ermöglicht.

Sämtliche Attribute zu den Objekten können auch abgefragt oder editiert werden. Die Daten werden online über einen Geo-Webserver bereitgestellt, weshalb man eine stabile Internetverbindung benötigt.

Auch bei Esri hat man Augmented Reality in die Produktpalette aufgenommen.

Vorallem bei 3D-Stadtmodellen hat GIS in Verbindung mit Augemented Reality ein enormes Potential. Mit der Einbindung von Echtzeit-Informationen in Geodaten können viele Problemstellungen effizienter gelöst werden.

Quellen:

<sup>[1]</sup> Grindtec: [https://www.grintec.com/Augview](https://www.grintec.com/Augview)