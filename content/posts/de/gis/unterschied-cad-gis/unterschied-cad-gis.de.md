---
layout: "post"
title: "GIS vs CAD - Der Unterschied zwischen GIS und CAD"
date: "2019-02-15"
description: ""
category: "GIS"
tags: ["CAD", "Geodaten"]
image: "/assets/img/cad-gis-unterschied.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

## Der Unterschied zwischen GIS und CAD

[Geoinformationssystem](/gis/was-ist-gis "Was ist GIS?") (GIS) bezeichnet ein System zur Darstellung und Bearbeitung von Geodaten, also Daten, denen eine räumliche Lage zugewiesen wurde. Mit einem Geoinformationssystem kann man komplexe Sachverhalte strukturiert darstellen, präsentieren und analysieren.

Mit CAD-Systeme kann man digitale Inhalte erstellen und/oder graphisch modellieren. Diese sind üblicherweise Pläne bzw. Zeichnungen, sowie 3D-Modelle.

Da die projizierten Daten in beiden Systemen einen Bezug zur realen Welt haben sollen, werden verschiedene Maßstäbe, Größen und Detaillierungsebenen verwendet. In GIS wird dies durch unterschiedliche Darstellungsmodelle geregelt, wo definiert wird, welche Objekte unter welchen Bedingungen dargestellt werden. In CAD spielen Maßstäbe eine wichtigere Rolle. Die Hauptunterschiede zwischen CAD und GIS sind:

*   [GIS-Daten bzw. Geodaten](/gis/was-sind-geodaten "GIS-Daten bzw. Geodaten") benötigen einen örtlichen Bezug im Gegensatz zu CAD-Daten
*   In GIS liegt der Fokus auf der Visualisierung, Pflege und Analyse von Daten;
*   In CAD spielt eine sehr präzise Darstellung eine wichtigere Rolle damit mithilfe der Pläne z.B. Bauteile produziert werden können
*   GIS-Daten sind sehr vielfältig und es können die verschiedensten Datenformate und -quellen zusammen kommen
*   CAD-Daten werden hauptsächlich als DXF- oder DWG-Datei abgespeichert
*   Ein GIS ist wesentlich effizienter und flexibler im Datenmanagement als ein CAD-System

Beide Systeme können auch kombiniert genutzt werden, da sie sich ideal ergänzen.