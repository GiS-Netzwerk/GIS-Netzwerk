---
layout: "post"
title: "Hochaufloesende Satellitenbilder downloaden - Fernerkundung"
date: "2019-05-20"
description: "Für alle die noch nicht die entsprechenden Anlaufstellen für aktuelle Satellitenbilder kennen, gibt es hier ein paar weiterführende Links bei denen man weltweite Satellitenbilder größtenteils kostenlos downloaden kann."
category: "GIS"
tags: ["Fernerkundung", "Geodaten"]
image: "/assets/img/hochaufloesende-satellitenbilder-downloaden.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Für alle die noch nicht die entsprechenden Anlaufstellen für aktuelle Satellitenbilder kennen, gibt es hier ein paar weiterführende Links bei denen man weltweite Satellitenbilder größtenteils kostenlos downloaden kann.

## [](#satellitenbilder-downloaden)Satellitenbilder downloaden

### [](#1-copernicus-open-access-hub)1\. Copernicus Open Access Hub

Der [Open Access Hub von Copernicus](https://scihub.copernicus.eu/dhus) (Sentinels Scientific Data Hub) bietet kostenlosen und offenen Zugriff auf Sentinel-1, Sentinel-2 und Sentinel-5P-Produkte.

Sentinel-Daten sind auch über die Copernicus-Daten- und Informationszugriffsdienste (DIAS) auf mehreren Plattformen verfügbar.

[https://www.copernicus.eu/de/datenzugriff/dias](https://www.copernicus.eu/de/datenzugriff/dias)

Die Sentinel-Satelliten sind Satelliten der [ESA](https://www.esa.int/ESA) im Rahmen des Copernicus-Programm. Dieses Programm wurde in Zusammenarbeit mit der EU geschaffen und soll eine Erdbeobachtung insbesondere für die Bereiche Umwelt, Verkehr, Wirtschaft und Sicherheitspolitik ermöglichen.

![Copernicus Satelliten](./sentinel-satellites-copernicus-programme-1-678x322.png "Copernicus Satelliten")Credit: ESA/DLR/FU Berlin, [CC BY-SA 3.0 IGO](https://creativecommons.org/licenses/by-sa/3.0/igo/)

Alle Sentinel-Daten, die über den Sentinel Data Hub verfügbar sind, unterliegen der [Legal Notice on the use of Copernicus Sentinel Data and Service Information](https://sentinels.copernicus.eu/documents/247904/690755/Sentinel_Data_Legal_Notice).

### [](#2-geoss-portal)2\. GEOSS Portal

Das [GEOSS-Portal](http://www.geoportal.org/) ist eine kartenbasierte Online-Benutzeroberfläche, mit der Benutzer Erdbeobachtungsdaten und -ressourcen von verschiedenen Anbietern aus aller Welt downloaden können.

Auch dieses Portal wird von der Europäischen Weltraumorganisation (ESA) betrieben.

### [](#3-worldview---nasa)3\. Worldview - NASA

[NASA Worldview](https://worldview.earthdata.nasa.gov/) bietet eine interaktive Benutzeroberfläche um nach hochauflösenden und globalen Satellitenbildern zu suchen. Außerdem werden auch thematische Bilder zu Waldbränden, Luftqualität, Hochwasserüberwachung und mehr angeboten.

### [](#4-european-space-imaging)4\. European Space Imaging

[European Space Imaging](https://www.euspaceimaging.com/) ist der führende Anbieter von VHR-Satellitenbildern (Very High Resolution) für Europa, Nordafrika und die GUS-Staaten.

### [](#5-glovis)5\. GloVis

Seit 2001 steht Benutzern der [USGS Global Visualization Viewer](https://glovis.usgs.gov/) (GloVis) für den Zugriff auf Fernerkundungsdaten zur Verfügung. Im Jahr 2017 wurde es neu gestaltet, um den sich ändernden Internettechnologien Rechnung zu tragen. Mit benutzerfreundlichen Navigationstools können Benutzer Szenen sofort anzeigen und herunterladen.

### [](#6-geostore---airbus-defence-and-space)6\. GeoStore - Airbus Defence and Space

Im [GeoStore](https://www.intelligence-airbusds.com/geostore/) der von AIRBUS betrieben wird können hochauflösende und aktuelle Satellitenbilder bestellt werden.

### [](#7-eoweb-geoportal---dlr)7\. EOWEB GeoPortal - DLR

[EOWEB GeoPortal](https://eoweb.dlr.de/egp/) (EGP) ist ein Multi-Mission-Webportal für den interaktiven Zugriff auf die DLR-Erdbeobachtungsdatenbestände.