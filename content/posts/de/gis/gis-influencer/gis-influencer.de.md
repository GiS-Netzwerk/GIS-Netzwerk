---
layout: "post"
title: "Die bekanntesten Geospatial und GIS Influencers"
date: "2019-03-14"
description: "Hier findest du eine Liste der bekanntesten GIS Influencers mit Links zu deren Webprofilen, sodass du immer auf dem aktuellen Stand bleibst."
category: "GIS"
tags: []
image: "/assets/img/gis-influencer.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

# [](#geospatial-und-gis-influencers)Geospatial und GIS Influencers

Auch im Bereich [GIS](/gis/as-ist-gis "Was ist GIS?") und [Geodaten](/gis/was-sind-geodaten "Was sind geodaten?") gibt es natürlich zahlreiche GIS Influencers, die ihr Wissen über Social Media Kanäle und deren Blogs bzw. Websiten verbreiten und Menschen dabei inspirieren.

Hier findest du eine Liste der bekanntesten GIS Influencers mit Links zu deren Webprofilen, sodass du immer auf dem aktuellen Stand bleibst.

Zuerst aber noch eine Twitter Liste von CARTO, bei der du mit einem Klick 50 bekannten GIS und Geography Influencern folgen kannst: [CARTO Twitter Guide 2018.](https://twitter.com/CARTO/lists/carto-twitter-guide-2018)

## [](#top-gis-influencers)Top GIS Influencers

### [](#1-anita-graser)1\. Anita Graser

![GIS Influencers Anita Graser](https://underdark.files.wordpress.com/2007/03/img_20161121_003528.jpg?w=150&h=150)

_Bildquelle: [https://anitagraser.com/about/](https://anitagraser.com/about/); [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en_US)_

**Data Scientist und Open-Source-GIS Expertin**

Anita Graser ist in Österreich tätig als Autorin, Data Scientist und Open-Source-GIS-Expertin in den Bereichen Algorithmen, Mobilitätsforschung, Open-Source-GIS-Lösungen, Geodatenanalyse und Geodatenvisualisierung, sowie Spatial Big Data.

Sie ist Teil des QGIS Teams und seit 2013 QGIS PSC Member. Insgesamt hat sie bereits 13 Jahre GIS Erfahrung gesammelt.

Anita Graser hat zahlreiche sehr bekannte QGIS Tutorials - wie [QGIS Map Design*](https://amzn.to/2HwGN6A) oder [Learning QGIS*](https://amzn.to/2F7J2KS) in Form von Büchern veröffentlicht. (Die komplette Liste findest du [hier*](https://amzn.to/2O3KC4h).) Außerdem unterrichtet sie an der Universität Salzburg für die berufsbegleitenden Geoinformatik Fernstudiengänge, die im Rahmen von [UNIGIS](/gis/unigis-professional-weiterbildung) angeboten werden. Hier kannst du ihr folgen:

*   [Twitter](https://twitter.com/underdarkgis?lang=de)
*   [GitHub](https://github.com/anitagraser/)
*   [StackExchange](https://gis.stackexchange.com/users/187/underdark)
*   [Linkedin](https://www.linkedin.com/in/anita-graser-95102530/)
*   [Youtube](https://www.youtube.com/anitagraser/)
*   [Xing](https://www.xing.com/profile/Anita_Graser2/cv)
*   [anitagraser.com](https://anitagraser.com/)

### [](#2-ted-mackinnon)2\. Ted MacKinnon

![GIS Influencers Ted MacKinnon](https://gogeomatics.ca/wp-content/uploads/MacKinnon2.jpg)

_Bildquelle: [https://gogeomatics.ca/author/tmackinnon/](https://gogeomatics.ca/author/tmackinnon/)_

**Certified Geomatics Specialist**

Ted MacKinnon ist aus Kanada und ein Profi mit mehr als 20 jahren Erfahrung im Bereich [GIS](/gis/was-ist-gis "Was ist GIS?"), Fernerkundung, Kartografie, LIDAR und GPS-Vermessung in der privaten Wirtschaft, im akademischen Bereich, in non-profit Organisationen, sowie im öffentlichen Sektor. Er inspiriert die jüngere [Geodaten](/gis/was-sind-geodaten)-Community, mehr über kanadische geographische Informationen zu erfahren.

*   [Twitter](https://twitter.com/tedmackinnon?lang=de)
*   [Instagram](https://www.instagram.com/t_mackinnon/)
*   [Facebook](https://www.facebook.com/ted.mackinnon)
*   [Linkedin](https://www.linkedin.com/in/tedmackinnon/?originalSubdomain=ca)
*   [Youtube](https://www.youtube.com/channel/UCldWLa9bKxS7KDZlWGImRrw)
*   [Xing](https://www.xing.com/profile/Ted_MacKinnon/cv)
*   [tmackinnon.com](https://tmackinnon.com/)

### [](#3-gretchen-peterson)3\. Gretchen Peterson

![GIS Influencers Gretchen Peterson](https://pbs.twimg.com/profile_images/933003884615802880/kqQ_3Su__400x400.jpg)

_Bildquelle: [https://twitter.com/petersongis?lang=de](https://twitter.com/petersongis?lang=de)_

**Cartographer, Data Scientist und Product Developer**

Gretchen Peterson ist eine Kartografin, Data Scientist und Produktentwickler bei [PetersonGIS](http://petersongis.com), sowie Autorin in Colorado. Sie ist Expertin für die Inferenz und Visualisierung von Geodaten. Außerdem erstellt Gretchen Peterson Karten für Tegola. Sie veröffentlichte bereits Bücher wie [Cartographer's Toolkit: Colors, Typography, Patterns*](https://amzn.to/2TFht5u) und [City Maps: A coloring book for adults*](https://amzn.to/2Fc4Ztg).

*   [Twitter](https://twitter.com/petersongis?lang=de)
*   [Linkedin](https://www.linkedin.com/in/gretchenpeterson/?locale=de_DE)
*   [Youtube](https://www.youtube.com/channel/UC5JSHHSkGx7Et7RDRZzv0sQ)
*   [gretchenpeterson.com](https://www.gretchenpeterson.com/)

### [](#weitere-bekannte-pers%C3%B6nlichkeiten)Weitere bekannte Persönlichkeiten

24 weitere bekannte GIS Experten mit Links zu deren Twitter Profilen:

*   [Amanda Taub](http://twitter.com/amandahstaub)
*   [Andy Gup](http://twitter.com/agup)
*   [Alex Chaucer](http://twitter.com/geoparadigm)
*   [Bill Dollins](http://twitter.com/billdollins)
*   [James Fee](http://twitter.com/cageyjames)
*   [Valerie Yakich](http://twitter.com/GeoEntelechy)
*   [Glenn Letham](http://twitter.com/gletham)
*   [GIS Geospatial News](http://twitter.com/gisuser)
*   [Geospatial News](http://twitter.com/geospatialnews)
*   [Kate Chapman](http://twitter.com/wonderchook)
*   [Jim Barry](http://twitter.com/JimBarry)
*   [OpenGeo](http://twitter.com/OpenGeo)
*   [Dave Smith](http://twitter.com/DruidSmith)
*   [Andrew Turner](http://twitter.com/ajturner)
*   [OpenStreetMap](http://twitter.com/openstreetmap)
*   [Dave Bouwman](http://twitter.com/dbouwman)
*   [Don Meltz](http://twitter.com/DonMeltz)
*   [Jesse Rouse](http://twitter.com/kindaspatial)
*   [LindaHecht](http://twitter.com/LindaHecht)
*   [Jason Birch](http://twitter.com/jasonbirch)
*   [Justin C. Houk](http://twitter.com/GEOpdx)
*   [Adam Estrada](http://twitter.com/GeoDAWG)
*   [Randal Hale](http://twitter.com/rjhale)
*   [Joe Francica](http://twitter.com/joefrancica)

Das ist immernoch nicht genug? Hier findest du noch mehr GIS Influencer (dank opengeo): [Top 100 Geospatial Influencer](https://docs.google.com/spreadsheet/ccc?key=0Ana1iJKeRrCwdHhzdEt1d0ZuQVB2Y3NGc3dWVG5yTFE&usp=sharing)