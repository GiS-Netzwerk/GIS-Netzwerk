---
layout: "post"
title: "Geo und GIS Podcasts um auf dem aktuellen Stand zu bleiben"
date: "2019-04-20"
description: "Podcasts sind eine großartige Möglichkeit, um bei aktuellen Entwicklungen auf dem aktuellsten Stand zu bleiben. Das Beste von allem ist, dass man Podcasts praktisch überall nebenbei hören kann."
category: "GIS"
tags: ["Podcasts"]
image: "/assets/img/geo-und-gis-podcasts-um-auf-dem-aktuellen-stand-zu-bleiben.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Podcasts sind eine großartige Möglichkeit, um bei aktuellen Entwicklungen auf dem aktuellsten Stand zu bleiben. Das Beste von allem ist, dass man Podcasts praktisch überall nebenbei hören kann.

## [](#1-veryspatial)**1\. VerySpatial**

Der Podcast wird von Jesse Rouse, Sue Bergeron und Frank Lafone gehostet. Ein etablierter Podcast, in dem Geografie, Geoinformationstechnologien und die Auswirkungen von [GIS](/gis/was-ist-gis "Was ist GIS") auf den digitalen Alltag besprochen werden.

[http://veryspatial.com/](http://veryspatial.com/)

![VerySpatial Podcast](VSLogo_Small_400x400.jpg "VerySpatial")

## [](#2-mapscaping)**2\. Mapscaping**

Neuer Podcast. Hauptsächlich Interviews mit Personen aus der GIS und Geo Industrie.

[https://mapscaping.com/blogs/the-mapscaping-podcast](https://mapscaping.com/blogs/the-mapscaping-podcast)

![Mapscaping Podcast](/gedY7m5O_400x400.jpg "Mapscaping")

## [](#3-radio-osm)**3\. Radio OSM**

Berichte und Neuigkeiten rund um [OpenStreetMap](http://openstreetmap.de), ​die freie Wiki-Weltkarte.

[https://podcast.openstreetmap.de/](https://podcast.openstreetmap.de/)

![Radio OSM](podcast-e1557674429764-150x150.png)

## [](#4-geodorable)**4\. Geodorable**

Ein Podcast, der alles und jeden über die [Geodaten](/geodaten-was-sind-geodaten)welt beinhalten kann.

[https://geodorable.com/](https://geodorable.com/)

![Geodorable Podcast](index.jpg "Geodorable")

## [](#5-the-mappyist-hour)**5\. The Mappyist Hour**

Geographen und Geo-Typen, die darüber sprechen, wie unglaublich ihr Beruf nach Feierabend ist.

[http://www.themappyisthour.com/](http://www.themappyisthour.com/)

![The Mappyist Hour Podcast](F_739vLb_400x400.jpg "The Mappyist Hour")

## [](#6-speaking-of-gis)**6\. Speaking of GIS**

Ein Podcast von Kurt Towler. Der Podcast beinhaltet Interviews mit anderen Geospatialisten und Rückblicke auf Konferenzen.

[https://speakingofgis.com/](https://speakingofgis.com/)

![Speaking of GIS Podcast](ltcN4uOG_400x400.jpg "Speaking of GIS")

## [](#7-scene-from-above)**7\. Scene from Above**

Ein Podcast mit Blick auf die Welt der modernen [Fernerkundung](/tags/fernerkundung) und Erdbeobachtung. Angetrieben von ihrer Leidenschaft für alles Raster- und Geodaten, streben sie eine Mischung aus Nachrichten, Meinungen, Diskussionen und Interviews an.

[http://scenefromabove.org/](http://scenefromabove.org/)

![Scene from Above Podcast](XIxrl16S_400x400.jpg "Scene from Above")

## [](#8-directions-magazine)**8\. Directions Magazine**

Etwa alle sechs Wochen werden neue standortbasierte Podcasts von einer anderen Geoinformations-Filiale herausgegeben.

[https://player.fm/series/directions-magazine-podcasts](https://player.fm/series/directions-magazine-podcasts)

![Directions Magazine Podcast](VrHWMDgk_400x400.jpg "Directions Magazine ")

## [](#9-cageyjames--geobabbler)**9\. Cageyjames & Geobabbler**

In diesem monatlichen Podcast von James Fee und Bill Dollins geht es darum, wie Sie räumliche Technologien in Ihren Arbeitsabläufen einsetzen können.

[https://cng.fireside.fm/](https://cng.fireside.fm/)

![Cageyjames & Geobabbler Podcast](cagey.png "Cageyjames & Geobabbler")

## [](#10-geographical-imaginations)**10\. Geographical Imaginations**

Geographical Imaginations Expedition & Institute ist eine wachsende öffentliche Geographieinitiative für multimediale Medien, die akademisches und alltägliches geografisches oder räumliches Denken zusammenbringen soll.

[https://podcasts.apple.com/us/podcast/geographical-imaginations/id1386704057?mt=2](https://podcasts.apple.com/us/podcast/geographical-imaginations/id1386704057?mt=2)

![Geographical Imaginations](170x170bb.jpg)

## [](#11-women-and-drones)**11\. Women and Drones**

Ein Podcast, der dazu dient, Frauen in der UAS-Branche auf der ganzen Welt bekannter zu machen.

[http://womenanddrones.libsyn.com/](http://womenanddrones.libsyn.com/)

![Women and Drones Podcast](nCObdGxO_400x400.jpg "Women and Drones")