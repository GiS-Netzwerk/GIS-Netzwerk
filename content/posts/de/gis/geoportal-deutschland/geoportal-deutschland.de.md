---
layout: "post"
title: "Geoportal Deutschland und Geoportale der deutschen Bundesländer"
date: "2019-06-03"
description: "Hier findest du das Geoportal Deutschland und alle Geoportale der sechzehn Bundesländer mit Links kompakt und alphabetisch geordnet."
category: "GIS"
tags: ["Web-GIS", "Geodaten"]
image: "/assets/img/geoportal-deutschland-und-bundeslaender.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Hier findest du das Geoportal Deutschland und alle Geoportale der sechzehn Bundesländer mit Links kompakt und alphabetisch geordnet.

[Geoportal Deutschland](https://www.geoportal.de/DE/Geoportal/geoportal.html?lang=de)

## [](#geoportale-der-bundesl%C3%A4nder)Geoportale der Bundesländer

[**1\. Geoportal Baden-Württemberg**](https://www.geoportal-bw.de/)

[**2\. Geoportal Bayern**](https://geoportal.bayern.de/geoportalbayern/)

[**3\. Geoportal Berlin**](https://fbinter.stadt-berlin.de/fb/index.jsp)

[**4\. Geoportal Brandenburg**](https://geoportal.brandenburg.de/startseite/)

[**5\. Geoportal Bremen**](https://www.geo.bremen.de/)

[**6\. Geoportal Hamburg**](https://geoportal-hamburg.de/geoportal/geo-online/)

[**7\. Geoportal Hessen**](http://www.geoportal.hessen.de/)

[**8\. Geoportal Mecklenburg-Vorpommern**](https://www.geoportal-mv.de/portal/)

[**9\. Geoportal Niedersachsen**](http://www.geodaten.niedersachsen.de/startseite/)

[**10\. Geportal Nordrhein-Westfalen**](https://www.geoportal.nrw/)

[**11\. Geoportal Rheinland-Pfalz**](https://www.geoportal.nrw/)

[**12\. Geoportal Saarland**](http://geoportal.saarland.de/mapbender/geoportal/mod_index.php?mb_user_myGui=Geoportal-SL)

[**13\. Geoportal Sachsen**](https://geoportal.sachsen.de/cps/karte.html?showmap=true)

[**14\. Geoportal Sachsen-Anhalt**](https://www.lvermgeo.sachsen-anhalt.de/de/startseite_viewer.html)

[**15\. Geoportal Schleswig-Holstein**](https://danord.gdi-sh.de/viewer/resources/apps/Anonym/index.html?lang=de)

[**16\. Geoportal Thüringen**](http://www.geoproxy.geoportal-th.de/geoclient/start_geoproxy.jsp)