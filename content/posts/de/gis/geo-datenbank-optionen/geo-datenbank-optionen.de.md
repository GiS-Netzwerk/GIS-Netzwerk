---
layout: "post"
title: "GIS- und Geo-Datenbank Managementsystem Optionen"
date: "2019-04-14"
description: "Mit den immer weiter wachsenden Datenmengen stellt sich irgendwann die Frage, wie diese effektiv verwaltet werden können. Hier kommen GIS-Datenbanken zum Einsatz."
category: "GIS"
tags: ["SQL"]
image: "/assets/img/gis-und-geo-datenbanken.jpg"
caption: "ESA/DLR/FU Berlin; CC BY-SA 3.0 IGO"
published: "yes"
author: "Max Dietrich"
lang: "de"
---

Mit den immer weiter wachsenden Datenmengen stellt sich irgendwann die Frage, wie diese effektiv verwaltet werden können. Hier kommen GIS-Datenbanken zum Einsatz.

Mit räumlichen Datenbanken, die Teil eines [Geoinformationssystems](/gis/was-ist-gis "Geoinformationssystem") sind, können [Geodaten](/gis/was-sind-geodaten) gespeichert, verwaltet und abgefragt werden.

## [](#open-source-und-kostenlos)**Open Source und kostenlos**

### [](#1-arangodb-community-edition)1\. ArangoDB Community Edition

ArangoDB ist eine benutzerfreundliche,leistungsfähige Open-Source-NoSQL-Datenbank mit einer einzigartigen Kombination von Funktionen.

[https://www.arangodb.com](https://www.arangodb.com)

### [](#2-postgis--postgresql)2\. PostGIS / PostgreSQL

PostGIS stellt räumliche Objekte für die PostgreSQL-Datenbank bereit, die die Speicherung und Abfrage von Informationen zu Standort und Zuordnung ermöglichen.

[https://postgis.net](https://postgis.net) / [https://www.postgresql.org](https://www.postgresql.org)

### [](#3-mariadb)3\. MariaDB

Einer der beliebtesten Datenbankserver. Entwickelt von den ursprünglichen Entwicklern von MySQL.

[https://mariadb.org/](https://mariadb.org/)

### [](#4-mysql)4\. MySQL

MySQL ist eines der weltweit verbreitetsten relationalen Datenbankverwaltungssysteme. Es ist als Open-Source-Software sowie als kommerzielle Enterpriseversion für verschiedene Betriebssysteme verfügbar.

[https://www.mysql.com/](https://www.mysql.com/de/)

### [](#5-orientdb)5\. OrientDB

OrientDB ist eine in Java geschriebene Open-Source-NoSQL-Datenbank. OrientDB ist eine dokumentenorientierte Datenbank, die zusätzlich über Eigenschaften von Graphdatenbanken verfügt

[https://www.orientdb.com](https://www.orientdb.com)

### [](#6-sqlite--spatiallite)6\. SQLite / SpatialLite

SpatiaLite ist eine Open-Source-Bibliothek, die SQLite um vollwertige Spatial-SQL-Funktionen erweitert.

[https://www.sqlite.org](https://www.sqlite.org/index.html) / [https://www.gaia-gis.it/fossil/libspatialite](https://www.gaia-gis.it/fossil/libspatialite/index)

## [](#kostenpflichtig)**Kostenpflichtig**

### [](#7-oracle-spatial)7\. Oracle Spatial

[https://www.oracle.com](https://www.oracle.com/technetwork/database/options/spatialandgraph/downloads/index.html)

Oracle Spatial and Graph ist eine separat lizenzierte Komponente der Oracle-Datenbank. Die Erweiterung dient der Speicherung und Verwaltung von Geoinformationen.