from oauth2client.service_account import ServiceAccountCredentials
import httplib2
import json
import time
import pandas as pd

# developers.google.com/search/apis/indexing-api/v3/prereqs#header_2
JSON_KEY_FILE = "V:\Websites\GIS-Netzwerk\gis-netzwerk-4e7bc73cd855.json"
SCOPES = ["https://www.googleapis.com/auth/indexing"]

credentials = ServiceAccountCredentials.from_json_keyfile_name(JSON_KEY_FILE, scopes=SCOPES)
http = credentials.authorize(httplib2.Http())

urls = [
    "https://gis-netzwerk.com/web-development/gatsby-aws-codebuild-codepipeline",
    "https://gis-netzwerk.com/web-development/gatsby-js-google-adsense",
    "https://gis-netzwerk.com/web-development/statische-website-aws-cloudfront",
    "https://gis-netzwerk.com/gis/schreib-fuer-uns/",
    "https://gis-netzwerk.com/web-development/gatsby-buddy/",
    "https://gis-netzwerk.com/web-development/bildbearbeitung-mit-python-zuschneiden/",
    "https://gis-netzwerk.com/gis/pyqgis-layer-bilder-exportieren/",
    "https://gis-netzwerk.com/web-development/gatsby-analytics-reporting-api-seitenaufrufe/",
    "https://gis-netzwerk.com/gis/react-leaft-erste-schritte/",
    "https://gis-netzwerk.com/gis/extraktion-hohl-vollformen-raster-dgm/",
    "https://gis-netzwerk.com/gis/was-ist-gis/",
    "https://gis-netzwerk.com/gis/openlayers-web-map/",
    "https://gis-netzwerk.com/web-development/migration-wordpress-gatsby/",
    "https://gis-netzwerk.com/gis/datenschutz/",
    "https://gis-netzwerk.com/gis/kontakt/",
    "https://gis-netzwerk.com/gis/impressum/",
    "https://gis-netzwerk.com/gis/mit-gis-netzwerk-werben/",
    "https://gis-netzwerk.com/gis/geoportal-deutschland-und-geoportale-der-deutschen-bundeslander/",
    "https://gis-netzwerk.com/gis/geodaten-transformation-konvertierung-und-formatumwandlung/",
    "https://gis-netzwerk.com/gis/hochaufloesende-satellitenbilder-downloaden/",
    "https://gis-netzwerk.com/gis/fernerkundung-und-bildklassifikation/",
    "https://gis-netzwerk.com/gis/open-street-map-daten-als-shapefiles-downloaden/",
    "https://gis-netzwerk.com/gis/qgis-kostenlos-lernen/",
    "https://gis-netzwerk.com/gis/gis-software-kategorien-welche-arten-von-gis-software-gibt-es/",
    "https://gis-netzwerk.com/gis/8-open-source-web-gis-anwendungen/",
    "https://gis-netzwerk.com/gis/fme-lizenz-kostenlos-fur-den-privaten-gebrauch/",
    "https://gis-netzwerk.com/gis/geo-und-gis-podcasts-um-auf-dem-aktuellen-stand-zu-bleiben/",
    "https://gis-netzwerk.com/gis/was-ist-ein-shapefile-shp-dbf-shx/",
    "https://gis-netzwerk.com/gis/einfacher-download-fur-30-meter-srtm-tiles/",
    "https://gis-netzwerk.com/gis/gk-utm-koordinaten-umrechnen-transformationstool/",
    "https://gis-netzwerk.com/gis/gis-und-geo-datenbank-managementsystem-optionen/",
    "https://gis-netzwerk.com/gis/gis-und-augmented-reality-ar/",
    "https://gis-netzwerk.com/gis/gis-freiwilligenarbeit-mit-gis-von-zuhause-die-welt-verbessern/",
    "https://gis-netzwerk.com/gis/gehalt-in-der-gis-branche/",
    "https://gis-netzwerk.com/gis/die-bekanntesten-geospatial-und-gis-influencers/",
    "https://gis-netzwerk.com/gis/postgre-sql-mit-post-gis-installieren-und-in-qgis-einrichten/",
    "https://gis-netzwerk.com/gis/web-feature-service-open-source-wfs/",
    "https://gis-netzwerk.com/gis/geographie-und-gis-blogs/",
    "https://gis-netzwerk.com/gis/gis-anwendungen-welche-gis-anwendungen-gibt-es/",
    "https://gis-netzwerk.com/gis/gis-jobborsen-auf-der-suche-nach-einem-neuen-gis-job/",
    "https://gis-netzwerk.com/gis/unterschied-cad-gis/",
    "https://gis-netzwerk.com/gis/auto-cad-map-3-d-shapefile-export/",
    "https://gis-netzwerk.com/gis/gis-firmen-und-gis-dienstleister-verzeichnis/",
    "https://gis-netzwerk.com/gis/unigis-weiterbildung-geoinformatik/",
    "https://gis-netzwerk.com/gis/gis-software-optionen/",
    "https://gis-netzwerk.com/gis/geodaten-deutschland-download/",
    "https://gis-netzwerk.com/gis/geodatenmanager-weiterbildung-universitat-tubingen/",
    "https://gis-netzwerk.com/gis/geodaten-was-sind-geodaten/",
    "https://gis-netzwerk.com/gis/geodaten-was-sind-geodaten/",
    "https://gis-netzwerk.com/en/web-development/gatsby-js-google-adsense",
    "https://gis-netzwerk.com/en/gis/geographic-information-system-what-is-gis",
    "https://gis-netzwerk.com/en/gis/satellite-imagery-download",
    "https://gis-netzwerk.com/en/gis/download-openstreetmap-data",
    "https://gis-netzwerk.com/en/gis/free-ways-learn-qgis",
    "https://gis-netzwerk.com/en/gis/free-fme-licence-private-use",
    "https://gis-netzwerk.com/en/gis/gis-podcasts",
    "https://gis-netzwerk.com/en/gis/what-is-shapefile",
    "https://gis-netzwerk.com/en/gis/gis-and-geo-database-management-system-options",
    "https://gis-netzwerk.com/en/gis/gis-voluntary-work",
    "https://gis-netzwerk.com/en/gis/gis-influencers",
    "https://gis-netzwerk.com/en/gis/gis-blogs",
    "https://gis-netzwerk.com/en/gis/gis-applications",
    "https://gis-netzwerk.com/en/gis/gis-vs-cad-the-difference-between-gis-and-cad",
    "https://gis-netzwerk.com/en/gis/open-source-proprietary-software-options",
    "https://gis-netzwerk.com/en/gis/geodata-what-are-geodata",
]

def indexURL(urls, http):
    # print(type(url)); print("URL: {}".format(url));return;

    ENDPOINT = "https://indexing.googleapis.com/v3/urlNotifications:publish"
    
    for u in urls:
        # print("U: {} type: {}".format(u, type(u)))
    
        content = {}
        content['url'] = u.strip()
        content['type'] = "URL_UPDATED"
        json_ctn = json.dumps(content)    
        # print(json_ctn);return
    
        response, content = http.request(ENDPOINT, method="POST", body=json_ctn)

        result = json.loads(content.decode())
        time.sleep(2)

        # For debug purpose only
        if("error" in result):
            print("Error({} - {}): {}".format(result["error"]["code"], result["error"]["status"], result["error"]["message"]))
        else:
            print("urlNotificationMetadata.url: {}".format(result["urlNotificationMetadata"]["url"]))
            print("urlNotificationMetadata.latestUpdate.url: {}".format(result["urlNotificationMetadata"]["latestUpdate"]["url"]))
            print("urlNotificationMetadata.latestUpdate.type: {}".format(result["urlNotificationMetadata"]["latestUpdate"]["type"]))
            print("urlNotificationMetadata.latestUpdate.notifyTime: {}".format(result["urlNotificationMetadata"]["latestUpdate"]["notifyTime"]))

"""
data.csv has 2 columns: URL and date.
I just need the URL column.
"""
indexURL(urls, http)