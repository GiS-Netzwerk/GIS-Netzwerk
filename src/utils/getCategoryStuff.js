import "../styles/variables.scss";

export default function getCategoryStuff(category) {
    if (category == "GIS") {
        var bordercategory = "3px solid #3f51b5"
        var linkcategory = "/gis"
        var color = "#3f51b5"
    } else if (category == "Web-Development") {
        var bordercategory = "3px solid rgba(152,70,242,1)"
        var linkcategory = "/web-development"
        var color = "rgba(152,70,242,1)"
    } else if (category == "Entertainment"){
        var bordercategory = "3px solid rgb(58, 204, 219)"
        var linkcategory = "/entertainment"
        var color = "rgb(58, 204, 219)"
    } else {
        var bordercategory = "3px solid #ff5b60"
        var linkcategory = "/unterhaltung"
        var color = "#ff5b60"
    }
    var category_name = category
    return [bordercategory, linkcategory, color, category_name]
}