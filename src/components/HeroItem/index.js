import React from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';
import useTranslations from '../useTranslations';
import getCategoryStuff from '../../utils/getCategoryStuff'
import * as S from './styled';
import { Link } from 'gatsby';

const HeroItem = ({
  slug,
  category,
  date,
  timeToRead,
  title,
  description,
  image,
  locale,
  layout,
}) => {
  const { toRead } = useTranslations();

  
  const { listImages } = useStaticQuery(
    graphql`
    query {
      listImages: allImageSharp {
        edges {
          node {
            fluid(maxWidth: 1920, maxHeight: 350) {
              src
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    }
    
  `,
  );

  const postImgCover = listImages.edges.find(img => {
    return img.node.fluid.src.includes('cover');
  });

  const imgName = image ? image.split('/')[3] : false;

  const colorcategory = getCategoryStuff(category)[2]
  const categorySlug = category.toLowerCase()

  const postImg = imgName
    ? listImages.edges.find(img => {
        return img.node.fluid.src.includes(imgName);
      })
    : false;

  return (
      <div>
      <S.HeroItemImgContainer>
        {postImg && (
          <S.HeroItemImg
            fluid={postImg.node.fluid}
            alt={title}
            title={title}
          />
        )}
        {!postImg && (
          <S.HeroItemImg
            fluid={postImgCover.node.fluid}
            alt={title}
            title={title}
          />
        )}
        <S.HeroItemInfoContainer>
          <S.HeroItemInfo>
            <S.HeroItemCategoryLink to={`/${categorySlug}`}>
              <S.HeroItemTag background={colorcategory}>{category}</S.HeroItemTag>
            </S.HeroItemCategoryLink>
            <S.HeroLayoutTag layout={layout}>/ {layout}</S.HeroLayoutTag>
            <S.HeroItemDate>
              {date} • {timeToRead} min {toRead}
            </S.HeroItemDate>
            <S.HeroItemLink to={`/${slug}`}> <S.HeroItemTitle>{title}</S.HeroItemTitle></S.HeroItemLink>
            <S.HeroItemDescription>{description}</S.HeroItemDescription>
          </S.HeroItemInfo>
        </S.HeroItemInfoContainer>
      </S.HeroItemImgContainer>
    </div>
  );
};

HeroItem.propTypes = {
  slug: PropTypes.string.isRequired,
  background: PropTypes.string,
  category: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  timeToRead: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default HeroItem;
