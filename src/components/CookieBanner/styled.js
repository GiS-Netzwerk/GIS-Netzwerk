import styled from 'styled-components';

export const CookieButton = styled.button`
  padding: 12px 15px;
  margin: 2rem auto;
  border-width: 0px;
  border-color: rgb(0, 0, 0);
  border-radius: 4px;
  border-style: solid;
  font-size: 15px;
  font-weight: 700;
  color: rgb(255, 255, 255);
  width: 299px;
  background: var(--primary-color);
  display: flex;
  align-items: center;
  justify-content: center;

`

export const CookieBanner = styled.div`
    text-align: center;
    border-radius: 4px;
    color: white;
    font-size: 1.6rem;
    margin: 100px auto auto auto;
    max-width: 400px;
    padding: 2rem 2rem 2rem 2rem;
    `
