import React from 'react';
import CookieConsent from 'react-cookie-consent';
import * as S from './styled';
import { Link } from 'gatsby';
import useTranslations from '../useTranslations';
import ReactGA from 'react-ga';


const  CookieBanner = ({
    
    locale,
}) => {
    let localeslug;

    locale === "en" ? 
        localeslug = "/en/privacy-policy" : localeslug = "/datenschutz";

    const { privacypolicy, cookiedescription, cookiedescriptionads, cookietitle, cookietitleads, accept } = useTranslations();

    return (
            <CookieConsent
            disableStyles={true}
            style={{
                backgroundColor: 'rgba(20,20,20,0.8)',
                minHeight: '26px',
                fontSize: '14px',
                color: '#ccc',
                lineHeight: '26px',
                position: 'fixed',
                bottom: '0',
                width: '100vw',
                height: '100vh',
                overflow: 'hidden',
                zIndex: '9999'
            }}
            buttonStyle={{
                padding: '12px 15px',
                margin: '2rem auto',
                borderWidth: '0px',
                borderColor: 'rgb(0, 0, 0)',
                borderRadius: '4px',
                borderStyle: 'solid',
                fontSize: '15px',
                fontWeight: '700',
                color: 'rgb(255, 255, 255)',
                width: '299px',
                background: 'var(--primary-color)',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
            }}
            location="bottom"
            buttonText={accept}
            cookieName="analytics"
            onAccept={() => {
                ReactGA.initialize('UA-117248551-1');
                ReactGA.pageview(window.location.pathname + window.location.search);
              }}
            >
            <S.CookieBanner>
            <h2>GIS-Netzwerk</h2>
            {cookiedescription} <Link to={localeslug}style={{textDecoration:'underline'}}>{privacypolicy}</Link>.
            </S.CookieBanner>
            </CookieConsent>
    );
}
export default CookieBanner
