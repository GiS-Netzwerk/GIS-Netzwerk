import styled from 'styled-components';
import media from 'styled-media-query';
import LocalizedLink from '../LocalizedLink';
import { Link } from 'gatsby';

export const Navigation = styled.nav`
  ${media.greaterThan('medium')`
    align-items: center;
    justify-content: center;
    margin-top: 20px;
  `}
`;

export const NavigationLink = styled(LocalizedLink)`
  transition: 0.2s;
  :hover {
    color: white;
  }  
`;

export const NavigationButton = styled(Link)`
  background: var(--primary-color);
  border-radius: 2px;
  color: #fff;
  padding: var(--space-sm) var(--space);
  text-decoration: none;
  font-weight: bold;
  text-align: center;
  ${media.greaterThan('medium')`
    margin-left: var(--space-lg);
  `}
`;
