import React, { useState } from 'react';
import { Link } from "gatsby"
import * as S from './styled';
import useTranslations from '../useTranslations';
import Languages from '../Languages';
import Navigation from '../FooterNavigation';
import SocialLinks from '../SocialLinks/SocialLinks.js';
import Logo from '../Logo';


const Footer = () => {

  const [toggleMenu, setToggleMenu] = useState(false);

  function handleToggleMenu() {
    setToggleMenu(!toggleMenu);
  }

  const { hello, subline, copyrightNotice} = useTranslations();

  return (
    <>
    <S.FooterWrapper>
      <S.FooterContainer>
        <S.LogoLink to="/" title="GIS-Netzwerk" aria-label="GIS-Netzwerk">
          <Logo />
        </S.LogoLink>
        <S.FooterNav>      
          <Navigation isActive={toggleMenu} handleToggleMenu={handleToggleMenu} />
        </S.FooterNav>
          
        <S.FooterDivider>
        </S.FooterDivider>
        <S.FooterSocials>
          <SocialLinks/>
        </S.FooterSocials>
        
        <S.NavLanguages>
          <Languages />
        </S.NavLanguages>
        </S.FooterContainer>
      </S.FooterWrapper>
    </>
  );
};

export default Footer;
