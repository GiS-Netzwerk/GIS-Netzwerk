import styled from 'styled-components';
import media from 'styled-media-query';

export const ListWrapper = styled.section`
  margin-bottom: 20px;
  ${media.greaterThan('small')`
        display: grid;
        grid-gap: 20px;
        grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));
    `}
  ${media.greaterThan('large')`
        grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));
    `}
`;

export const IndexListing = styled.div`
  margin: 30px auto;
  max-width: 100%;
  ${media.greaterThan('medium')`   
  padding: 0 20px 0 20px;  
    `}
`

export const CategoryListing = styled.div`
  margin: 80px auto 20px auto;
  padding: 0 20px 0 20px;
  max-width: 100%;
  ${media.greaterThan('large')`     
    `}
`

export const TagListing = styled.div`
  margin: 80px auto 20px auto;
  padding: 0 20px 0 20px;
  max-width: 100%;
  ${media.greaterThan('large')`     
    `}
`