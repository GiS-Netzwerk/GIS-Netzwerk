import React from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';
import * as S from './styled';
import { Parallax } from 'react-scroll-parallax';

const PostCover = ({
  title,
  image,
}) => {

  const { listImages } = useStaticQuery(
    graphql`
      query {
        listImages: allImageSharp {
          edges {
            node {
              fluid(maxWidth: 1920, maxHeight: 350) {
                src
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    `,
  );

  const postImgCover = listImages.edges.find(img => {
    return img.node.fluid.src.includes('cover');
  });

  const imgName = image ? image.split('/')[3] : false;

  const postImg = imgName
    ? listImages.edges.find(img => {
        return img.node.fluid.src.includes(imgName);
      })
    : false;

  return (
      <div>
        <Parallax className="custom-class" y={[-20, 20]} tagOuter="figure">
          {postImg && (
            <S.PostCoverImg
              fluid={postImg.node.fluid}
              alt={title}
              title={title}
              style={{objectFit:'cover'}}
            />
          )}
          {!postImg && (
            <S.PostCoverImg
              fluid={postImgCover.node.fluid}
              alt={title}
              title={title}
              style={{objectFit:'cover'}}
            />
          )}
        </Parallax>
    </div>
  );
};


export default PostCover;
