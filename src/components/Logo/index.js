import React from 'react';
import logo from '../../gis-netzwerk_favicon.png';

const Logo = () => {
  return <img src={logo} alt="GIS-Netzwerk" />;
};

export default Logo;
