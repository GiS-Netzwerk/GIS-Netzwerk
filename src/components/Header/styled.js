import styled from 'styled-components';
import media from 'styled-media-query';
import LocalizedLink from '../LocalizedLink';

export const HeaderWrapper = styled.div`
  background-color: #000;
  z-index: 9999;
  position: fixed;
  width: 100%;
  border-bottom: 3px solid ${props =>
    props.categoryColor ? props.categoryColor : "black"};
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;    
  max-width: 100%;
  margin: 0 auto;
  align-items: center;
  padding: calc(var(--space) * 0.3) var(--space);
  position: relative;
  ${media.greaterThan('medium')`
    flex-direction: row;
  `}
  ${media.greaterThan('large')`
    padding: calc(var(--space)*0.3) var(--space-sm);
  `}
`;

export const LogoLink = styled(LocalizedLink)`
  display: inline-block;
  margin-right: auto;
  width: 170px;
`;

export const NavMenu = styled.div`
  width: auto;
  ${media.greaterThan('medium')`
    margin: 0 10px 0 auto;
    width: auto;
  `}
`;
