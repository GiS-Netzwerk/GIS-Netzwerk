import React from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';
import useTranslations from '../useTranslations';
import getCategoryStuff from '../../utils/getCategoryStuff'
import * as S from './styled';



const PostItem = ({
  slug,
  category,
  date,
  timeToRead,
  title,
  image,
  layout,
  locale,
}) => {
  const { toRead } = useTranslations();

  const { listImages } = useStaticQuery(
    graphql`
      query {
        listImages: allImageSharp {
          edges {
            node {
              fluid(maxWidth: 360, maxHeight: 300) {
                src
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    `,
  );

  const postImgCover = listImages.edges.find(img => {
    return img.node.fluid.src.includes('cover');
  });

  const imgName = image ? image.split('/')[3] : false;

  const colorcategory = getCategoryStuff(category)[2]

  const postImg = imgName
    ? listImages.edges.find(img => {
        return img.node.fluid.src.includes(imgName);
      })
    : false;

  return (
    <S.PostItemLink to={slug}>
      <S.PostItemWrapper>
        <S.PostImageItemWrapper>
          <S.PostItemLayoutInfo>{layout}</ S.PostItemLayoutInfo>
          {postImg && (
            <S.PostItemImg
              fluid={postImg.node.fluid}
              alt={title}
            />
          )}
          {!postImg && (
            <S.PostItemImg
              fluid={postImgCover.node.fluid}
              alt={title}
            />
          )}
        </S.PostImageItemWrapper>
        <S.PostItemInfo>
          <S.PostItemTag background={colorcategory}>
            {category}
          </S.PostItemTag>
          <S.PostItemDate>
            {date} • {timeToRead} min {toRead}
          </S.PostItemDate>
          <S.PostItemTitle>{title}</S.PostItemTitle>
          {/*<S.PostItemDescription>{description}</S.PostItemDescription>*/}
        </S.PostItemInfo>
      </S.PostItemWrapper>
    </S.PostItemLink>
    
  );
};

PostItem.propTypes = {
  slug: PropTypes.string.isRequired,
  background: PropTypes.string,
  category: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  timeToRead: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  layout: PropTypes.string.isRequired,
};

export default PostItem;
