import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart, faEnvelope, faRss} from '@fortawesome/free-solid-svg-icons'
import { faTwitter, faInstagram, faXing, faLinkedin, faGithub } from '@fortawesome/free-brands-svg-icons'
import config from "../../../data/SiteConfig";
import useTranslations from '../useTranslations';
import { Link } from "gatsby"
import * as S from './styled';

const iconHeart = <FontAwesomeIcon icon={faHeart} style={{color:'red'}}/>
const iconInsta = <FontAwesomeIcon icon={faInstagram}/>
const iconTwitter = <FontAwesomeIcon icon={faTwitter} />
const iconRss = <FontAwesomeIcon icon={faRss} />
const iconMail = <FontAwesomeIcon icon={faEnvelope} />
const iconXing = <FontAwesomeIcon icon={faXing} />
const iconLinkedin = <FontAwesomeIcon icon={faLinkedin} />
const iconGithub = <FontAwesomeIcon icon={faGithub} />

const SocialLinks = () => {
    return (
        <S.SocialLinks>
            <S.LinkInsta title="Instagram Link" alt="Instagram Link" href="https://www.instagram.com/_maxdietrich/">{iconInsta}</S.LinkInsta>
            <S.LinkTwitter title="Twitter Link" alt="Twitter Link" href="https://twitter.com/GISNetzwerk">{iconTwitter}</S.LinkTwitter>
            <S.LinkXing title="Xing Link" alt="Xing Link" href="https://www.xing.com/profile/Max_Dietrich7">{iconXing}</S.LinkXing>
            <S.LinkLinkedin title="Linkedin Link" alt="Linkedin Link" href="https://www.linkedin.com/in/max-dietrich-807bb5161/">{iconLinkedin}</S.LinkLinkedin>
            <S.LinkMail title="Mail Link" alt="Mail Link" href="mailto:kontakt@gis-netzwerk.com">{iconMail}</S.LinkMail>
        </S.SocialLinks>
    );
  
}

export default SocialLinks;
