import styled from 'styled-components';
import media from 'styled-media-query';
import Img from 'gatsby-image';
import { Link } from "gatsby"


export const SocialLinks = styled.div`  
    color: #80868B;  
`

export const LinkInsta = styled.a`
    margin: 20px; 
    transition: all .3s;
    :hover {
        color: #E1306C;
        text-decoration: none;
`

export const LinkTwitter = styled.a`  
    margin: 20px; 
    transition: all .3s;
    :hover {
        color: #0aceee;
        text-decoration: none;
    }
`

export const LinkXing = styled.a`
    margin: 20px; 
    transition: all .3s;
    :hover {
        color: #126567;
        text-decoration: none;
    }
`

export const LinkLinkedin = styled.a`
    margin: 20px; 
    transition: all .3s;
    :hover {
        color: #0e76a8;
        text-decoration: none;
    }
`

export const LinkGitHub = styled.a`  
    margin: 20px; 
    transition: all .3s;
    :hover {
        color: #24292e;
        text-decoration: none;
    }
`

export const LinkRSS = styled(Link)`  
    margin: 20px; 
    transition: all .3s;
    :hover {
        color: #f26522;
        text-decoration: none;
    }
`
export const LinkMail = styled.a`  
    margin: 20px; 
    transition: all .3s;
    :hover {
        color: darkred;
        text-decoration: none;
    }
`

export const Socials = styled.p`
`