import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import "../../layout/variables.scss";
import Mailchimp from 'react-mailchimp-form'
import * as styles from './EmailListForm.module.scss';

class EmailListForm extends Component {
  render() {
    return (
      <div className="email-container" >
          <p class="email-container-title">Hey!</p>
          <p class="lead">Lust auf mehr Geoinformatik, GIS und Web-Development News?</p>
          <p>
          <Form.Group controlId="formBasicEmail">
          <Mailchimp action='https://gis-netzwerk.us20.list-manage.com/subscribe/post?u=098dd2c3187e7002e77fc5e31&amp;id=46a8747b7e' 
          //Adding multiple fields:
          fields={[
            {
              name: 'EMAIL',
              placeholder: 'Email',
              type: 'email',
              required: true
            },
          ]}
          // Change predetermined language
          messages = {
            {
              sending: "Senden...",
              success: "Willkommen! Du bist jetzt Mitglied des Newsletters",
              error: "Ein unerwarteter Fehler ist aufgetreten. Was ist passiert?",
              empty: "Du hast deine E-Mail-Adresse vergessen.",
              duplicate: "Locker bleiben! Probier es in ein paar Minuten erneut",
              button: "Abonnieren!"
            }
          }
          // Add a personalized class
          className={styles.EmailListForm}
          />
          
          <Form.Text className="EmailFormText" >
          Keine Sorge, kein Spam. Versprochen!
        </Form.Text>
      </Form.Group>
      </p>
    </div>
    )
}
}      
export default EmailListForm

