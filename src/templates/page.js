import React from 'react';
import { graphql } from 'gatsby';
import TitlePage from '../components/TitlePage';
import SEO from '../components/SEO/seo';
import { Helmet } from 'react-helmet' 
import * as S from '../components/Content/styled';
import config from "../../data/SiteConfig";
import { MDXRenderer } from "gatsby-plugin-mdx"
import styled from 'styled-components';

export const PageWrapper = styled.div`
  margin-top: 80px;
`

const Page = props => {
  const { pageContext } = props;
  const { slug } = pageContext;
  const post = props.data.mdx;

  if (!post.frontmatter.id) {
    post.frontmatter.id = slug;
  }

  return (
    <>
      <SEO postNode={post} postPath={slug} />
      <Helmet>
        <title>{`${post.frontmatter.title} | ${config.siteTitle}`}</title>
        <link rel="canonical" href={`${config.siteUrl}${post.frontmatter.id}/`} />
      </Helmet>
      <PageWrapper>
        <TitlePage text={post.frontmatter.title} />
        <S.Content >
          <MDXRenderer>{post.body}</MDXRenderer>
        </S.Content>
      </PageWrapper>
    </>
  );
};

export const query = graphql`
  query Page($locale: String!, $title: String!) {
    mdx(
      frontmatter: { title: { eq: $title } }
      fields: { locale: { eq: $locale } }
    ) {
      frontmatter {
        layout
        title
        description
        image
      }
      body
    }
  }
`;

export default Page;
