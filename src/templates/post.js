import React from 'react';
import { graphql } from 'gatsby';
import TitlePage from '../components/TitlePage';
import SEO from '../components/SEO/seo';
import styled from 'styled-components';
import media from 'styled-media-query';
import * as S from '../components/Content/styled';
import { Link } from 'gatsby';
import getCategoryStuff from '../utils/getCategoryStuff';
import licence from '../utils/licence';
import useTranslations from '../components/useTranslations';
import PostCover from '../components/PostCover';
import { Helmet } from 'react-helmet' 
import config from "../../data/SiteConfig";
import SocialShare from "../components/SocialShare/SocialShare.js";
import { MDXRenderer } from "gatsby-plugin-mdx"




export const MetaElement = styled.div`
  text-align: center;
`
export const CategoryElement = styled.a`
  text-decoration: none;
  font-weight: 200;
  color: white;
  padding: 5px 10px;
  font-size: 1.25rem;
  text-transform: uppercase;
  :hover {
    color: white;
    text-decoration: none;
  }
`

export const PostCoverCaption = styled.p`
  position: relative;
  float: right;
  padding-right: 12px;
  text-transform: uppercase;
  color: rgb(85,85,85);
  font-size: 10px;
`

export const DateElement = styled.div`    
  color: rgb(108, 117, 125);
  font-size: 1.2rem;
  margin-top: 8px;

`
export const ToC = styled.div`    
  margin-bottom: 40px;
  border-bottom: solid 2px rgba(10, 10, 10, 0.1);
  padding: 10px 15px 10px;
  
  ${media.lessThan('medium')`
    float: none;
    border-left: none;
    max-width: 100%;
    border-bottom: 1px solid rgba(0,0,0,.82);
    margin: auto auto 3rem;
  `}
`
export const ToCHeader = styled.div`
  padding-left: 30px;
  font-weight: bold;
  margin-bottom: 10px;
  font-size: 14px !important;
  margin-top: 0px !important;
`
export const ToCContent = styled.div`
  font-size: 14px;
  text-align: -webkit-match-parent;
  color: #848d95;
  ul {
    margin-left: 0px;
    padding-bottom: 0px;
    list-style-type: none;
    margin-bottom: 0px !important;
  }
  li {
    margin: 0;
  }
  li:before {
    content: "# ";
    margin: 5px 0 5px 0;
    line-height: 1.2;
  }
  a {
    color: rgb(85, 85, 85);
    display: inline-block;
    font-weight: 700;
    transition: color 0.2s ease 0s;
    ${media.lessThan('medium')`
      margin: 5px 0 5px 0;
  `}
  }
`
const Post = props => {
  const { pageContext } = props;
  const { slug, locale } = pageContext;
  const post = props.data.mdx;
  let localeslug;

  locale === "de" ? localeslug = "" : localeslug = "/en";
  
  if (!post.frontmatter.id) {
    post.frontmatter.id = slug;
  }

  const categoryColor = getCategoryStuff(post.frontmatter.category)[2];
  const categorySlug_temp = post.frontmatter.category.toLowerCase();
  const categorySlug = `${localeslug}/${categorySlug_temp}`;
  
  const licenceName = licence(post.frontmatter.caption)[0];
  const licenceLink = licence(post.frontmatter.caption)[1];
  const newCaption = licence(post.frontmatter.caption)[2];

  const {
    imageSource,
    TableofContents,
  } = useTranslations();
  
  return (
    <>
      <SEO postPath={slug} postNode={post} postSEO="post" />
      <Helmet>
        <title>{`${post.frontmatter.title} | ${config.siteTitle}`}</title>
        <link rel="canonical" href={`${config.siteUrl}${post.frontmatter.id}`} />
      </Helmet>
      <PostCover image={post.frontmatter.image} title={post.frontmatter.title}/>
      <PostCoverCaption color={categoryColor}>{imageSource} {newCaption} <a href={licenceLink}>{licenceName}</a></PostCoverCaption>
      <TitlePage text={post.frontmatter.title} />
      <MetaElement>
      <Link to={categorySlug}><CategoryElement style={{backgroundColor: `${categoryColor}`}}>{post.frontmatter.category}</CategoryElement></Link>
      <DateElement>{post.frontmatter.date}</DateElement>
      </MetaElement>
      <S.Content categoryColor={categoryColor}>
        <ToC>
          {/*<ToCHeader>{TableofContents}</ToCHeader>
          <ToCContent dangerouslySetInnerHTML={{ __html: post.tableOfContents }} />*/}
        </ToC>
        <MDXRenderer>{post.body}</MDXRenderer>
        {/*<SocialShare postPath={post.frontmatter.id} postNode={post}/>*/}
      </S.Content>
    </>
  );
};

export const query = graphql`
  query Post($locale: String!, $title: String!) {
    mdx(
      frontmatter: { title: { eq: $title } }
      fields: { locale: { eq: $locale } }
    ) {
      frontmatter {
        title
        date
        category
        tags
        description
        image
        caption
      }
      body
      tableOfContents
    }
  }
`;

export default Post ;
